# AngularEdge

ng serve --open

This project was generated with [angular-cli](https://github.com/angular/angular-cli) version 1.0.0-beta.28.3.

## Development server
Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive/pipe/service/class/module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).
Before running the tests make sure you are serving the app via `ng serve`.

## Deploying to GitHub Pages

Run `ng github-pages:deploy` to deploy to GitHub Pages.

## Further help

To get more help on the `angular-cli` use `ng help` or go check out the [Angular-CLI README](https://github.com/angular/angular-cli/blob/master/README.md).


## Sass setup
Using SASS
Getting Started
Create a new project and navigate into the project

ng new my-app --style=scss
cd my-app
Installing Bootstrap

# version 3.x
npm install bootstrap-sass --save

# version 4.x
npm install bootstrap@next --save
Configuring Project
Create an empty file _variables.scss in src/.

If you are using bootstrap-sass, add the following to _variables.scss:

$icon-font-path: '../node_modules/bootstrap-sass/assets/fonts/bootstrap/';
In styles.scss add the following:

// version 3
@import 'variables';
@import '../node_modules/bootstrap-sass/assets/stylesheets/_bootstrap';

// version 4
@import 'variables';
@import '../node_modules/bootstrap/scss/bootstrap';
Testing Project
Open app.component.html and add the following markup:

<button class="btn btn-primary">Test Button</button>
With the application configured, run ng serve to run your application in develop mode. In your browser navigate to the application localhost:4200. Verify the bootstrap styled button appears. To ensure your variables are used open _variables.scss and add the following:

$brand-primary: red;
Return the browser to see the font color changed.