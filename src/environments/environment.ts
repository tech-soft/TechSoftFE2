// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `ts`, but if you do
// `ng build --env=prod` then `prod.ts` will be used instead.
// The list of which env maps to which file can be found in `angular-cli.json`.
//const host = 'localhost:8080';
const host = '192.168.0.155:8080';
const path = '/techsoftapp';
const baseUrl: String = 'http://' + host + path;

export const environment = {
  production: false,
  HOST: host,
  BASE: baseUrl,
  APIS: {
    AUTH: baseUrl + '/auth',
    BANCO: {
      BASE: baseUrl + '/bancos'
    },
    CIUDAD: {
      BASE: baseUrl + '/ciudades'
    },
    COMPROBANTE_TIPO: {
      BASE: baseUrl + '/comprobante-tipos',
      REMITO: baseUrl + '/comprobante-tipos/remitos'
    },
    DOCUMENTO_TIPO: {
      BASE: baseUrl + '/documento-tipos'
    },
    ESTADO: {
      BASE: baseUrl + '/estados',
      OC: baseUrl + '/estados/ordenes-compra'
    },
    GRAVADOS: {
      BASE: baseUrl + '/gravados'
    },
    IVA_TIPO: {
      BASE: baseUrl + '/iva-tipos'
    },
    MONEDA: {
      BASE: baseUrl + '/monedas'
    },
    ORDEN_COMPRA: {
      BASE: baseUrl + '/ordenes-compra',
      EMITIDAS: baseUrl + '/ordenes-compra/emitidas',
      CANCELAR: baseUrl + '/ordenes-compra/cancelar',
      EMITIR: baseUrl + '/ordenes-compra/emitir',
      RECIBIR: baseUrl + '/ordenes-compra/recibir'
    },
    ORIGEN: {
      BASE: baseUrl + '/origenes'
    },
    PAGO_FORMA: {
      BASE: baseUrl + '/pago-formas'
    },
    PAIS: {
      BASE: baseUrl + '/paises'
    },
    PERSONA: {
      BASE: baseUrl + '/personas',
      BY_KEYWORD: baseUrl + '/personas/keyword',
      DATOS_COMERCIALES: {
        BASE: baseUrl + '/personas/datos-comerciales'
      },
      CONTACTO: {
        BASE: baseUrl + '/personas/contactos'
      },
      DOMICILIO: {
        BASE: baseUrl + '/personas/domicilios'
      },
      RELACION: {
        BASE: baseUrl + '/personas/relacionadas'
      }
    },
    PRECIO_LISTA: {
      BASE: baseUrl + '/precio-listas'
    },
    PRODUCTO: {
      BASE: baseUrl + '/productos',
      PREVIEW: baseUrl + '/productos/previews',
      PREVIEW_LISTAS: baseUrl + '/productos/previews/listas'
    },
    PROVEEDOR: {
      BASE: baseUrl + '/proveedores',
      ACTIVO: baseUrl + '/proveedores/activos'
    },
    PROVINCIA: {
      BASE: baseUrl + '/provincias'
    },
    PUESTO: {
      BASE: baseUrl + '/puestos',
      ACTIVO: baseUrl + '/puestos/activos',
      ACTUAL: baseUrl + '/puestos/activos/sucursales'
    },
    RELACION_TIPO: {
      BASE: baseUrl + '/persona-relaciones-tipos'
    },
    REMITO: {
      BASE: baseUrl + '/proveedores/remitos',
      BY_PROVEEDOR: baseUrl + '/proveedores/remitos/proveedores'
    },
    SUCURSAL: {
      BASE: baseUrl + '/sucursales'
    },
    USUARIO: {
      BASE: baseUrl + '/usuarios',
      BY_USERNAME: baseUrl + '/usuarios/username'
    }
  },
  HTTP: {
    ERROR: {
      EMPTY: 0,
      BAD_REQUEST: 400,
      UNAUTHORIZED: 401,
      FORBIDDEN: 403,
      NOT_FOUND: 404,
      REQUEST_TIMEOUT: 408
    }
  },
  ESTADOS: {
    ACTIVO: 1,
    BAJA: 2
  },
  ESTADOS_OC: {
    NUEVA: 1000,
    EMITIDA: 1001,
    RECIBIDA: 1002,
    CANCELADA: 1003,
    ITEM_NUEVO: 1100,
    ITEM_PEDIDO: 1101,
    ITEM_RECIBIDO: 1102,
    ITEM_PENDIENTE: 1103,
    ITEM_NO_DISPONIBLE: 1104,
    ITEM_CANCELADO: 1105
  },
  OBJECT_TYPE: {
    PRODUCTOS: 1
  }
};
