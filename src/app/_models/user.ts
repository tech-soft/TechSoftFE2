import * as md from '.';

export interface User {
  id: number;
  persona: md.Persona;
  username: String;
  password: String;
  activo: boolean;
  // logo: File;
}
