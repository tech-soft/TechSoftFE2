import * as md from '.';

export interface Persona {
  id: number;
  nombre: String;
  documentoTipo: md.DocumentoTipo;
  documentoNumero: number;
  estado: md.Estado;
  cliente: boolean;
  proveedor: boolean;
}
