import * as md from '.';

export interface RemitoProveedor {
  id: number;
  comprobanteTipo: md.ComprobanteTipo;
  puestoId: number;
  numero: number;
  proveedor: md.Proveedor;
  items: md.RemitoProveedorItem[];
  fecha: string;
  observaciones: string;
}