import * as md from '.';

export interface PersonaRelacionada {
  id: number;
  personaPrincipal: md.Persona;
  personaRelacionada: md.Persona;
  tipo: md.PersonaRelacionTipo;
}
