import * as md from '.';

export interface FacturaProveedor {
  id: number;
  comprobanteTipo: md.ComprobanteTipo;
  puesto: number;
  numero: number;
  proveedor: md.Proveedor;
  items: md.FacturaProveedorItem[];
  fecha: string;
  remito: md.RemitoProveedor;
}