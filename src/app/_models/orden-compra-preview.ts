import * as md from '.';

export interface OrdenCompraPreview {
  id: number;
  puesto: number;
  numero: number;
  proveedor: md.Proveedor;
  fechaCreacion: string;
  estado: md.Estado;
}
