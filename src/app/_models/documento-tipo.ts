
export interface DocumentoTipo {
  id: number;
  descripcion: String;
  descripcionReducida: String;
}
