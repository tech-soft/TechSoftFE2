import * as md from '.';

export interface FacturaProveedorItem {
  id: number;
  producto: md.ProductoPreview;
  cantidad: number;
  ivaTipo: md.IvaTipo;
  precioUnitarioSinIva: number;
  valorUnitarioIva: number;
  precioUnitarioConIva: number;
}