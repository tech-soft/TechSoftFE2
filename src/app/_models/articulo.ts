import * as md from '.';


export interface Articulo {
  id: number;
  descripcion: String;
  descripcionReducida: String;
  codigoBarras: String;
  nroSerie: boolean;
  costo: number;
  stockEntrante: number;
  stockDisponible: number;
  gravado: md.Gravado;
  origen: md.Origen;
  estado: md.Estado;
  precios: Array<md.Precio>;
}
