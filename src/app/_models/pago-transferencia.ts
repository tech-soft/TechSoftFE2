import * as md from '.';

export interface PagoTransferencia extends md.Pago {
  cuenta: string;
  banco: md.Banco;
}