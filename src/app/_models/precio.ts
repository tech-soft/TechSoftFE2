import * as md from '.';

export interface Precio {
  id: number;
  lista: md.PrecioLista;
  renta: number;
  precioFinal: number;
}
