
export interface Moneda {
  id: number;
  descripcion: string;
  simbolo: string;
}