import * as md from '.';

export interface Contacto {
  id: number;
  tipo: number;
  contacto: String;
  descripcion: String;
  personaId: number;
}
