import * as md from '.';

export interface Pago {
  id: number;
  proveedor: md.Proveedor;
  forma: md.PagoForma;
  moneda: md.Moneda;
  fechaEmision: string;
  importe: number;
  descripcion: string;
}