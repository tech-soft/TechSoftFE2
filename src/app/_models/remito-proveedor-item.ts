import * as md from '.';

export interface RemitoProveedorItem {
  id: number;
  producto: md.ProductoPreview;
  cantidad: number;
  numerosSerie: String[];
  ordenCompra: md.OrdenCompraEmitida;
  cantidadOrdenCompra: number;
}