import * as md from '.';

export interface Costo {
  id: number;
  articulo: md.Articulo;
  proveedor: md.Persona;
  fecha: string;
  costo: number;
  cantidad: number;
}
