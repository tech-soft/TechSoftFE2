import * as md from '.';

export interface Provincia {
  id: number;
  nombre: String;
  pais: md.Pais;
}
