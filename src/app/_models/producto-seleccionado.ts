export interface ProductoSeleccionado {
  id: number;
  descripcion: string;
  cantidad: number;
}