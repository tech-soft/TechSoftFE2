import * as md from '.';

export interface PersonaComercial {
  id: number;
  personaId: number;
  ivaTipo: md.IvaTipo;
  pagoForma: md.PagoForma;
  precioLista: md.PrecioLista;
  descuento: number;
}
