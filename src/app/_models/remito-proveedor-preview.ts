import * as md from '.';

export interface RemitoProveedorPreview {
  id: number;
  puestoId: number;
  numero: number;
  proveedor: md.Proveedor;
  fecha: string;
}
