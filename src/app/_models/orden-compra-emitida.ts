import * as md from '.';

export interface OrdenCompraEmitida {
  id: number;
  puesto: number;
  numero: number;
  seleccionada: boolean;
}
