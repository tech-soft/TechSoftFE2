import * as md from '.';

export interface OrdenCompraItem {
  id: number;
  producto: md.ProductoPreview;
  cantidad: number;
  precio: number;
  estado: md.Estado;
}
