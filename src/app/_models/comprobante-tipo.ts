
export interface ComprobanteTipo {
  id: number;
  letra: string;
  tipo: number;
}