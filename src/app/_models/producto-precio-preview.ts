
export interface ProductoPrecioPreview {
    id: number;
    descripcion: string;
    stockEntrante: number;
    stockDisponible: number;
    stockTransito: number;
    lista1SinIv: number;
    lista1ConIva: number;
    lista2SinIva: number;
    lista2ConIva: number;
    lista3SinIva: number;
    lista3ConIva: number;
    lista4SinIva: number;
    lista4ConIva: number;
    lista5SinIva: number;
    lista5ConIva: number;
    lista6SinIva: number;
    lista6ConIva: number;
  }