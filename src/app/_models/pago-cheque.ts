import * as md from '.';

export interface PagoCheque extends md.Pago {
  emisor: md.Persona;
  numero: string;
  banco: md.Banco; 
  fechaPago: string;
}