import * as md from '.';

export interface Producto {
  id: number;
  descripcion: String;
  descripcionReducida: String;
  codigoBarras: String;
  gravado: md.Gravado;
  origen: md.Origen;
  estado: md.Estado;
}
