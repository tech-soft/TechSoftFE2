
export enum MessageType {
  success = 'success',
  error = 'error',
  warning = 'warning',
  info = 'info'
}

export interface Message {
  message: String;
  type: MessageType;
}