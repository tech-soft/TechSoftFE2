import * as md from '.';

export interface OrdenCompra {
  id: number;
  puesto: md.Puesto;
  numero: number;
  proveedor: md.Proveedor;
  items: md.OrdenCompraItem[];
  fechaCreacion: string;
  fechaEntrega: string;
  estado: md.Estado;
  observaciones: string;
}
