export interface ProductoStock {
    productoId: number;
    stockEntrante: number;
    stockDisponible: number;
    stockTransito: number;
    stockReservado: number;
    stockVendido: number;
}