import * as md from '.';

export interface FacturaProveedorPreview {
  id: number;
  puesto: number;
  numero: number;
  proveedor: md.Proveedor;
  fecha: string;
}