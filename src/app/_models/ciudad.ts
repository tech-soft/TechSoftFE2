import * as md from '.';

export interface Ciudad {
  id: number;
  nombre: String;
  codigoPostal: String;
  provincia: md.Provincia;
}
