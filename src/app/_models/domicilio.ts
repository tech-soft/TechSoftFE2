import * as md from '.';

export interface Domicilio {
  id: number;
  personaId: number;
  calle: String;
  nro: String;
  piso: String;
  dpto: String;
  ciudad: md.Ciudad;
  descripcion: String;
  principal: boolean;
}
