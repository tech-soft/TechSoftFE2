import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { ComprasRoutingModule } from './compras-routing.module';
import { FacturasProveedorCrudComponent } from './facturas-proveedor-crud/facturas-proveedor-crud.component';
import { FacturasProveedorSearchComponent } from './facturas-proveedor-search/facturas-proveedor-search.component';
import { OrdenComprasCrudComponent } from './orden-compras-crud/orden-compras-crud.component';
import { OrdenComprasSearchComponent } from './orden-compras-search/orden-compras-search.component';
import { PagosSearchComponent } from './pagos-search/pagos-search.component';
import { RemitosProveedorCrudComponent } from './remitos-proveedor-crud/remitos-proveedor-crud.component';
import { RemitosProveedorSearchComponent } from './remitos-proveedor-search/remitos-proveedor-search.component';

@NgModule({
  declarations: [
    FacturasProveedorCrudComponent,
    FacturasProveedorSearchComponent,
    OrdenComprasCrudComponent,
    OrdenComprasSearchComponent,
    PagosSearchComponent,
    RemitosProveedorSearchComponent,
    RemitosProveedorCrudComponent,
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FontAwesomeModule,
    FormsModule,
    NgbModule.forRoot(),
    ComprasRoutingModule,
    SharedModule
  ],
  exports: [
    OrdenComprasCrudComponent,
    OrdenComprasSearchComponent,
    RemitosProveedorSearchComponent,
    RemitosProveedorCrudComponent,
    FacturasProveedorSearchComponent,
    FacturasProveedorCrudComponent,
    PagosSearchComponent
  ],
  providers: [
  ]
})
export class ComprasModule { }
