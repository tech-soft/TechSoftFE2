import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';

import * as md from '../../_models';
import { NgbDateStruct, NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CompraService } from '../../shared/services/compra.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { environment } from '../../../environments/environment';
import { MessageService } from 'src/app/shared/services/message.service';
import { ConvertersService } from 'src/app/shared/utils/converters.service';

@Component({
  selector: 'app-orden-compras-crud',
  templateUrl: './orden-compras-crud.component.html',
  styleUrls: ['./orden-compras-crud.component.scss']
})
export class OrdenComprasCrudComponent implements OnInit {

  title: string;

  proveedores: md.Proveedor[];

  ocId: number;
  ordenCompra: md.OrdenCompra = <md.OrdenCompra>{ "items": [] };

  readOnly: boolean;
  hasOrdenCompra: boolean;

  model: NgbDateStruct;
  dateString: string;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private compraService: CompraService,
    private messageService: MessageService,
    private ordenCompraService: OrdenCompraService,
    private convertersService: ConvertersService
  ) {
    this.readOnly = false;
    this.ocId = 0;
    this.hasOrdenCompra = false;
    this.title = "Gestión de Órdenes de Compra";
  }

  // TODO: agregar spinner cuando esta esperando (init, save, etc)
  // TODO: switchMap to change the router param
  // TODO: boton para limpiar fecha de entrega
  // TODO: estilo del datepicker

  ngOnInit(): void {
    this.model = this.convertersService.setDefaultDate();
    this.onSelectDate(this.model);
    this.getProveedores();
    this.getCRUDStatus();
  }

  // Items
  agregarProducto(producto: md.ProductoSeleccionado): void {
    if (typeof producto == 'object') {
      let tempItem: md.OrdenCompraItem = <md.OrdenCompraItem>{ "producto": <md.ProductoPreview>{} };
      tempItem.id = this.ordenCompra.items.length + 1;
      tempItem.producto.id = producto.id;
      tempItem.producto.descripcion = producto.descripcion;
      tempItem.cantidad = producto.cantidad;
      tempItem.estado = {
        id: 1100,
        descripcion: "ITEM_NUEVO"
      }
      this.ordenCompra.items.push(tempItem);
    }
  }

  quitarProducto(itemAQuitar: md.OrdenCompraItem): void {
    this.ordenCompra.items = this.ordenCompra.items.filter(item => item !== itemAQuitar);
  }

  // OC
  clonar() {    
    this.messageService.clear();

    this.getPuesto();
    this.ordenCompra.id = undefined;
    this.ordenCompra.numero = undefined;
    this.ordenCompra.estado = undefined;
    this.ordenCompra.fechaCreacion = new Date().toISOString().split("T")[0];

    this.ocId = 0;
    this.readOnly = false
    this.hasOrdenCompra = false
  }

  emitir() {
    this.messageService.clear();

    this.ordenCompraService.emitirOrdenCompra(this.ordenCompra)
      .subscribe(
        result => {
          if (result) {
            this.messageService.addSuccess("La orden de compra fue emitida con éxito.");
            this.ocId = result.id;
            this.readOnly = true;
            this.getCRUDStatus();
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  // Form
  getStatus(currentStatus: string): void {
    switch (currentStatus) {
      case 'edit':
        this.edit();
        break;
      case 'delete':
        this.eliminarOrdenCompra();
        break;
      case 'save':
        this.guardarOrdenCompra();
        break;
      case 'cancel':
        this.cancel();
        break;
      default:
        break;
    }
  }

  crudHabilitado(): boolean {
    if (!this.hasOrdenCompra || (this.hasOrdenCompra && 
      this.ordenCompra && this.ordenCompra.estado && this.ordenCompra.estado.id == environment.ESTADOS_OC.NUEVA)){
      return true;
    } else {
      return false;
    }
  }

  validForm(showAlerts: boolean): boolean {

    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.ordenCompra.proveedor) {
      if (showAlerts) {
        this.messageService.addError("Seleccione un proveedor.");
      }
      valido = false;
    }
    if (!this.ordenCompra.items || this.ordenCompra.items.length === 0) {
      if (showAlerts) {
        this.messageService.addError("La orden debe tener al menos un item.");
      }
      valido = false;
    }

    return valido;
  }

  onSelectDate(date: NgbDateStruct) {
    if (date != null && date.year > date.day) {
      this.model = date;
      this.dateString =  this.convertersService.onSelectDate(this.model);      
    } else {
      this.dateString = undefined;
    }

    if (this.dateString) {
      this.ordenCompra.fechaEntrega = this.dateString;
    } else {
      this.ordenCompra.fechaEntrega = undefined;
    }
  }

  // Init
  private getProveedores(): void {
    this.compraService.getProveedoresActivos()
      .subscribe(
        result => {
          this.proveedores = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getPuesto(): void {
    this.compraService.getPuestoActual()
      .subscribe(
        result => {
          this.ordenCompra.puesto = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getCRUDStatus(): void {
    // TODO: should test this
    this.hasOrdenCompra =
      this.route.snapshot.paramMap.get('id') != undefined
      && this.route.snapshot.paramMap.get('id') !== '0';

    if (this.hasOrdenCompra) {
      this.readOnly = true;
      this.ocId = +this.route.snapshot.paramMap.get('id');
      this.getOrdenCompra();

    } else {
      this.readOnly = false;
      this.ocId = 0;
      this.getPuesto();
      this.ordenCompra.fechaCreacion = new Date().toISOString().split("T")[0];

    }
  }

  // OC
  private getOrdenCompra(): void {
    if (this.ocId > 0) {
      this.ordenCompraService.getOrdenCompra(this.ocId)
        .subscribe(
          result => {
            this.setOrdenCompra(result);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private setOrdenCompra(ordenCompra: md.OrdenCompra): void {
    this.ordenCompra = ordenCompra;
    if (ordenCompra.fechaEntrega) {
      let fechaEntrega: Date = new Date(ordenCompra.fechaEntrega);
      this.model = { day: fechaEntrega.getUTCDate(), month: fechaEntrega.getUTCMonth() + 1, year: fechaEntrega.getUTCFullYear() };
    }
  }

  private guardarOrdenCompra(): void {
    if (this.validForm(true)) {

      this.onSelectDate(this.model);      

      (this.hasOrdenCompra) ? this.actualizarOrdenCompra() : this.insertarOrdenCompra();
    }
  }

  private insertarOrdenCompra(): void {
    this.ordenCompraService.postOrdenCompra(this.ordenCompra)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha generado la orden de compra con número " + result.numero);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó la orden de compra. Verifique que la misma se haya generado correctamente.");
          }

        },
        error => {
          this.messageService.addError(`${error.message}`);
          if (!this.hasOrdenCompra) {
            this.ordenCompra.estado = undefined;
          }
          return throwError(error);
        }
      );
  }

  private actualizarOrdenCompra(): void {
    this.ordenCompraService.putOrdenCompra(this.ordenCompra)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha actualizado la orden de compra con número " + result.numero);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó la orden de compra. Verifique que la misma se haya generado correctamente.");
          }

        },
        error => {
          this.messageService.addError(`${error.message}`);
          if (!this.hasOrdenCompra) {
            this.ordenCompra.estado = undefined;
          }
          return throwError(error);
        }
      );
  }

  private initAfterSave(ordenId: number): void {
    this.ocId = ordenId;
    this.getOrdenCompra();

    this.readOnly = true;
    this.hasOrdenCompra = true;
    this.router.navigate([`/orden-compras/crud/${this.ocId}`]);
  }

  private eliminarOrdenCompra(): void {
    // TODO: confirm msg
    if (this.ordenCompra && this.ordenCompra.id) {
      this.ordenCompraService.deleteOrdenCompra(this.ordenCompra.id)
        .subscribe(
          result => {
            this.messageService.addSuccess("Se ha eliminado con éxito.");
            this.router.navigate(['/orden-compras/search']);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  // Form
  private edit(): void {
    this.readOnly = false;
    this.hasOrdenCompra = true;
  };

  private cancel(): void {
    this.getOrdenCompra();
    this.readOnly = true;
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    // TODO: se pierde el mensaje de confirmacion despues de insertar/eliminar nueva porque pasa por aca
    this.messageService.clear();
  }

}
