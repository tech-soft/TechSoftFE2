import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';
import { CrudButtonsComponent } from 'src/app/shared/crud-buttons/crud-buttons.component';
import { MessageService } from 'src/app/shared/services/message.service';
import { ProductosService } from 'src/app/shared/services/productos.service';
import { TypeAheadProductosComponent } from 'src/app/shared/ui-controls/type-ahead-productos/type-ahead-productos.component';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { CompraService } from '../../shared/services/compra.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { OrdenComprasCrudComponent } from './orden-compras-crud.component';

class CompraServiceMock {
  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return of(undefined);
  }
  public getPuestoActual(): Observable<md.Puesto> {
    return of(undefined);
  }
}

class OrdenCompraServiceMock {
  public getOrdenCompra(id: number): Observable<md.OrdenCompra> {
    return of(undefined);
  }
  public postOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return of(undefined);
  }
  public putOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return of(undefined);
  }
  public deleteOrdenCompra(id: number): Observable<md.OrdenCompra> {
    return of(undefined);
  }
  public emitirOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return of(undefined);
  }
}

class ProductosServiceMock {
  public getProductosPreview(): Observable<md.ProductoPreview[]> {
    return of(undefined);
  }
}

describe('OrdenComprasCrudComponent', () => {
  let component: OrdenComprasCrudComponent;
  let fixture: ComponentFixture<OrdenComprasCrudComponent>;
  let compraService: CompraService;
  let messageService: MessageService;
  let ordenCompraService: OrdenCompraService;

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrdenComprasCrudComponent,
        TypeAheadProductosComponent,
        CrudButtonsComponent
      ],
      imports: [
        FormsModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule.forRoot()
      ],
      providers: [
        { provide: CompraService, useClass: CompraServiceMock },
        { provide: OrdenCompraService, useClass: OrdenCompraServiceMock },
        { provide: ProductosService, useClass: ProductosServiceMock },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: convertToParamMap({ id: undefined }) } }
        },
        MessageService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(OrdenComprasCrudComponent);
    component = fixture.componentInstance;
    compraService = TestBed.get(CompraService);
    messageService = TestBed.get(MessageService);
    ordenCompraService = TestBed.get(OrdenCompraService);
    fixture.detectChanges();
  }));

  it('se debe inicializar valores en el constructor', fakeAsync(() => {
    expect(component.readOnly).toBeFalsy();
    expect(component.ocId).toBe(0);
    expect(component.hasOrdenCompra).toBeFalsy();
    expect(component.title).toBe("Gestión de Órdenes de Compra");
  }));

  it('ngOnInit debe llamar a getProveedoresActivos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(compraService.getProveedoresActivos).toHaveBeenCalled();
  }));

  it('getProveedoresActivos debe devolver error cuando falla', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('ngOnInit debe llamar a getCRUDStatus y definir estados segun CRUD status', fakeAsync(() => {
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(component.hasOrdenCompra).toBeFalsy();
    expect(component.readOnly).toBeFalsy();
    expect(component.ocId).toBe(0);
  }));

  it('agregarProducto debe agregar elemento del array productos', fakeAsync(() => {
    let productoSeleccionado = { id: 1, descripcion: 'Test push new object', cantidad: 1 };
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(component.ordenCompra.items.length).toBe(0);
    component.agregarProducto(productoSeleccionado);

    tick();
    fixture.detectChanges();
    expect(component.ordenCompra.items.length).toBe(1);
    expect(component.ordenCompra.items.some(item => item.producto.descripcion === productoSeleccionado.descripcion)).toBeTruthy();
  }));

  it('quitarProducto debe eliminar elemento del array productos', fakeAsync(() => {
    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[0]);
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(component.ordenCompra.items.length).toBe(mk.ORDENES_COMPRA[0].items.length);
    expect(component.ordenCompra.items.some(item => item === mk.ORDENES_COMPRA[0].items[0])).toBeTruthy();
    component.quitarProducto(mk.ORDENES_COMPRA[0].items[0]);

    tick();
    fixture.detectChanges();
    expect(component.ordenCompra.items.length).toBe(mk.ORDENES_COMPRA[0].items.length - 1);
    expect(component.ordenCompra.items.some(item => item === mk.ORDENES_COMPRA[0].items[0])).toBeFalsy();
  }));

  it('guardarOrdenCompra debe llamar a postOrdenCompra OrdenCompraService si la orden es válida y hasOrdenCompra false', fakeAsync(() => {
    spyOn<any>(component, 'validForm').and.returnValue(true);
    spyOn(ordenCompraService, 'postOrdenCompra').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasOrdenCompra = false;
    component.guardarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.postOrdenCompra).toHaveBeenCalled();

    ordenCompraService.postOrdenCompra(mk.ORDENES_COMPRA[0]).subscribe(
      result => {
        expect(component.readOnly).toBeTruthy();
        expect(component.hasOrdenCompra).toBeTruthy();
      }
    );
  }));

  it('guardarOrdenCompra debe llamar a putOrdenCompra OrdenCompraService si la orden es válida y hasOrdenCompra true', fakeAsync(() => {
    spyOn<any>(component, 'validForm').and.returnValue(true);
    spyOn(ordenCompraService, 'putOrdenCompra').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasOrdenCompra = true;
    component.guardarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.putOrdenCompra).toHaveBeenCalled();

    ordenCompraService.putOrdenCompra(mk.ORDENES_COMPRA[0]).subscribe(
      result => {
        expect(component.readOnly).toBeTruthy();
        expect(component.hasOrdenCompra).toBeTruthy();
      }
    );
  }));

  it('guardarOrdenCompra no debe llamar a postOrdenCompra ni a putOrdenCompra si la orden no es válida', fakeAsync(() => {
    spyOn<any>(component, 'validForm').and.returnValue(false);
    spyOn(ordenCompraService, 'postOrdenCompra').and.returnValue(of());
    spyOn(ordenCompraService, 'putOrdenCompra').and.returnValue(of());
    component.ngOnInit();
    component.guardarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.postOrdenCompra).not.toHaveBeenCalled();
    expect(ordenCompraService.putOrdenCompra).not.toHaveBeenCalled();
  }));

  it('guardarOrdenCompra debe devolver error cuando falla el post', fakeAsync(() => {
    spyOn<any>(component, 'validForm').and.returnValue(true);
    spyOn(ordenCompraService, 'postOrdenCompra').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasOrdenCompra = false;
    component.guardarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.postOrdenCompra).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('guardarOrdenCompra deben devolver error cuando falla el put', fakeAsync(() => {
    spyOn<any>(component, 'validForm').and.returnValue(true);
    spyOn(ordenCompraService, 'putOrdenCompra').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasOrdenCompra = true;
    component.guardarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.putOrdenCompra).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('eliminarOrdenCompra debe llamar a deleteOrdenCompra ordenCompraService si la orden es válido', fakeAsync(() => {
    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[0]);
    spyOn(ordenCompraService, 'deleteOrdenCompra').and.returnValue(of());
    component.ngOnInit();
    component.eliminarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.deleteOrdenCompra).toHaveBeenCalled();
  }));

  it('eliminarOrdenCompra no debe llamar a deleteOrdenCompra si la orden no es válido', fakeAsync(() => {
    spyOn(ordenCompraService, 'deleteOrdenCompra').and.returnValue(of());
    component.ngOnInit();
    component.eliminarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.deleteOrdenCompra).not.toHaveBeenCalled();
  }));

  it('eliminarOrdenCompra deben devolver error cuando falla', fakeAsync(() => {
    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[0]);
    spyOn(ordenCompraService, 'deleteOrdenCompra').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();
    component.eliminarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.deleteOrdenCompra).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);

  }));

  it('edit debe setear variables para editar', fakeAsync(() => {
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.edit();

    tick();
    fixture.detectChanges();
    expect(component.readOnly).toBeFalsy();
    expect(component.hasOrdenCompra).toBeTruthy();
  }));

  it('cancel debe setear variables', fakeAsync(() => {
    spyOn(ordenCompraService, 'getOrdenCompra').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.ocId = 1;
    component.cancel();

    tick();
    fixture.detectChanges();
    expect(component.readOnly).toBeTruthy();
    expect(ordenCompraService.getOrdenCompra).toHaveBeenCalled();

  }));

  it('clonar debe crear nueva oc con los datos de la existente', fakeAsync(() => {
    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[1]);
    spyOn(compraService, 'getPuestoActual').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.clonar();

    tick();
    fixture.detectChanges();
    expect(compraService.getPuestoActual).toHaveBeenCalled();
    expect(component.ordenCompra.id).toBeUndefined();
    expect(component.ordenCompra.numero).toBeUndefined();
    expect(component.ordenCompra.estado).toBeUndefined();
    expect(component.ordenCompra.fechaCreacion).toBe(new Date().toISOString().split("T")[0]);
    expect(component.ocId).toBe(0);
    expect(component.readOnly).toBeFalsy();
    expect(component.hasOrdenCompra).toBeFalsy();
  }));

  it('emitir debe llamar a emitirOrdenCompra en ordenCompraService', fakeAsync(() => {
    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[1]);
    spyOn(ordenCompraService, 'emitirOrdenCompra').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.emitir();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.emitirOrdenCompra).toHaveBeenCalled();

    ordenCompraService.emitirOrdenCompra(mk.ORDENES_COMPRA[1]).subscribe(
      result => {
        expect(component.readOnly).toBeTruthy();
        expect(component.ocId).toBe(result.id);
      }
    );

    component.ordenCompra = Object.assign({}, mk.ORDENES_COMPRA[0]);
    spyOn(ordenCompraService, 'deleteOrdenCompra').and.returnValue(of());
    component.ngOnInit();
    component.eliminarOrdenCompra();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.deleteOrdenCompra).toHaveBeenCalled();
  }));

  it('getStatus debe seleccionar accion de acuerdo a string enviado', fakeAsync(() => {
    spyOn(component, 'edit');
    spyOn(component, 'eliminarOrdenCompra');
    spyOn(component, 'guardarOrdenCompra');
    spyOn(component, 'cancel');
    component.ngOnInit();
    component.getStatus('edit');

    tick();
    fixture.detectChanges();
    expect(component.edit).toHaveBeenCalled();
    component.getStatus('delete');

    tick();
    fixture.detectChanges();
    expect(component.eliminarOrdenCompra).toHaveBeenCalled();
    component.getStatus('save');

    tick();
    fixture.detectChanges();
    expect(component.guardarOrdenCompra).toHaveBeenCalled();
    component.getStatus('cancel');

    tick();
    fixture.detectChanges();
    expect(component.cancel).toHaveBeenCalled();
  }));

  it('ngOnDestroy debe limpiar los mensajes', fakeAsync(() => {
    spyOn(messageService, 'clear');
    component.ngOnInit();
    component.ngOnDestroy();

    tick();
    fixture.detectChanges();
    expect(messageService.messages.length).toBe(0);
    expect(messageService.clear).toHaveBeenCalled();
  }));
});
