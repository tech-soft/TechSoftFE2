import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { throwError } from 'rxjs';

import * as md from '../../_models';
import { CompraService } from '../../shared/services/compra.service';
import { MessageService } from '../../shared/services/message.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { ConvertersService } from '../../shared/utils/converters.service';

@Component({
  selector: 'app-orden-compras-search',
  templateUrl: './orden-compras-search.component.html',
  styleUrls: ['./orden-compras-search.component.scss']
})
export class OrdenComprasSearchComponent implements OnInit {

  title: string;

  proveedores: md.Proveedor[];
  puestos: md.Puesto[];
  estados: md.Estado[];
  ordenes: md.OrdenCompraPreview[];

  proveedorSeleccionado: md.Proveedor;
  fechaSeleccionada: NgbDateStruct;
  puestoSeleccionado: md.Puesto;
  numeroSeleccionado: number;
  estadoSeleccionado: md.Estado;

  dateString: string;

  // TODO: spinner
  // TODO: en html agregar pipe para ver el puesto 0001 en vez de 1
  // TODO: datepicker style

  constructor(
    private router: Router,
    private compraService: CompraService,
    private messageService: MessageService,
    private ordenCompraService: OrdenCompraService,
    private convertersService: ConvertersService
  ) {
    this.title = "Búsqueda de Órdenes de Compra";
  }

  ngOnInit(): void {
    this.fechaSeleccionada = this.convertersService.setDefaultDate();
    this.onSelectDate(this.fechaSeleccionada);
    this.getProveedores();
    this.getEstadosOC();
    this.getPuestos();
  }

  buscar(): void {
    this.onSelectDate(this.fechaSeleccionada);

    this.ordenCompraService.getOrdenesCompra(
      (this.proveedorSeleccionado) ? this.proveedorSeleccionado.id.toString() : undefined,
      this.dateString ? this.convertersService.ngbDateStructToEpochString(this.fechaSeleccionada) : undefined,
      (this.puestoSeleccionado) ? this.puestoSeleccionado.id.toString() : undefined,
      (this.numeroSeleccionado) ? this.numeroSeleccionado.toString() : undefined,
      (this.estadoSeleccionado) ? this.estadoSeleccionado.id.toString() : undefined
    )
      .subscribe(
        result => {
          this.ordenes = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  limpiar(): void {
    this.ordenes = undefined;
    this.proveedorSeleccionado = undefined;
    this.fechaSeleccionada = undefined;
    this.puestoSeleccionado = undefined;
    this.numeroSeleccionado = undefined;
    this.estadoSeleccionado = undefined;
  }

  goto(menuOption: String, id: number): void {
    switch (menuOption) {
      case 'OrdenCompraCrudAdd':
        this.router.navigate(['/orden-compras/crud']);
        break;
      case 'OrdenCompraCrudEdit':
        this.router.navigate([`/orden-compras/crud/${id}`]);
        break;
      default:
        break;
    }
  }

  onSelectDate(date: NgbDateStruct) {
    if (date != null && date.year > date.day) {
      this.fechaSeleccionada = date;
      this.dateString = this.convertersService.onSelectDate(this.fechaSeleccionada);
    } else {
      this.dateString = undefined;
    }
  }

  private getProveedores(): void {
    this.compraService.getProveedoresActivos()
      .subscribe(
        result => {
          this.proveedores = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  private getEstadosOC(): void {
    this.ordenCompraService.getEstadosOC()
      .subscribe(
        result => {
          this.estados = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  private getPuestos(): void {
    this.compraService.getPuestosActivos()
      .subscribe(
        result => {
          this.puestos = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
