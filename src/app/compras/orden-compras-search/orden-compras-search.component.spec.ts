import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { CompraService } from '../../shared/services/compra.service';
import { MessageService } from '../../shared/services/message.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { OrdenComprasSearchComponent } from './orden-compras-search.component';

class CompraServiceMock {
  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return of(undefined);
  }

  public getPuestosActivos(): Observable<md.Puesto[]> {
    return of(undefined);
  }
}

class OrdenCompraServiceMock {
  public getOrdenesCompra(proveedorId: string, fechaCreacion: string, puestoId: string, numero: string, estadoId: string): Observable<md.OrdenCompraPreview[]> {
    return of(undefined);
  }

  public getEstadosOC(): Observable<md.Estado[]> {
    return of(undefined);
  }
}

describe('OrdenComprasSearchComponent', () => {

  let component: OrdenComprasSearchComponent;
  let fixture: ComponentFixture<OrdenComprasSearchComponent>;
  let compraService: CompraService;
  let messageService: MessageService;
  let ordenCompraService: OrdenCompraService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        OrdenComprasSearchComponent
      ],
      imports: [
        FormsModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        { provide: CompraService, useClass: CompraServiceMock },
        { provide: OrdenCompraService, useClass: OrdenCompraServiceMock },
        MessageService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(OrdenComprasSearchComponent);
    component = fixture.componentInstance;
    compraService = TestBed.get(CompraService);
    messageService = TestBed.get(MessageService);
    ordenCompraService = TestBed.get(OrdenCompraService);
    fixture.detectChanges();
  }));

  it('ngOnInit debe obtener proveedores activos, puestos según la sucursal actual y estados de órdenes de compra',
    fakeAsync(() => {
      spyOn(compraService, 'getProveedoresActivos').and.returnValue(of());
      spyOn(ordenCompraService, 'getEstadosOC').and.returnValue(of());
      spyOn(compraService, 'getPuestosActivos').and.returnValue(of());
      component.ngOnInit();

      tick();
      fixture.detectChanges();
      expect(compraService.getProveedoresActivos).toHaveBeenCalled();
      expect(ordenCompraService.getEstadosOC).toHaveBeenCalled();
      expect(compraService.getPuestosActivos).toHaveBeenCalled();
    }));

  it('buscar debe llamar a getOrdenesCompra', fakeAsync(() => {
    spyOn(ordenCompraService, 'getOrdenesCompra').and.returnValue(of());
    component.ngOnInit();
    component.buscar();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.getOrdenesCompra).toHaveBeenCalled();
  }));

  it('limpiar debe dejar variables undefined', fakeAsync(() => {
    component.ordenes = Object.assign({}, mk.ORDENES_COMPRA_PREVIEW);
    component.proveedorSeleccionado = Object.assign({}, mk.PROVEEDORES[0]);
    let date = new Date();
    component.fechaSeleccionada = { day: date.getDate(), month: date.getMonth(), year: date.getFullYear() };
    component.puestoSeleccionado = Object.assign({}, mk.PUESTOS[0]);
    component.numeroSeleccionado = 1234;
    component.estadoSeleccionado = Object.assign({}, mk.ESTADOS_ORDENES[0]);
    component.ngOnInit();
    component.limpiar();

    tick();
    fixture.detectChanges();
    expect(component.ordenes).toBeUndefined();
    expect(component.proveedorSeleccionado).toBeUndefined();
    expect(component.fechaSeleccionada).toBeUndefined();
    expect(component.puestoSeleccionado).toBeUndefined();
    expect(component.estadoSeleccionado).toBeUndefined();
    expect(component.numeroSeleccionado).toBeUndefined();
  }));

  it('OnDestroy debe eliminar los mensajes de error', fakeAsync(() => {
    spyOn(ordenCompraService, 'getOrdenesCompra').and.returnValue(throwError({}));
    spyOn(messageService, 'clear').and.returnValue(of());
    component.ngOnInit();
    component.ngOnDestroy();

    tick();
    fixture.detectChanges();
    expect(messageService.clear).toHaveBeenCalled();
  }));

  // errores al fallar llamadas al service
  it('debe mostrarse un error si falla getProveedoresActivos',
    fakeAsync(() => {
      spyOn(compraService, 'getProveedoresActivos').and.returnValue(throwError({}));
      spyOn(messageService, 'addError').and.returnValue(of());
      component.ngOnInit();

      tick();
      fixture.detectChanges();
      expect(messageService.addError).toHaveBeenCalledTimes(1);
    }));

  it('debe mostrarse un error si falla getPuestosActivos',
    fakeAsync(() => {
      spyOn(compraService, 'getPuestosActivos').and.returnValue(throwError({}));
      spyOn(messageService, 'addError').and.returnValue(of());
      component.ngOnInit();

      tick();
      fixture.detectChanges();
      expect(messageService.addError).toHaveBeenCalledTimes(1);
    }));

  it('debe mostrarse un error si falla getOrdenesCompra',
    fakeAsync(() => {
      spyOn(ordenCompraService, 'getOrdenesCompra').and.returnValue(throwError({}));
      spyOn(messageService, 'addError').and.returnValue(of());
      component.ngOnInit();

      tick();
      fixture.detectChanges();
      expect(messageService.addError).toHaveBeenCalledTimes(1);
    }));

  it('debe mostrarse un error si falla getEstadosOC',
    fakeAsync(() => {
      spyOn(ordenCompraService, 'getEstadosOC').and.returnValue(throwError({}));
      spyOn(messageService, 'addError').and.returnValue(of());
      component.ngOnInit();

      tick();
      fixture.detectChanges();
      expect(messageService.addError).toHaveBeenCalledTimes(1);
    }));
});
