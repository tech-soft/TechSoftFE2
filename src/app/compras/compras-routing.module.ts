import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../_guards/auth.guard';
import { FacturasProveedorCrudComponent } from './facturas-proveedor-crud/facturas-proveedor-crud.component';
import { FacturasProveedorSearchComponent } from './facturas-proveedor-search/facturas-proveedor-search.component';
import { OrdenComprasCrudComponent } from './orden-compras-crud/orden-compras-crud.component';
import { OrdenComprasSearchComponent } from './orden-compras-search/orden-compras-search.component';
import { PagosSearchComponent } from './pagos-search/pagos-search.component';
import { RemitosProveedorCrudComponent } from './remitos-proveedor-crud/remitos-proveedor-crud.component';
import { RemitosProveedorSearchComponent } from './remitos-proveedor-search/remitos-proveedor-search.component';

const routes: Routes = [
  {
    path: 'orden-compras/crud',
    component: OrdenComprasCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orden-compras/crud/:id',
    component: OrdenComprasCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'orden-compras/search',
    component: OrdenComprasSearchComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/remitos/crud',
    component: RemitosProveedorCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/remitos/crud/:id',
    component: RemitosProveedorCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/remitos/search',
    component: RemitosProveedorSearchComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/facturas/crud',
    component: FacturasProveedorCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/facturas/crud/:id',
    component: FacturasProveedorCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/facturas/search',
    component: FacturasProveedorSearchComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'proveedores/pagos/search',
    component: PagosSearchComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class ComprasRoutingModule { }
