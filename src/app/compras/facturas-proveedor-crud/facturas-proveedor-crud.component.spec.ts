import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FacturasProveedorCrudComponent } from './facturas-proveedor-crud.component';

describe('FacturasProveedorCrudComponent', () => {
  let component: FacturasProveedorCrudComponent;
  let fixture: ComponentFixture<FacturasProveedorCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FacturasProveedorCrudComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FacturasProveedorCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
