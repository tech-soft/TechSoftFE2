import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { throwError } from 'rxjs';

import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { ConvertersService } from '../../shared/utils/converters.service';
import { CompraService } from '../../shared/services/compra.service';
import { RemitoProveedorService } from '../../shared/services/remito-proveedor.service';

@Component({
  selector: 'app-remitos-proveedor-search',
  templateUrl: './remitos-proveedor-search.component.html',
  styleUrls: ['./remitos-proveedor-search.component.scss']
})
export class RemitosProveedorSearchComponent implements OnInit {

  title: string;

  proveedores: md.Proveedor[];
  remitos: md.RemitoProveedorPreview[];

  proveedorSeleccionado: md.Proveedor;
  fechaSeleccionada: NgbDateStruct;
  numeroSeleccionado: number;

  dateString: string;

  // TODO: spinner
  // TODO: formatear puesto para 1 -> 0001
  
  constructor(
    private router: Router,
    private compraService: CompraService,
    private messageService: MessageService,
    private remitoProveedorService: RemitoProveedorService,
    private convertersService: ConvertersService
  ) {
    this.title = "Búsqueda de Remitos";
  }

  ngOnInit(): void {
    this.fechaSeleccionada = this.convertersService.setDefaultDate();
    this.onSelectDate(this.fechaSeleccionada);
    this.getProveedores();
  }

  buscar(): void {
    this.onSelectDate(this.fechaSeleccionada);

    this.remitoProveedorService.getRemitos(
      (this.proveedorSeleccionado) ? this.proveedorSeleccionado.id.toString() : undefined,
      this.dateString ? this.convertersService.ngbDateStructToEpochString(this.fechaSeleccionada) : undefined,
      (this.numeroSeleccionado) ? this.numeroSeleccionado.toString() : undefined
    )
      .subscribe(
        result => {
          this.remitos = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  limpiar(): void {
    this.remitos = undefined;
    this.proveedorSeleccionado = undefined;
    this.fechaSeleccionada = undefined;
    this.numeroSeleccionado = undefined;
  }

  goto(menuOption: String, id: number): void {
    switch (menuOption) {
      case 'RemitoCrudAdd':
        this.router.navigate(['/proveedores/remitos/crud']);
        break;
      case 'RemitoCrudEdit':
        this.router.navigate([`/proveedores/remitos/crud/${id}`]);
        break;
      default:
        break;
    }
  }

  onSelectDate(date: NgbDateStruct) {
    if (date != null && date.year > date.day) {
      this.fechaSeleccionada = date;
      this.dateString =  this.convertersService.onSelectDate(this.fechaSeleccionada);      
    } else {
      this.dateString = undefined;
    }
  }

  private getProveedores(): void {
    this.compraService.getProveedoresActivos()
      .subscribe(
        result => {
          this.proveedores = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
