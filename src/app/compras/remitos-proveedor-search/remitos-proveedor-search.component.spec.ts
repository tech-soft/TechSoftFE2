import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { CompraService } from '../../shared/services/compra.service';
import { RemitoProveedorService } from '../../shared/services/remito-proveedor.service';
import { RemitosProveedorSearchComponent } from './remitos-proveedor-search.component';

class CompraServiceMock {
  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return of(undefined);
  }
}

class RemitoServiceMock {
  public getRemitos(proveedorId: string, fecha: string, numero: string): Observable<md.RemitoProveedorPreview[]> {
    return of(undefined);
  }
}

describe('RemitosProveedorSearchComponent', () => {

  let component: RemitosProveedorSearchComponent;
  let fixture: ComponentFixture<RemitosProveedorSearchComponent>;
  let compraService: CompraService;
  let messageService: MessageService;
  let remitoProveedorService: RemitoProveedorService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RemitosProveedorSearchComponent
      ],
      imports: [
        FormsModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        { provide: CompraService, useClass: CompraServiceMock },
        { provide: RemitoProveedorService, useClass: RemitoServiceMock },
        MessageService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RemitosProveedorSearchComponent);
    component = fixture.componentInstance;
    compraService = TestBed.get(CompraService);
    messageService = TestBed.get(MessageService);
    remitoProveedorService = TestBed.get(RemitoProveedorService);
    fixture.detectChanges();
  }));

  it('ngOnInit debe llamar a getProveedoresActivos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(compraService.getProveedoresActivos).toHaveBeenCalled();
  }));

  it('getProveedores debe devolver error cuando falla getProveedoresActivos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalled();
  }));

  it('buscar debe llamar a getRemitos', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemitos').and.returnValue(of());
    component.ngOnInit();
    component.buscar();

    tick();
    fixture.detectChanges();
    expect(remitoProveedorService.getRemitos).toHaveBeenCalled();
  }));

  it('buscar debe devolver error cuando falla getRemitos en RemitoService', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemitos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();
    component.buscar();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalled();
  }));

  it('limpiar debe dejar variables undefined', fakeAsync(() => {
    component.remitos = Object.assign({}, mk.REMITOS_PROVEEDOR_PREVIEW);
    component.proveedorSeleccionado = Object.assign({}, mk.PROVEEDORES[0]);
    let date = new Date();
    component.fechaSeleccionada = { day: date.getDate(), month: date.getMonth(), year: date.getFullYear() };
    component.numeroSeleccionado = 1234;
    component.ngOnInit();
    component.limpiar();

    tick();
    fixture.detectChanges();
    expect(component.remitos).toBeUndefined();
    expect(component.proveedorSeleccionado).toBeUndefined();
    expect(component.fechaSeleccionada).toBeUndefined();
    expect(component.numeroSeleccionado).toBeUndefined();
  }));

  it('OnDestroy debe eliminar los mensajes de error', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemitos').and.returnValue(throwError({}));
    spyOn(messageService, 'clear').and.returnValue(of());
    component.ngOnInit();
    component.ngOnDestroy();

    tick();
    fixture.detectChanges();
    expect(messageService.clear).toHaveBeenCalled();
  }));
});
