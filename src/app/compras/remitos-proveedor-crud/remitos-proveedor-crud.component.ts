import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';
import { ConvertersService } from 'src/app/shared/utils/converters.service';

import * as md from '../../_models';
import { CompraService } from '../../shared/services/compra.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { RemitoProveedorService } from '../../shared/services/remito-proveedor.service';

@Component({
  selector: 'app-remitos-proveedor-crud',
  templateUrl: './remitos-proveedor-crud.component.html',
  styleUrls: ['./remitos-proveedor-crud.component.scss']
})
export class RemitosProveedorCrudComponent implements OnInit {

  title: string;

  proveedores: md.Proveedor[];
  comprobanteTipos: md.ComprobanteTipo[];

  ordenesCompraEmitidas: md.OrdenCompraEmitida[] = Array<md.OrdenCompraEmitida>();

  remitoId: number;
  remito: md.RemitoProveedor = <md.RemitoProveedor>{ "items": [] };
  itemSeleccionado: md.RemitoProveedorItem = <md.RemitoProveedorItem>{ "numerosSerie": [] };

  readOnly: boolean;
  hasRemito: boolean;
  readOnlyObservaciones: boolean;
  showSeries: boolean;

  model: NgbDateStruct;
  dateString: string;

  nuevoSerie: String;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private compraService: CompraService,
    private messageService: MessageService,
    private ordenCompraService: OrdenCompraService,
    private remitoProveedorService: RemitoProveedorService,
    private convertersService: ConvertersService
  ) {
    this.readOnly = false;
    this.readOnlyObservaciones = false;
    this.remitoId = 0;
    this.hasRemito = false;
    this.showSeries = false;
    this.title = "Gestión de Remitos de Proveedores";
  }

  // TODO: agregar spinner cuando esta esperando
  // TODO: switchMap to change the router param
  // TODO: add 00000 al nro de orden

  ngOnInit(): void {
    this.model = this.convertersService.setDefaultDate();
    this.onSelectDate(this.model);
    this.getProveedores();
    this.getComprobanteTipos();
    this.getCRUDStatus();
  }

  // OC
  obtenerOrdenesCompraEmitidas(): void {
    this.limpiarItems();
    if (this.remito.proveedor) {
      this.ordenCompraService.getOrdenesCompraEmitidas(this.remito.proveedor.id)
        .subscribe(
          ordenesPreview => {

            if (ordenesPreview) {
              ordenesPreview.map(ordenPreview => {
                let ordenEmitida: md.OrdenCompraEmitida = <md.OrdenCompraEmitida>{}; // TODO: change test

                ordenEmitida.id = ordenPreview.id;
                ordenEmitida.puesto = ordenPreview.puesto;
                ordenEmitida.numero = ordenPreview.numero;
                ordenEmitida.seleccionada = false;

                this.ordenesCompraEmitidas.push(ordenEmitida);

              });
            }
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  relacionarOrdenCompra(orden: md.OrdenCompraEmitida): void {
    (orden.seleccionada) ? this.agregarOrdenCompra(orden) : this.quitarOrdenCompra(orden);
  }

  // Series  
  faltanSeries(): boolean {
    if (this.itemSeleccionado && this.itemSeleccionado.cantidad > 0 &&
      (this.itemSeleccionado.numerosSerie && this.itemSeleccionado.numerosSerie.length < this.itemSeleccionado.cantidad) || (!this.itemSeleccionado.numerosSerie)) {
      return true;
    } else {
      return false;
    }
  }

  seleccionarItem(item: md.RemitoProveedorItem): void {
    this.itemSeleccionado = item;
    this.nuevoSerie == undefined;
  }

  agregarSerie(): void {
    if (this.nuevoSerie) {
      this.remito.items.map(item => {
        if (item.id == this.itemSeleccionado.id) {
          if (item.numerosSerie == undefined) {
            item.numerosSerie = [];
          }
          item.numerosSerie.push(this.nuevoSerie);
        }
      });
      this.nuevoSerie = undefined;
    }
  }

  eliminarSerie(serieAQuitar: String): void {
    this.remito.items.map(item => {
      if (item.id == this.itemSeleccionado.id) {
        item.numerosSerie = item.numerosSerie.filter( serie => serie !== serieAQuitar);
      }
    })
  }

  // Items
  agregarProducto(producto: md.ProductoSeleccionado): void {
    if (typeof producto == 'object') {
      let tempItem: md.RemitoProveedorItem = <md.RemitoProveedorItem>{ "producto": <md.ProductoPreview>{} };
      tempItem.id = this.remito.items.length + 1;
      tempItem.producto.id = producto.id;
      tempItem.producto.descripcion = producto.descripcion;
      tempItem.cantidad = producto.cantidad;
      tempItem.ordenCompra = undefined;
      tempItem.cantidadOrdenCompra = 0;

      this.remito.items.push(tempItem);
    }
  }

  quitarProducto(itemAQuitar: md.RemitoProveedorItem): void {
    this.remito.items = this.remito.items.filter(item => item !== itemAQuitar);
  }

  // Form
  getStatus(currentStatus: string): void {
    switch (currentStatus) {
      case 'edit':
        this.edit();
        break;
      case 'save':
        this.guardarRemito();
        break;
      case 'cancel':
        this.cancel();
        break;
      default:
        break;
    }
  }

  validForm(showAlerts: boolean): boolean {
    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.remito.comprobanteTipo) {
      if (showAlerts) this.messageService.addError("Seleccione un tipo de comprobante.");
      valido = false;
    }

    if (!this.remito.puestoId) {
      if (showAlerts) this.messageService.addError("Ingrese el puesto del comprobante.");
      valido = false;
    }

    if (!this.remito.numero) {
      if (showAlerts) this.messageService.addError("Ingrese el numero del comprobante.");
      valido = false;
    }

    if (!this.remito.proveedor) {
      if (showAlerts) this.messageService.addError("Seleccione un proveedor.");
      valido = false;
    }

    if (!this.remito.items) {
      if (showAlerts) this.messageService.addError("Debe cargar al menos un producto al remito.");
      valido = false;
    }

    if (this.remito.items && (this.remito.items.filter(item => item.cantidad > 0).length = 0)) {
      if (showAlerts) this.messageService.addError("Al menos uno de los productos debe tener una cantidad definida.");
      valido = false;
    }

    if (!this.remito.fecha) {
      if (showAlerts) this.messageService.addError("Ingrese la fecha del comprobante.");
      valido = false;
    }

    if (!this.itemsValidos) {
      if (showAlerts) this.messageService.addError("La cantidad de los items relacionados a órdenes de compra no pueden superar a la cantidad solicitada en la orden de compra.");
      valido = false;
    }

    if (!this.seriesValidos) {
      if (showAlerts) this.messageService.addError("La cantidad de números de serie no puede superar a la cantidad de productos.");
      valido = false;
    }

    return valido;
  }

  onSelectDate(date: NgbDateStruct) {
    if (date != null && date.year > date.day) {
      this.model = date;
      this.dateString = this.convertersService.onSelectDate(this.model);
    } else {
      this.dateString = undefined;
    }

    if (this.dateString) {
      this.remito.fecha = this.dateString;
    } else {
      this.remito.fecha = undefined;
    }
  }

  toggleSeries() {
    this.showSeries = !this.showSeries;
    this.itemSeleccionado = <md.RemitoProveedorItem>{ "numerosSerie": [] };
    if (this.showSeries && this.remito.items && this.remito.items.length > 0){
      this.seleccionarItem(this.remito.items[0]);
    }
  }

  // Init
  private getProveedores(): void {
    this.compraService.getProveedoresActivos()
      .subscribe(
        result => {
          this.proveedores = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getComprobanteTipos(): void {
    this.compraService.getRemitoTipos()
      .subscribe(
        result => {
          this.comprobanteTipos = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getCRUDStatus(): void {
    // TODO: should test this
    this.hasRemito =
      this.route.snapshot.paramMap.get('id') != undefined
      && this.route.snapshot.paramMap.get('id') !== '0';

    if (this.hasRemito) {
      this.readOnly = true;
      this.readOnlyObservaciones = true;
      this.remitoId = +this.route.snapshot.paramMap.get('id');
      this.getRemito();

    } else {
      this.readOnly = false;
      this.readOnlyObservaciones = false;
      this.remitoId = 0;

    }
  }

  // Remito
  private getRemito(): void {
    if (this.remitoId > 0) {
      this.remitoProveedorService.getRemito(this.remitoId)
        .subscribe(
          result => {
            this.setRemito(result);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private setRemito(remitoObtenido: md.RemitoProveedor): void {
    this.remito = remitoObtenido;
    if (remitoObtenido.fecha) {
      let fecha: Date = new Date(remitoObtenido.fecha);
      this.model = { day: fecha.getUTCDate(), month: fecha.getUTCMonth() + 1, year: fecha.getUTCFullYear() };
    }
  }

  private guardarRemito(): void {
    if (this.validForm(true)) {
      this.onSelectDate(this.model);

      (this.hasRemito) ? this.actualizarRemito() : this.insertarRemito();
    }
  }

  private insertarRemito(): void {
    this.remitoProveedorService.postRemito(this.remito)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha generado el remito con número " + result.numero);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó el remito. Verifique que el mismo se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private actualizarRemito(): void {
    this.remitoProveedorService.putRemito(this.remito)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha actualizado el remito con número " + result.numero);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó el remito. Verifique que el mismo se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private initAfterSave(id: number): void {
    this.remitoId = id;
    this.getRemito();

    this.readOnly = true;
    this.hasRemito = true;
    this.readOnlyObservaciones = true;
    this.router.navigate([`/proveedores/remitos/crud/${this.remitoId}`]);
  }

  // Items
  private itemsValidos(): boolean {
    return !this.remito.items.filter(item => item.cantidadOrdenCompra > 0)
      .some(item => item.cantidad > item.cantidadOrdenCompra);
  }

  private limpiarItems(): void {
    this.ordenesCompraEmitidas = [];
    this.remito.items = this.remito.items.filter(item => (item.ordenCompra == undefined));
    this.itemSeleccionado = <md.RemitoProveedorItem>{ "numerosSerie": [] };
    this.nuevoSerie = undefined;
    this.showSeries = false;
  }

  // Series
  private seriesValidos(): boolean {
    return !this.remito.items.filter(item => item.numerosSerie)
      .some(item => item.cantidad < item.numerosSerie.length);
  }

  // OC
  private agregarOrdenCompra(orden: md.OrdenCompraEmitida): void {
    this.ordenCompraService.getOrdenCompra(orden.id)
      .subscribe(
        result => {

          result.items.map(itemOC => {
            let newItem: md.RemitoProveedorItem = <md.RemitoProveedorItem>{};
            newItem.id = this.remito.items.length + 1;
            newItem.producto = itemOC.producto;
            newItem.cantidad = itemOC.cantidad;
            newItem.cantidadOrdenCompra = itemOC.cantidad;
            newItem.ordenCompra = orden;

            this.remito.items.push(newItem);
          });

        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private quitarOrdenCompra(ordenAQuitar: md.OrdenCompraEmitida): void {
    this.remito.items = this.remito.items.filter(item => (item.ordenCompra == undefined || (item.ordenCompra && item.ordenCompra.id != ordenAQuitar.id)));
  }

  // Form
  private edit(): void {
    this.readOnly = true;
    this.hasRemito = true;
    this.readOnlyObservaciones = false;
  };

  private cancel(): void {
    this.getRemito();
    this.readOnly = true;
    this.readOnlyObservaciones = true;
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
