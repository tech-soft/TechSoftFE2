import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { Observable, of, throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { CompraService } from '../../shared/services/compra.service';
import { OrdenCompraService } from '../../shared/services/orden-compra.service';
import { RemitoProveedorService } from '../../shared/services/remito-proveedor.service';
import { RemitosProveedorCrudComponent } from './remitos-proveedor-crud.component';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { CustomNgbDateParserFormatter } from '../../shared/services/custom-ngb-date-parser-formatter';

class CompraServiceMock {
  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return of(undefined);
  }
  public getRemitoTipos(): Observable<md.ComprobanteTipo[]> {
    return of(undefined);
  }
}

class OrdenCompraServiceMock {
  public getOrdenCompra(id: number): Observable<md.OrdenCompra> {
    return of(undefined);
  }
  public getOrdenesCompraEmitidas(proveedorId: number): Observable<md.OrdenCompraPreview[]> {
    return of(undefined);
  }
}

class RemitoServiceMock {
  public getRemito(id: number): Observable<md.RemitoProveedor> {
    return of(undefined);
  }
  public postRemito(remito: md.RemitoProveedor): Observable<md.RemitoProveedor> {
    return of(undefined);
  }
  public putRemito(remito: md.RemitoProveedor): Observable<md.RemitoProveedor> {
    return of(undefined);
  }
  public deleteRemito(id: number): Observable<md.RemitoProveedor> {
    return of(undefined);
  }
}

describe('RemitosProveedorCrudComponent', () => {

  let component: RemitosProveedorCrudComponent;
  let fixture: ComponentFixture<RemitosProveedorCrudComponent>;
  let compraService: CompraService;
  let messageService: MessageService;
  let ordenCompraService: OrdenCompraService;
  let remitoProveedorService: RemitoProveedorService;

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        RemitosProveedorCrudComponent
      ],
      imports: [
        HttpClientModule
      ],
      providers: [
        { provide: CompraService, useClass: CompraServiceMock },
        { provide: OrdenCompraService, useClass: OrdenCompraServiceMock },
        { provide: RemitoProveedorService, useClass: RemitoServiceMock },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: convertToParamMap({ id: undefined }) } }
        },
        // { provide: NgbDateParserFormatter, useFactory: () => new CustomNgbDateParserFormatter('longDate') },
        MessageService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(RemitosProveedorCrudComponent);
    component = fixture.componentInstance;
    compraService = TestBed.get(CompraService);
    messageService = TestBed.get(MessageService);
    ordenCompraService = TestBed.get(OrdenCompraService);
    remitoProveedorService = TestBed.get(RemitoProveedorService);
    fixture.detectChanges();
  }));

  it('se debe inicializar valores en el constructor', fakeAsync(() => {
    expect(component.readOnly).toBeFalsy();
    expect(component.readOnlyObservaciones).toBeFalsy();
    expect(component.remitoId).toBe(0);
    expect(component.hasRemito).toBeFalsy();
    expect(component.showSeries).toBeFalsy();
    expect(component.title).toBe("Gestión de Remitos de Proveedores");
  }));

  it('ngOnInit debe setear fecha inicial para el datepicker', fakeAsync(() => {
    component.ngOnInit();

    tick(); // TODO
    fixture.detectChanges();
    console.log(component.model);
    console.log(component.dateString);
    console.log(this.remito.fecha);
  }));

  it('ngOnInit debe llamar a getProveedoresActivos/getRemitoTipos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(of());
    spyOn(compraService, 'getRemitoTipos').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(compraService.getProveedoresActivos).toHaveBeenCalled();
    expect(compraService.getRemitoTipos).toHaveBeenCalled();
  }));

  it('getProveedoresActivos/getRemitoTipos deben devolver error cuando fallan', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(throwError({}));
    spyOn(compraService, 'getRemitoTipos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(2);
  }));

  it('ngOnInit debe setear crud status inicial para remito nuevo', fakeAsync(() => {
    component.ngOnInit();

    tick(); // TODO
    fixture.detectChanges();
    expect(component.hasRemito).toBeFalsy();
    expect(component.readOnly).toBeFalsy();
    expect(component.readOnlyObservaciones).toBeFalsy();
    expect(component.remitoId).toBe(0);
  }));

  it('ngOnInit debe setear crud status inicial para remito existente', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemito').and.returnValue(of());
    component.ngOnInit();

    tick(); // TODO
    fixture.detectChanges();
    expect(component.hasRemito).toBeTruthy();
    expect(component.readOnly).toBeTruthy();
    expect(component.readOnlyObservaciones).toBeTruthy();
    //expect(component.remitoId).toBe();
    expect(remitoProveedorService.getRemito).toHaveBeenCalled();

  }));

  it('getRemito debe obtener remito', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemito').and.returnValue(of(mk.REMITOS_PROVEEDOR[0]));
    component.ngOnInit();

    tick(); // TODO
    fixture.detectChanges();
    expect(remitoProveedorService.getRemito).toHaveBeenCalled();

    remitoProveedorService.getRemito(mk.REMITOS_PROVEEDOR[0].id).subscribe(
      result => {
        expect(result).toBe(mk.REMITOS_PROVEEDOR[0]);
        expect(component.remito).toBe(mk.REMITOS_PROVEEDOR[0]);
        // TODO: test model
      }
    );

  }));

  it('getRemito debe devolver error cuando falla', fakeAsync(() => {
    spyOn(remitoProveedorService, 'getRemito').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick(); // TODO
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('obtenerOrdenesCompraEmitidas debe llamar a getOrdenesCompraEmitidas en OrdenCompraService si el remito posee proveedor', fakeAsync(() => {
    component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
    spyOn(ordenCompraService, 'getOrdenesCompraEmitidas').and.returnValue(of());
    component.ngOnInit();
    component.obtenerOrdenesCompraEmitidas();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.getOrdenesCompraEmitidas).toHaveBeenCalled();
  }));

  it('obtenerOrdenesCompraEmitidas no debe llamar a getOrdenesCompraEmitidas en OrdenCompraService si el remito no posee proveedor', fakeAsync(() => {
    component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
    component.remito.proveedor = undefined;
    spyOn(ordenCompraService, 'getOrdenesCompraEmitidas').and.returnValue(of());
    component.ngOnInit();
    component.obtenerOrdenesCompraEmitidas();

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.getOrdenesCompraEmitidas).not.toHaveBeenCalled();
  }));

  it('obtenerOrdenesCompraEmitidas debe devolver error cuando falla', fakeAsync(() => {
    component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
    spyOn(ordenCompraService, 'getOrdenesCompraEmitidas').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();
    component.obtenerOrdenesCompraEmitidas();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('relacionarOrdenCompra debe llamar a getOrdenCompra en OrdenCompraService', fakeAsync(() => {
    let ordenSeleccionada = Object.assign({}, mk.ORDENES_COMPRA_PREVIEW[0]);
    spyOn(ordenCompraService, 'getOrdenCompra').and.returnValue(of(mk.ORDENES_COMPRA[0]));
    component.ngOnInit();
    component.relacionarOrdenCompra(mk.ORDENES_COMPRA_EMITIDAS[0]);

    tick();
    fixture.detectChanges();
    expect(ordenCompraService.getOrdenCompra).toHaveBeenCalled();

    ordenCompraService.getOrdenCompra(ordenSeleccionada.id).subscribe(
      result => {
        expect(result).toBe(mk.ORDENES_COMPRA[0]);
        expect(component.remito.items.length).toBe(result.items.length);
      }
    );

  }));

  // it('relacionarOrdenCompra debe devolver error cuando falla', fakeAsync(() => {
  //   let ordenSeleccionada = Object.assign({}, mk.ORDENES_COMPRA_PREVIEW[0]);
  //   spyOn(ordenCompraService, 'getOrdenCompra').and.returnValue(throwError({}));
  //   spyOn(messageService, 'addError').and.returnValue(of());
  //   component.ngOnInit();
  //   component.relacionarOrdenCompra(ordenSeleccionada);

  //   tick();
  //   fixture.detectChanges();
  //   expect(messageService.addError).toHaveBeenCalledTimes(1);
  // }));

  // it('quitarOrdenCompra debe eliminar elemento del array de ordenes seleccionadas', fakeAsync(() => {
  //   component.ordenesCompraSeleccionadas = Object.assign([{}], mk.ORDENES_COMPRA_PREVIEW);
  //   component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.ordenesCompraSeleccionadas.length).toBe(mk.ORDENES_COMPRA_PREVIEW.length);
  //   expect(component.ordenesCompraSeleccionadas.some(orden => orden === mk.ORDENES_COMPRA_PREVIEW[0])).toBeTruthy();
  //   expect(component.remito.items.some(item => item.ordenCompraId === mk.ORDENES_COMPRA_PREVIEW[0].id)).toBeTruthy();
  //   component.quitarOrdenCompra(mk.ORDENES_COMPRA_PREVIEW[0]);

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.ordenesCompraSeleccionadas.length).toBe(mk.ORDENES_COMPRA_PREVIEW.length - 1);
  //   expect(component.ordenesCompraSeleccionadas.some(orden => orden === mk.ORDENES_COMPRA_PREVIEW[0])).toBeFalsy();
  //   expect(component.remito.items.some(item => item.ordenCompraId === mk.ORDENES_COMPRA_PREVIEW[0].id)).toBeFalsy();

  // }));

  // it('agregarProducto debe agregar elemento del array productos', fakeAsync(() => {
  //   let productoSeleccionado = { id: 1, descripcion: 'Test push new object', cantidad: 1 };
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items.length).toBe(0);
  //   component.agregarProducto(productoSeleccionado);

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items.length).toBe(1);
  //   expect(component.remito.items.some(item => item.producto.descripcion === productoSeleccionado.descripcion)).toBeTruthy();
  // }));

  // it('quitarProducto debe eliminar elemento del array productos', fakeAsync(() => {
  //   component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items.length).toBe(mk.REMITOS_PROVEEDOR[0].items.length);
  //   expect(component.remito.items.some(item => item === mk.REMITOS_PROVEEDOR[0].items[0])).toBeTruthy();
  //   component.quitarProducto(mk.REMITOS_PROVEEDOR[0].items[0]);

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items.length).toBe(mk.REMITOS_PROVEEDOR[0].items.length - 1);
  //   expect(component.remito.items.some(item => item === mk.REMITOS_PROVEEDOR[0].items[0])).toBeFalsy();
  // }));

  // it('relacionarNumerosSerie debe agregar elemento del array numerosSerie', fakeAsync(() => {
  //   let itemId = 3;
  //   let numerosSerieNuevos = ["123456", "142536"];
  //   component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items.length).toBe(mk.REMITOS_PROVEEDOR[0].items.length);
  //   expect(component.remito.items[2].numerosSerie).toBeUndefined();
  //   component.relacionarNumerosSerie(itemId, numerosSerieNuevos);

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.remito.items[2].numerosSerie.length).toBe(2);
  //   expect(component.remito.items[2].numerosSerie).toBe(numerosSerieNuevos);
  // }));

  // it('guardarRemito debe llamar a postRemito RemitoProveedorService si el remito es válido y hasRemito false', fakeAsync(() => {
  //   spyOn<any>(component, 'validForm').and.returnValue(true);
  //   spyOn(remitoProveedorService, 'postRemito').and.returnValue(of());
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.hasRemito = false;
  //   component.guardarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.postRemito).toHaveBeenCalled();

  //   remitoProveedorService.postRemito(mk.REMITOS_PROVEEDOR[0]).subscribe(
  //     result => {
  //       expect(component.readOnly).toBeTruthy();
  //       expect(component.hasRemito).toBeTruthy();
  //     }
  //   );
  // }));

  // it('guardarRemito debe llamar a putRemito RemitoProveedorService si el remito es válido y hasRemito true', fakeAsync(() => {
  //   spyOn<any>(component, 'validForm').and.returnValue(true);
  //   spyOn(remitoProveedorService, 'putRemito').and.returnValue(of());
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.hasRemito = true;
  //   component.guardarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.putRemito).toHaveBeenCalled();

  //   remitoProveedorService.putRemito(mk.REMITOS_PROVEEDOR[0]).subscribe(
  //     result => {
  //       expect(component.readOnly).toBeTruthy();
  //       expect(component.hasRemito).toBeTruthy();
  //     }
  //   );
  // }));

  // it('guardarRemito no debe llamar a postRemito ni a putRemito si el remito no es válido', fakeAsync(() => {
  //   spyOn<any>(component, 'validForm').and.returnValue(false);
  //   spyOn(remitoProveedorService, 'postRemito').and.returnValue(of());
  //   spyOn(remitoProveedorService, 'putRemito').and.returnValue(of());
  //   component.ngOnInit();
  //   component.guardarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.postRemito).not.toHaveBeenCalled();
  //   expect(remitoProveedorService.putRemito).not.toHaveBeenCalled();
  // }));

  // it('guardarRemito deben devolver error cuando falla el post', fakeAsync(() => {
  //   spyOn<any>(component, 'validForm').and.returnValue(true);
  //   spyOn(remitoProveedorService, 'postRemito').and.returnValue(throwError({}));
  //   spyOn(messageService, 'addError').and.returnValue(of());
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.hasRemito = false;
  //   component.guardarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.postRemito).toHaveBeenCalled();
  //   expect(messageService.addError).toHaveBeenCalledTimes(1);
  // }));

  // it('guardarRemito deben devolver error cuando falla el put', fakeAsync(() => {
  //   spyOn<any>(component, 'validForm').and.returnValue(true);
  //   spyOn(remitoProveedorService, 'putRemito').and.returnValue(throwError({}));
  //   spyOn(messageService, 'addError').and.returnValue(of());
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.hasRemito = true;
  //   component.guardarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.putRemito).toHaveBeenCalled();
  //   expect(messageService.addError).toHaveBeenCalledTimes(1);
  // }));

  // it('eliminarRemito debe llamar a deleteRemito RemitoProveedorService si el remito es válido', fakeAsync(() => {
  //   component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
  //   spyOn(remitoProveedorService, 'deleteRemito').and.returnValue(of());
  //   component.ngOnInit();
  //   component.eliminarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.deleteRemito).toHaveBeenCalled();
  // }));

  // it('eliminarRemito no debe llamar a deleteRemito si el remito no es válido', fakeAsync(() => {
  //   spyOn(remitoProveedorService, 'deleteRemito').and.returnValue(of());
  //   component.ngOnInit();
  //   component.eliminarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.deleteRemito).not.toHaveBeenCalled();
  // }));

  // it('eliminarRemito deben devolver error cuando falla', fakeAsync(() => {
  //   component.remito = Object.assign({}, mk.REMITOS_PROVEEDOR[0]);
  //   spyOn(remitoProveedorService, 'deleteRemito').and.returnValue(throwError({}));
  //   spyOn(messageService, 'addError').and.returnValue(of());
  //   component.ngOnInit();
  //   component.eliminarRemito();

  //   tick();
  //   fixture.detectChanges();
  //   expect(remitoProveedorService.deleteRemito).toHaveBeenCalled();
  //   expect(messageService.addError).toHaveBeenCalledTimes(1);

  // }));

  // it('edit debe setear variables para editar', fakeAsync(() => {
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.edit();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.readOnly).toBeFalsy();
  //   expect(component.hasRemito).toBeTruthy();
  // }));

  // it('cancel debe setear variables', fakeAsync(() => {
  //   spyOn(remitoProveedorService, 'getRemito').and.returnValue(of());
  //   component.ngOnInit();

  //   tick();
  //   fixture.detectChanges();
  //   component.remitoId = 1;
  //   component.cancel();

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.readOnly).toBeTruthy();
  //   expect(remitoProveedorService.getRemito).toHaveBeenCalled();

  // }));

  // it('getStatus debe seleccionar accion de acuerdo a string enviado', fakeAsync(() => {
  //   spyOn(component, 'edit');
  //   spyOn(component, 'eliminarRemito');
  //   spyOn(component, 'guardarRemito');
  //   spyOn(component, 'cancel');
  //   component.ngOnInit();
  //   component.getStatus('edit');

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.edit).toHaveBeenCalled();
  //   component.getStatus('delete');

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.eliminarRemito).toHaveBeenCalled();
  //   component.getStatus('save');

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.guardarRemito).toHaveBeenCalled();
  //   component.getStatus('cancel');

  //   tick();
  //   fixture.detectChanges();
  //   expect(component.cancel).toHaveBeenCalled();
  // }));

  // it('ngOnDestroy debe limpiar los mensajes', fakeAsync(() => {
  //   spyOn(messageService, 'clear');
  //   component.ngOnInit();
  //   component.ngOnDestroy();

  //   tick();
  //   fixture.detectChanges();
  //   expect(messageService.messages.length).toBe(0);
  //   expect(messageService.clear).toHaveBeenCalled();
  // }));
});
