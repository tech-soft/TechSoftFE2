import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { CompraService } from '../../shared/services/compra.service';
import { FacturaService } from '../../shared/services/factura.service';
import { FacturasProveedorSearchComponent } from './facturas-proveedor-search.component';

class CompraServiceMock {
  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return of(undefined);
  }
}

class FacturaServiceMock {
  public getFacturas(proveedorId: string, fecha: string, numero: string): Observable<md.FacturaProveedorPreview[]> {
    return of(undefined);
  }
}

describe('FacturasProveedorSearchComponent', () => {
  let component: FacturasProveedorSearchComponent;
  let fixture: ComponentFixture<FacturasProveedorSearchComponent>;
  let compraService: CompraService;
  let messageService: MessageService;
  let facturaService: FacturaService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        FacturasProveedorSearchComponent 
      ],
      imports: [
        FormsModule,
        HttpClientModule, 
        FontAwesomeModule, 
        NgbModule.forRoot(), 
        RouterTestingModule
      ],
      providers: [
        { provide: CompraService, useClass: CompraServiceMock },
        { provide: FacturaService, useClass: FacturaServiceMock },
        MessageService
      ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(FacturasProveedorSearchComponent);
    component = fixture.componentInstance;
    compraService = TestBed.get(CompraService);
    messageService = TestBed.get(MessageService);
    facturaService = TestBed.get(FacturaService);
    fixture.detectChanges();
  }));

  it('ngOnInit debe llamar a getProveedoresActivos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(compraService.getProveedoresActivos).toHaveBeenCalled();
  }));

  it('getProveedores debe devolver error cuando falla getProveedoresActivos en CompraService', fakeAsync(() => {
    spyOn(compraService, 'getProveedoresActivos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalled();
  }));

  // it('buscar debe llamar a getRemitos', fakeAsync(() => {
  //   spyOn(facturaService, 'getFacturas').and.returnValue(of());
  //   component.ngOnInit();
  //   component.buscar();

  //   tick();
  //   fixture.detectChanges();
  //   expect(facturaService.getFacturas).toHaveBeenCalled();
  // }));

  // it('buscar debe devolver error cuando falla getRemitos en FacturaService', fakeAsync(() => {
  //   spyOn(facturaService, 'getFacturas').and.returnValue(throwError({}));
  //   spyOn(messageService, 'addError').and.returnValue(of());
  //   component.ngOnInit();
  //   component.buscar();

  //   tick();
  //   fixture.detectChanges();
  //   expect(messageService.addError).toHaveBeenCalled();
  // }));

  it('limpiar debe dejar variables undefined', fakeAsync(() => {
    component.facturas = [{
      id: 1,
      puesto: 1,
      numero: 1,
      proveedor: {
        id: 3,
        nombre: 'Airoldi'
      },
      fecha: '2018-01-01'
    }];
    component.proveedorSeleccionado = Object.assign({}, mk.PROVEEDORES[0]);
    let date = new Date();
    component.fechaSeleccionada = { day: date.getDate(), month: date.getMonth(), year: date.getFullYear() };
    component.numeroSeleccionado = 1234;
    component.ngOnInit();
    component.limpiar();

    tick();
    fixture.detectChanges();
    expect(component.facturas).toBeUndefined();
    expect(component.proveedorSeleccionado).toBeUndefined();
    expect(component.fechaSeleccionada).toBeUndefined();
    expect(component.numeroSeleccionado).toBeUndefined();
  }));

  // it('OnDestroy debe eliminar los mensajes de error', fakeAsync(() => {
  //   spyOn(facturaService, 'getFacturas').and.returnValue(throwError({}));
  //   spyOn(messageService, 'clear').and.returnValue(of());
  //   component.ngOnInit();
  //   component.ngOnDestroy();

  //   tick();
  //   fixture.detectChanges();
  //   expect(messageService.clear).toHaveBeenCalled();
  // }));

});
