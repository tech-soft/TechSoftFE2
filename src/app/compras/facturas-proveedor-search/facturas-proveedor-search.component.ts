import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';
import { throwError } from 'rxjs';

import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { ConvertersService } from '../../shared/utils/converters.service';
import { CompraService } from '../../shared/services/compra.service';
import { FacturaService } from '../../shared/services/factura.service';


@Component({
  selector: 'app-facturas-proveedor-search',
  templateUrl: './facturas-proveedor-search.component.html',
  styleUrls: ['./facturas-proveedor-search.component.scss']
})
export class FacturasProveedorSearchComponent implements OnInit {

  title: string;

  proveedores: md.Proveedor[];
  facturas: md.FacturaProveedorPreview[];

  proveedorSeleccionado: md.Proveedor;
  fechaSeleccionada: NgbDateStruct;
  numeroSeleccionado: number;

  constructor(
    private router: Router,
    private compraService: CompraService,
    private facturaService: FacturaService,
    private messageService: MessageService
  ) {
    this.title = "Búsqueda de Facturas";
  }

  ngOnInit(): void {
    this.getProveedores();
  }

  buscar(): void {

    // this.facturaService.getFacturas(
    //   (this.proveedorSeleccionado) ? this.proveedorSeleccionado.id.toString() : undefined,
    //   ConvertersService.ngbDateStructToEpochString(this.fechaSeleccionada),
    //   (this.numeroSeleccionado) ? this.numeroSeleccionado.toString() : undefined
    // )
    //   .subscribe(
    //     result => {
    //       this.facturas = result;
    //     },
    //     error => {
    //       this.messageService.addError(`${error.message}`);
    //       return throwError(error);
    //     }
    //   );
  }

  limpiar(): void {
    this.facturas = undefined;
    this.proveedorSeleccionado = undefined;
    this.fechaSeleccionada = undefined;
    this.numeroSeleccionado = undefined;
  }

  goto(menuOption: String, id: number): void {
    switch (menuOption) {
      case 'FacturaCrudAdd':
        this.router.navigate(['/proveedores/facturas/crud']);
        break;
      case 'FacturaCrudEdit':
        this.router.navigate([`/proveedores/facturas/crud/${id}`]);
        break;
      default:
        break;
    }
  }

  private getProveedores(): void {
    this.compraService.getProveedoresActivos()
      .subscribe(
        result => {
          this.proveedores = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}


