import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosSearchComponent } from './productos-search.component';
import { RouterTestingModule } from '@angular/router/testing';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { ReactiveFormsModule } from '@angular/forms';
import { LOCALE_ID } from '@angular/core';

describe('ProductosSearchComponent', () => {
  let component: ProductosSearchComponent;
  let fixture: ComponentFixture<ProductosSearchComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosSearchComponent ],
      imports: [
        RouterTestingModule,
        BrowserModule,
        HttpClientModule,
        FormsModule,
        FontAwesomeModule, 
        ReactiveFormsModule
      ],
      providers: [
        {
          provide: LOCALE_ID,
          useValue: 'es-AR'
        }
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosSearchComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component)
  //   .toBeTruthy();
  // });
});
