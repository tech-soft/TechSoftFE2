import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';
import { ProductosService } from 'src/app/shared/services/productos.service';
import * as md from '../../_models';
import { NgxSpinnerService } from 'ngx-spinner';

@Component({
  selector: 'app-productos-search',
  templateUrl: './productos-search.component.html',
  styleUrls: ['./productos-search.component.scss']
})
export class ProductosSearchComponent implements OnInit {

  title: string;

  productos: md.ProductoPrecioPreview[];

  productoDescripcion: String;
  conStockDisponible: boolean;
  conStockEntrante: boolean;

  constructor(
    private router: Router,
    private productosService: ProductosService,
    private messageService: MessageService,
    private spinner: NgxSpinnerService
  ) {
    this.title = 'Búsqueda de Productos';
  }

  ngOnInit(): void {
  }

  buscar(): void {
    if (this.productoDescripcion) {
      this.spinner.show();
      this.productosService.getProductosPreciosPreview(this.productoDescripcion, this.conStockDisponible, this.conStockEntrante)
        .subscribe(
          result => {
            this.productos = result;
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          },
          () => { this.spinner.hide(); });
    }
  }

  limpiar(): void {

  }

  goto(menuOption: String, id: number): void {
    switch (menuOption) {
      case 'ProductoCrudAdd':
        this.router.navigate(['/productos/crud']);
        break;
      case 'ProductoCrudEdit':
        this.router.navigate([`/productos/crud/${id}`]);
        break;
      default:
        break;
    }
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
