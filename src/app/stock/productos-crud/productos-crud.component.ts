import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import * as md from '../../_models';
import { EstadosService } from '../../shared/services/estados.service';
import { ModalCostosComponent } from '../modal-costos/modal-costos.component';
import { MessageService } from 'src/app/shared/services/message.service';
import { ProductosService } from 'src/app/shared/services/productos.service';
import { throwError } from 'rxjs';

@Component({
  selector: 'app-productos-crud',
  templateUrl: './productos-crud.component.html',
  styleUrls: ['./productos-crud.component.scss']
})
export class ProductosCrudComponent implements OnInit {

  title: string;

  estados: md.Estado[];
  origenes: md.Origen[];
  gravados: md.Gravado[];

  productoId: number;
  producto: md.Producto = <md.Producto>{};

  readOnly: boolean;
  hasProducto: boolean;

  // modalOption: NgbModalOptions = {};

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private estadoService: EstadosService,
    private messageService: MessageService,
    private productoService: ProductosService
    // private modalService: NgbModal,
  ) {
    this.readOnly = false;
    this.productoId = 0;
    this.hasProducto = false;
    this.title = "Gestión de Productos";
  }

  // TODO: agregar spinner cuando esta esperando

  ngOnInit(): void {
    this.getEstados();
    this.getOrigenes();
    this.getGravados();
    this.getCRUDStatus();
  }

  guardarProducto(): void {
    if (this.validForm(true)) {

      (this.hasProducto) ? this.actualizarProducto() : this.insertarProducto();

    }
  }

  eliminarProducto(): void {
    // TODO: confirm msg
    if (this.producto && this.producto.id) {
      this.productoService.deleteProducto(this.producto.id)
        .subscribe(
          result => {
            this.messageService.addSuccess("Se ha eliminado con éxito.");
            this.router.navigate([`/productos/search`]);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  edit(): void {
    this.readOnly = true;
    this.hasProducto = true;
  };

  cancel(): void {
    this.getProducto();
    this.readOnly = true;
  }

  getStatus(currentStatus: string): void {
    switch (currentStatus) {
      case 'edit':
        this.edit();
        break;
      case 'save':
        this.guardarProducto();
        break;
      case 'cancel':
        this.cancel();
        break;
      case 'delete':
        this.eliminarProducto();
        break;
      default:
        break;
    }
  }

  validForm(showAlerts: boolean): boolean {

    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.producto.descripcion) {
      if (showAlerts) this.messageService.addError("ingrese una descripción.");
      valido = false;
    }

    if (!this.producto.estado) {
      if (showAlerts) this.messageService.addError("ingrese un estado.");
      valido = false;
    }

    return valido;
  }

  private getEstados(): void {
    this.estadoService.getEstados()
      .subscribe(
        result => {
          this.estados = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  private getOrigenes(): void {
    this.productoService.getOrigenes()
      .subscribe(
        result => {
          this.origenes = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  private getGravados(): void {
    this.productoService.getGravados()
      .subscribe(
        result => {
          this.gravados = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  private getCRUDStatus(): void {
    // TODO: should test this
    this.hasProducto =
      this.route.snapshot.paramMap.get('id') != undefined
      && this.route.snapshot.paramMap.get('id') !== '0';

    if (this.hasProducto) {
      this.readOnly = true;
      this.productoId = +this.route.snapshot.paramMap.get('id');
      this.getProducto();

    } else {
      this.readOnly = false;
      this.productoId = 0;

    }
  }

  private getProducto(): void {
    if (this.productoId > 0) {
      this.productoService.getProducto(this.productoId)
        .subscribe(
          result => {
            this.producto = result;
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private insertarProducto(): void {
    this.productoService.postProducto(this.producto)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha generado el producto con código " + result.id);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó el producto. Verifique que el mismo se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private actualizarProducto(): void {
    this.productoService.putProducto(this.producto)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("Se ha actualizado el producto con código " + result.id);

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el número con que se generó el producto. Verifique que el mismo se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private initAfterSave(id: number): void {
    this.productoId = id;
    this.getProducto();

    this.readOnly = true;
    this.hasProducto = true;
    this.router.navigate([`/prodcutos/crud/${this.productoId}`]);
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }

  // openModal = () => {
  //   this.modalOption.backdrop = 'static';
  //   this.modalOption.keyboard = false;
  //   const modalRef = this.modalService.open(ModalCostosComponent, this.modalOption);
  //   modalRef.componentInstance.id = this.articulo.id;

  // }

}
