import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ProductosCrudComponent } from './productos-crud.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('ProductosCrudComponent', () => {
  let component: ProductosCrudComponent;
  let fixture: ComponentFixture<ProductosCrudComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ProductosCrudComponent ],
      imports: [
        FontAwesomeModule]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ProductosCrudComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
