import { Component, Input, OnInit } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { COSTOS } from '../../_mocks/costos.mock';

@Component({
  selector: 'app-modal-costos',
  templateUrl: './modal-costos.component.html',
  styleUrls: ['./modal-costos.component.scss']
})
export class ModalCostosComponent implements OnInit {

  @Input() id;
  mockCostos = COSTOS;
  costos = this.mockCostos;

  constructor(
    public activeModal: NgbActiveModal
  ) { }

  ngOnInit(): void { }

  cerrar(): void {
    this.activeModal.close();
  }

  trackByFn(index, item): number {
    return index;
  }

}
