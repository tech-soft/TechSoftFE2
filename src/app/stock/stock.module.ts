import { CommonModule, registerLocaleData } from '@angular/common';
import localeEs from '@angular/common/locales/es';
import localeEsExtra from '@angular/common/locales/extra/es';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { SharedModule } from '../shared/shared.module';
import { ModalCostosComponent } from './modal-costos/modal-costos.component';
import { ProductosSearchComponent } from './productos-search/productos-search.component';
import { StockRoutingModule } from './stock-routing.module';
import { ProductosCrudComponent } from './productos-crud/productos-crud.component';
import { NgxSpinnerModule } from 'ngx-spinner';

registerLocaleData(localeEs, 'es-AR', localeEsExtra);

@NgModule({
  declarations: [
    ProductosCrudComponent,
    ProductosSearchComponent,
    ModalCostosComponent
  ],
  entryComponents: [
    ModalCostosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule.forRoot(),
    StockRoutingModule,
    SharedModule,
    NgxSpinnerModule
  ],
  exports: [
    ProductosCrudComponent,
    ProductosSearchComponent
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es-AR'
    },
  ]
})
export class StockModule { }
