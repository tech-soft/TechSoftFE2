import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../_guards/auth.guard';
import { ProductosSearchComponent } from './productos-search/productos-search.component';
import { ProductosCrudComponent } from './productos-crud/productos-crud.component';

const routes: Routes = [
  {
    path: 'productos/crud',
    component: ProductosCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'productos/crud/:id',
    component: ProductosCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'productos/search',
    component: ProductosSearchComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class StockRoutingModule { }
