import { Component, OnInit } from '@angular/core';

import * as md from '../../_models';
import { MessageService } from '../services/message.service';

@Component({
  selector: 'app-messages',
  templateUrl: './messages.component.html',
  styleUrls: ['./messages.component.scss']
})
export class MessagesComponent implements OnInit {

  constructor(
    public messageService: MessageService
  ) { }

  ngOnInit(): void {
  }

  dismiss(message: md.Message): void {
    this.messageService.dismiss(message);
  }

  trackByFn(index, item): number {
    return index;
  }
}
