import { Component, OnInit, Input } from '@angular/core';
import { Output } from '@angular/core';
import { EventEmitter } from '@angular/core';

@Component({
  selector: 'app-crud-buttons',
  templateUrl: './crud-buttons.component.html',
  styleUrls: ['./crud-buttons.component.scss']
})
export class CrudButtonsComponent implements OnInit {

  @Input() hasEntity: number;
  @Input() isReadOnly: boolean;
  @Input() isValidForm: boolean;
  @Input() delete: boolean;
  @Output() status = new EventEmitter<string>();
  @Output() validationRequired = new EventEmitter<boolean>();

  constructor() {
  }

  ngOnInit(): void {   
  }

  changeStatus(currentStatus: string): void {
    
    if (currentStatus === 'save' && !this.isValidForm) {
      this.validationRequired.emit(true);
      return;
    };

    this.validationRequired.emit(false);
    this.status.emit(currentStatus);
  }

}
