import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable({
  providedIn: 'root'
})
export class OrdenCompraService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getEstadosOC(): Observable<md.Estado[]> {
    return this.httpClient.get<md.Estado[]>(
      environment.APIS.ESTADO.OC
    )
      .pipe(
        tap(
          estados => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getOrdenCompra(id: number): Observable<md.OrdenCompra> {
    return this.httpClient.get<md.OrdenCompra>(
      environment.APIS.ORDEN_COMPRA.BASE  + `/${id}`
    )
      .pipe(
        tap(
          orden => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getOrdenesCompra(
    proveedorId: string, fechaCreacion: string, puestoId: string, numero: string, estadoId: string
  ): Observable<md.OrdenCompraPreview[]> {

    let httpParams = new HttpParams();

    if (proveedorId) {
      httpParams = httpParams.set('proveedorId', proveedorId);
    }

    if (fechaCreacion) {
      httpParams = httpParams.set('fechaCreacion', fechaCreacion);
    }

    if (puestoId) {
      httpParams = httpParams.set('puestoId', puestoId);
    }

    if (numero) {
      httpParams = httpParams.set('numero', numero);
    }

    if (estadoId) {
      httpParams = httpParams.set('estadoId', estadoId);
    }

    // TODO: no llamar al servicio si no hay parametros

    return this.httpClient.get<md.OrdenCompraPreview[]>(
      environment.APIS.ORDEN_COMPRA.BASE,
      { params: httpParams }
    )
      .pipe(
        tap(
          ordenes => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getOrdenesCompraEmitidas(proveedorId: number): Observable<md.OrdenCompraPreview[]> {

    let httpParams = new HttpParams();
    httpParams = httpParams.set('proveedorId', proveedorId.toString());

    return this.httpClient.get<md.OrdenCompraPreview[]>(
      environment.APIS.ORDEN_COMPRA.EMITIDAS,
      { params: httpParams }
    )
      .pipe(
        tap(
          ordenes => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public emitirOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return this.httpClient.put<md.OrdenCompra>(
      environment.APIS.ORDEN_COMPRA.EMITIR,
      orden
    )
      .pipe(
        tap(
          orden => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public postOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return this.httpClient.post<md.OrdenCompra>(
      environment.APIS.ORDEN_COMPRA.BASE,
      orden
    )
      .pipe(
        tap(
          orden => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public putOrdenCompra(orden: md.OrdenCompra): Observable<md.OrdenCompra> {
    return this.httpClient.put<md.OrdenCompra>(
      environment.APIS.ORDEN_COMPRA.BASE,
      orden
    )
      .pipe(
        tap(
          orden => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public deleteOrdenCompra(id: number): Observable<md.OrdenCompra> {
    return this.httpClient.delete<md.OrdenCompra>(
      environment.APIS.ORDEN_COMPRA.BASE +  `/${id}`
    )
      .pipe(
        tap(
          orden => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
