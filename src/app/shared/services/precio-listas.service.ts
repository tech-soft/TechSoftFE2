import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class PrecioListasService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getPrecioListas(): Observable<Array<md.PrecioLista>> {

    return this.httpClient
      .get<Array<md.PrecioLista>>(
        environment.APIS.PRECIO_LISTA.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getPrecioListas', []))
      );
  }

  addPrecioLista(precioLista: md.PrecioLista): Observable<md.PrecioLista> {

    return this.httpClient
      .post<md.PrecioLista>(
        environment.APIS.PRECIO_LISTA.BASE,
        precioLista,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PrecioLista>('addPrecioLista'))
      );
  }

  updatePrecioLista(precioLista: md.PrecioLista): Observable<md.PrecioLista> {

    return this.httpClient
      .put<md.PrecioLista>(
        environment.APIS.PRECIO_LISTA.BASE,
        precioLista,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PrecioLista>('updatePrecioLista'))
      );
  }

  deletePrecioLista(id: number): Observable<md.PrecioLista> {

    return this.httpClient
      .delete<md.PrecioLista>(
        environment.APIS.PRECIO_LISTA.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PrecioLista>('deletePrecioLista'))
      );
  }
}
