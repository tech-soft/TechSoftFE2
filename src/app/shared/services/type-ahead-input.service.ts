import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { HttpClient } from '@angular/common/http';
import { getUrlScheme } from '@angular/compiler';
import { tap, catchError } from 'rxjs/operators';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class TypeAheadInputService {

  constructor(
    private httpClient: HttpClient,
    private handleErrorService: HandleErrorService
  ) { }

  search(term: String, type: number): any {

    if (!term.trim()) {
      return of([]);
    } else {
      let url = this.getUrl(type);

      if (url) {
        return this.httpClient
          .get<Array<any>>(
            url
          )
          .pipe(
            tap(_ => { }),
            catchError(
              this.handleErrorService.handleError<Array<any>>(`search term=${term}`)
            )
          );
      } else {
        return of([]);
      }
    }


  }

  getUrl(type: number): string {
    switch (type) {
      case environment.OBJECT_TYPE.PRODUCTOS:
        return '';
      default:
        return '';
    }
  }
}
