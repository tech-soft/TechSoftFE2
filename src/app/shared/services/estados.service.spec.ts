import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { ESTADOS } from '../../_mocks/estados.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { EstadosService } from './estados.service';
import { MessageService } from './message.service';

describe('EstadosService', () => {
  const serviceUrlBase = environment.APIS.ESTADO.BASE;

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        EstadosService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  it('Se espera que el servicio esté creado', inject([EstadosService], (service: EstadosService) => {
    expect(service)
      .toBeTruthy();
  }));

  // it('Se espera método GET sobre url ../techsoftapp/estado-tipos',
  //   async(
  //     inject([EstadosService, HttpTestingController],
  //       (service: EstadosService, backend: HttpTestingController) => {

  //         service.getEstados()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/estado-tipos',
  //   async(
  //     inject([EstadosService, HttpTestingController],
  //       (service: EstadosService, backend: HttpTestingController) => {

  //         service.addEstado(ESTADOS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/estado-tipos',
  //   async(
  //     inject([EstadosService, HttpTestingController],
  //       (service: EstadosService, backend: HttpTestingController) => {

  //         service.updateEstado(ESTADOS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/estado-tipos',
  //   async(
  //     inject([EstadosService, HttpTestingController],
  //       (service: EstadosService, backend: HttpTestingController) => {

  //         service.deleteEstado(ESTADOS[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + ESTADOS[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + ESTADOS[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos los estados',
  //   async(
  //     inject([EstadosService],
  //       (service: EstadosService) => {

  //         service.getEstados()
  //           .subscribe(estados => {
  //             expect(estados.length)
  //               .toBe(2, 'Se esperan 2 elementos');
  //             expect(estados)
  //               .toEqual(ESTADOS, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(ESTADOS);
  //       })
  //   )
  // );

});
