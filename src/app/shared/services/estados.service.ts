import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HandleErrorService } from '../../core/services/handle-error.service';
import * as md from '../../_models';
import { MessageService } from '../services/message.service';


@Injectable()
export class EstadosService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getEstados(): Observable<md.Estado[]> {

    return this.httpClient.get<md.Estado[]>(
      environment.APIS.ESTADO.BASE
    )
      .pipe(
        tap(
          estado => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status == 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addEstado(estado: md.Estado): Observable<md.Estado> {

    return this.httpClient.post<md.Estado>(
      environment.APIS.ESTADO.BASE,
      estado,
    )
      .pipe(
        tap(
          estado => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  updateEstado(estado: md.Estado): Observable<md.Estado> {

    return this.httpClient.put<md.Estado>(
      environment.APIS.ESTADO.BASE,
      estado,
    )
      .pipe(
        tap(
          estado => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deleteEstado(id: number): Observable<md.Estado> {

    return this.httpClient.delete<md.Estado>(
      environment.APIS.ESTADO.BASE + `/${id}`
    )
      .pipe(
        tap(
          estado => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
