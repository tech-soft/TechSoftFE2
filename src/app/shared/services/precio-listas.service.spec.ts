import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { LISTAS_PRECIO } from '../../_mocks/listas.mock';
import { MessageService } from './message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { environment } from '../../../environments/environment';
import { PrecioListasService } from './precio-listas.service';

describe('PrecioListasService', () => {
  const serviceUrlBase = environment.APIS.PRECIO_LISTA.BASE;

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PrecioListasService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([PrecioListasService], (service: PrecioListasService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/precio-listas',
  //   async(
  //     inject([PrecioListasService, HttpTestingController],
  //       (service: PrecioListasService, backend: HttpTestingController) => {

  //         service.getPrecioListas()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/precio-listas',
  //   async(
  //     inject([PrecioListasService, HttpTestingController],
  //       (service: PrecioListasService, backend: HttpTestingController) => {

  //         service.addPrecioLista(LISTAS_PRECIO[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/precio-listas',
  //   async(
  //     inject([PrecioListasService, HttpTestingController],
  //       (service: PrecioListasService, backend: HttpTestingController) => {

  //         service.updatePrecioLista(LISTAS_PRECIO[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/precio-listas',
  //   async(
  //     inject([PrecioListasService, HttpTestingController],
  //       (service: PrecioListasService, backend: HttpTestingController) => {

  //         service.deletePrecioLista(LISTAS_PRECIO[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + LISTAS_PRECIO[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + LISTAS_PRECIO[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos las listas de precio',
  //   async(
  //     inject([PrecioListasService],
  //       (service: PrecioListasService) => {

  //         service.getPrecioListas()
  //           .subscribe(precioListas => {
  //             expect(precioListas.length)
  //               .toBe(2, 'Se esperan 2 elementos');
  //             expect(precioListas)
  //               .toEqual(LISTAS_PRECIO, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(LISTAS_PRECIO);
  //       })
  //   )
  // );
});
