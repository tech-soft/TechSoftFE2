import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError, of } from 'rxjs';
import { tap, catchError, debounceTime, distinctUntilChanged, switchMap } from 'rxjs/operators';

import * as md from '../../_models';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getProducto(id: number): Observable<md.Producto> {
    return this.httpClient.get<md.Producto>(
      environment.APIS.PRODUCTO.BASE + `/${id}`
    )
      .pipe(
        tap(
          producto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getProductoStock(id: number): Observable<md.ProductoStock> {
    return this.httpClient.get<md.ProductoStock>(
      environment.APIS.PRODUCTO.BASE + `/${id}/stock`
    )
      .pipe(
        tap(
          producto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getProductosPreview(): Observable<md.ProductoPreview[]> {
    return this.httpClient
      .get<md.ProductoPreview[]>(
        environment.APIS.PRODUCTO.PREVIEW
      )
      .pipe(
        tap(
          productos => {
          }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getProductosPreciosPreview(term: String, conStockDisponible: boolean, conStockEntrante: boolean): Observable<md.ProductoPrecioPreview[]> {
    if (!term.trim()) {
      return of([]);
    }

    let httpParams = new HttpParams();

    httpParams = httpParams.set('conStockDisponible', (conStockDisponible) ? 'true' : 'false');
    httpParams = httpParams.set('conStockEntrante', (conStockEntrante) ? 'true' : 'false');
    httpParams = httpParams.set('q', term.toString());

    return this.httpClient
      .get<md.ProductoPrecioPreview[]>(
        environment.APIS.PRODUCTO.PREVIEW_LISTAS,
        { params: httpParams }
      )
      .pipe(
        tap(
          productos => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public searchProductos(terms: Observable<String>): Observable<any> {
    return terms.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap((term: String) => this.searchTerms(term))
    );
  }

  searchTerms(term: String): Observable<any> {
    if (!term.trim()) {
      return of([]);
    }

    let httpParams = new HttpParams();

    httpParams = httpParams.set('q', term.toString());

    return this.httpClient
      .get<md.ProductoPreview[]>(
        environment.APIS.PRODUCTO.PREVIEW_LISTAS,
        { params: httpParams }
      )
      .pipe(
        tap(
          productos => {
          }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public postProducto(producto: md.Producto): Observable<md.Producto> {
    return this.httpClient.post<md.Producto>(
      environment.APIS.PRODUCTO.BASE,
      producto
    )
      .pipe(
        tap(
          producto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public putProducto(producto: md.Producto): Observable<md.Producto> {
    return this.httpClient.put<md.Producto>(
      environment.APIS.PRODUCTO.BASE,
      producto
    )
      .pipe(
        tap(
          producto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public deleteProducto(id: number): Observable<md.Producto> {
    return this.httpClient.delete<md.Producto>(
      environment.APIS.PRODUCTO.BASE + `/${id}`
    )
      .pipe(
        tap(
          producto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getOrigenes(): Observable<md.Origen[]> {
    return this.httpClient.get<md.Origen[]>(
      environment.APIS.ORIGEN.BASE
    )
      .pipe(
        tap(
          origen => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getGravados(): Observable<md.Gravado[]> {
    return this.httpClient.get<md.Gravado[]>(
      environment.APIS.GRAVADOS.BASE
    )
      .pipe(
        tap(
          gravado => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }
}
