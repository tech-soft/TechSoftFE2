import { Injectable } from '@angular/core';

import * as md from '../../_models';

@Injectable({
  providedIn: 'root',
})
export class MessageService {

  messages: md.Message[] = Array<md.Message>();

  constructor() {

  }

  addError(errorMessage: String): void {
    let thisMessage: md.Message = <md.Message>{};
    thisMessage.type = md.MessageType.error;
    thisMessage.message = errorMessage;
    this.messages.push(thisMessage);
  }

  addWarning(warningMessage: String): void {
    let thisMessage: md.Message = <md.Message>{};
    thisMessage.type = md.MessageType.warning;
    thisMessage.message = warningMessage;
    this.messages.push(thisMessage);
  }

  addSuccess(successMessage: String): void {
    let thisMessage: md.Message = <md.Message>{};
    thisMessage.type = md.MessageType.success;
    thisMessage.message = successMessage;
    this.messages.push(thisMessage);
  }

  addInfo(infoMessage: String): void {
    let thisMessage: md.Message = <md.Message>{};
    thisMessage.type = md.MessageType.info;
    thisMessage.message = infoMessage;
    this.messages.push(thisMessage);
  }

  dismiss(messageToPop: md.Message): void {
    this.messages = this.messages.filter(message => message !== messageToPop);
  }

  clear(): void {
    this.messages = [];
  }
}
