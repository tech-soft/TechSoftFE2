import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable({
  providedIn: 'root'
})
export class CompraService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getProveedoresActivos(): Observable<md.Proveedor[]> {
    return this.httpClient.get<md.Proveedor[]>(
      environment.APIS.PROVEEDOR.ACTIVO
    )
      .pipe(
        tap(
          proveedores => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }
  
  public getPuestoActual(): Observable<md.Puesto> {
    // TODO: obtener sucursal
    const sucursalId = 1;
    return this.httpClient.get<md.Puesto[]>(
      environment.APIS.SUCURSAL.BASE + `/${sucursalId}/puestos/activos`
    )
      .pipe(
        tap(
          puestos => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getPuestosActivos(): Observable<md.Puesto[]> {
    return this.httpClient.get<md.Puesto[]>(
      environment.APIS.PUESTO.ACTIVO
    )
      .pipe(
        tap(
          puestos => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getRemitoTipos(): Observable<md.ComprobanteTipo[]> {
    return this.httpClient.get<md.ComprobanteTipo[]>(
      environment.APIS.COMPROBANTE_TIPO.REMITO
    )
      .pipe(
        tap(
          comprobantes => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }
}
