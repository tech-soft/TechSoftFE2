import { Injectable } from '@angular/core';

import * as md from '../../_models';
import { HttpParams } from '@angular/common/http';
import { environment } from '../../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class FacturaService {

  constructor(
    private httpClient: HttpClient
  ) { }

  // public getFacturas(
  //   proveedorId: string, fecha: string, numero: string
  // ): Observable<md.FacturaProveedorPreview[]> {

  //   let httpParams = new HttpParams();

  //   if (proveedorId) {
  //     httpParams = httpParams.set('proveedorId', proveedorId);
  //   }

  //   if (fecha) {
  //     httpParams = httpParams.set('fecha', fecha);
  //   }
    
  //   if (numero) {
  //     httpParams = httpParams.set('numero', numero);
  //   }

  //   return this.httpClient
  //     .get<md.FacturaProveedorPreview[]>(
  //       environment.APIS.FACTURA.BASE,
  //       {params: httpParams}
  //     )
  //     .pipe(
  //       tap(
  //         facturas => {
  //         }
  //       ),
  //       catchError(
  //         (err: any, caught: Observable<any>) => {
  //           return throwError(err.error)
  //         }
  //       ));
  // }

  // public getFactura(id: number): Observable<md.FacturaProveedor> {
  //   return this.httpClient
  //   .get<md.FacturaProveedor>(
  //     environment.APIS.FACTURA.BY_ID + id
  //   )
  //   .pipe(
  //     tap(
  //       factura => {
  //       }
  //     ),
  //     catchError(
  //       (err: any, caught: Observable<any>) => {
  //         return throwError(err.error)
  //       }
  //     ));
  // }

  // public postFactura(factura: md.FacturaProveedor): Observable<md.FacturaProveedor> {
  //   return this.httpClient
  //     .post<md.FacturaProveedor>(
  //       environment.APIS.FACTURA.BASE,
  //       factura
  //     )
  //     .pipe(
  //       tap(
  //         factura => {
  //         }
  //       ),
  //       catchError(
  //         (err: any, caught: Observable<any>) => {
  //           return throwError(err.error)
  //         }
  //       ));
  // }

  // public deleteFactura(id: number): Observable<md.FacturaProveedor> {
  //   return this.httpClient
  //     .delete<md.FacturaProveedor>(
  //       environment.APIS.FACTURA.BASE + id
  //     )
  //     .pipe(
  //       tap(
  //         factura => {
  //         }
  //       ),
  //       catchError(
  //         (err: any, caught: Observable<any>) => {
  //           return throwError(err.error)
  //         }
  //       ));
  // }
}

