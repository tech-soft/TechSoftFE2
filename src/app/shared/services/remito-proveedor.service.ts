import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable({
  providedIn: 'root'
})
export class RemitoProveedorService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getRemito(id: number): Observable<md.RemitoProveedor> {
    return this.httpClient.get<md.RemitoProveedor>(
      environment.APIS.REMITO.BASE + `/${id}`
    )
      .pipe(
        tap(
          remito => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getRemitos(proveedorId: string, fecha: string, numero: string): Observable<md.RemitoProveedorPreview[]> {

    let httpParams = new HttpParams();

    if (proveedorId) {
      httpParams = httpParams.set('proveedorId', proveedorId);
    }

    if (fecha) {
      httpParams = httpParams.set('fecha', fecha);
    }

    if (numero) {
      httpParams = httpParams.set('numero', numero);
    }

    return this.httpClient.get<md.RemitoProveedorPreview[]>(
      environment.APIS.REMITO.BASE,
      { params: httpParams }
    )
      .pipe(
        tap(
          remitos => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public postRemito(remito: md.RemitoProveedor): Observable<md.RemitoProveedor> {
    return this.httpClient.post<md.RemitoProveedor>(
      environment.APIS.REMITO.BASE,
      remito
    )
      .pipe(
        tap(
          remito => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public putRemito(remito: md.RemitoProveedor): Observable<md.RemitoProveedor> {
    return this.httpClient.put<md.RemitoProveedor>(
      environment.APIS.REMITO.BASE,
      remito
    )
      .pipe(
        tap(
          remito => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public deleteRemito(id: number): Observable<md.RemitoProveedor> {
    return this.httpClient.delete<md.RemitoProveedor>(
      environment.APIS.REMITO.BASE + `/${id}`
    )
      .pipe(
        tap(
          remito => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }
}
