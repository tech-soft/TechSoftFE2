import { inject, TestBed } from '@angular/core/testing';

import { MessageService } from './message.service';

describe('MessageService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        MessageService
      ]
    });
  });

  it('Se espera que el servicio esté creado', inject([MessageService], (service: MessageService) => {
    expect(service)
.toBeTruthy();
  }));
});
