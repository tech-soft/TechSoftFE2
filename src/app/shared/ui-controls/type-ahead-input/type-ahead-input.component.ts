import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { Observable } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

@Component({
  selector: 'app-type-ahead-input',
  templateUrl: './type-ahead-input.component.html',
  styleUrls: ['./type-ahead-input.component.scss']
})
export class TypeAheadInputComponent{

  // TODO: change src for type of object
  // TODO: try to use descripcionReducida
  @Input() src: { id: number, descripcion: string, descripcionReducida: string }[];
  @Output() selectedObject = new EventEmitter<{ id: number, descripcion: string, descripcionReducida: string }>();

  public model: { id: number, descripcion: string, descripcionReducida: string };

  formatter = (x: { descripcion: string }) => x.descripcion;
  
  // TODO: change this for a service call
  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.src.filter(v => v.descripcion.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  selectObject($event, input) {
    $event.preventDefault();
    if ($event.item) {
      this.selectedObject.emit($event.item);
    }
    input.value = '';
  }
}
