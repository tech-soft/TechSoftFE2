import { Component, Input, Output, EventEmitter, OnInit } from '@angular/core';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';
import { Observable, throwError } from 'rxjs';

import * as md from '../../../_models';
import { ProductosService } from '../../services/productos.service';

@Component({
  selector: 'app-type-ahead-productos',
  templateUrl: './type-ahead-productos.component.html',
  styleUrls: ['./type-ahead-productos.component.scss']
})
export class TypeAheadProductosComponent implements OnInit {

  @Output() selectedObject = new EventEmitter<md.ProductoSeleccionado>();

  productos: Array<md.ProductoPreview>;
  producto: md.ProductoSeleccionado = <md.ProductoSeleccionado>{};
  model: { id: number, descripcion: string };

  // TODO: permitir ingresar solo nro y sacar arrows
  cantidad: number;

  constructor(
    private productosService: ProductosService
  ) {
  }

  ngOnInit(): void {
    this.productosService.getProductosPreview()
      .subscribe(
        result => {
          this.productos = result;
        },
        error => {
          alert(error.message);
          return throwError(error);
        });
  }

  formatter = (x: { descripcion: string }) => x.descripcion;

  search = (text$: Observable<string>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.productos.filter(v => v.descripcion.toLowerCase().indexOf(term.toLowerCase()) > -1).slice(0, 10))
    );

  selectObject($event, descripcionInput, cantidadInput) {
    $event.preventDefault();
    if ($event.item) {
      this.producto.id = $event.item.id;
      this.producto.descripcion = $event.item.descripcion;
      this.producto.cantidad = this.cantidad;
      this.selectedObject.emit(this.producto);
    }
    descripcionInput.value = '';
    cantidadInput.value = '';
  }
}

