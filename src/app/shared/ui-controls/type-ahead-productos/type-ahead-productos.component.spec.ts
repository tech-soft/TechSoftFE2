import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TypeAheadProductosComponent } from './type-ahead-productos.component';

describe('TypeAheadProductosComponent', () => {
  let component: TypeAheadProductosComponent;
  let fixture: ComponentFixture<TypeAheadProductosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TypeAheadProductosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TypeAheadProductosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component).toBeTruthy();
  // });
});
