import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbDateParserFormatter, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { CrudButtonsComponent } from './crud-buttons/crud-buttons.component';
import { MessagesComponent } from './messages/messages.component';
import { CompraService } from './services/compra.service';
import { CustomNgbDateParserFormatter } from './services/custom-ngb-date-parser-formatter';
import { EstadosService } from './services/estados.service';
import { FacturaService } from './services/factura.service';
import { MessageService } from './services/message.service';
import { OrdenCompraService } from './services/orden-compra.service';
import { PrecioListasService } from './services/precio-listas.service';
import { RemitoProveedorService } from './services/remito-proveedor.service';
import { SharedRoutingModule } from './shared-routing.module';
import { TypeAheadInputComponent } from './ui-controls/type-ahead-input/type-ahead-input.component';
import { TypeAheadProductosComponent } from './ui-controls/type-ahead-productos/type-ahead-productos.component';
import { ConvertersService } from './utils/converters.service';

@NgModule({
  declarations: [
    MessagesComponent,
    CrudButtonsComponent,
    TypeAheadInputComponent,
    TypeAheadProductosComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule.forRoot(),
    SharedRoutingModule
  ],
  exports: [
    CommonModule,
    FormsModule,
    MessagesComponent,
    CrudButtonsComponent,
    TypeAheadInputComponent,
    TypeAheadProductosComponent
  ],
  providers: [
    { provide: NgbDateParserFormatter, useFactory: () => new CustomNgbDateParserFormatter('longDate') },
    ConvertersService,
    EstadosService,
    MessageService,
    PrecioListasService,
    CompraService,
    OrdenCompraService,
    RemitoProveedorService,
    FacturaService
  ]
})
export class SharedModule { }
