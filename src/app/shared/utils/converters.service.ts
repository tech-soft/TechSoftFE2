import { Injectable } from '@angular/core';
import { NgbDateParserFormatter, NgbDateStruct } from '@ng-bootstrap/ng-bootstrap';

@Injectable({
  providedIn: 'root'
})
export class ConvertersService {

  model: NgbDateStruct;
  dateString: string;

  constructor(
    private ngbDateParserFormatter: NgbDateParserFormatter
  ) {
  }

  ngbDateStructToEpochString(datePickerDate: NgbDateStruct): string {
    return (datePickerDate) ?
      (new Date(datePickerDate.year, datePickerDate.month - 1, datePickerDate.day).getTime() / 1000).toString() : undefined;
  }

  onSelectDate(date: NgbDateStruct): string {
    if (date != null) {
      this.model = date;   //needed for first time around due to ngModel not binding during ngOnInit call. Seems like a bug in ng2.
      this.dateString = this.ngbDateParserFormatter.format(date);
      return this.dateString;
    } else {
      return undefined;
    }
  }

  setDefaultDate(): NgbDateStruct {
    var startDate = new Date();
    let startYear = startDate.getFullYear().toString();
    let startMonth = startDate.getMonth() + 1;
    let startDay = "1";

    return this.ngbDateParserFormatter.parse(startYear + "-" + startMonth.toString() + "-" + startDay);
  }

}
