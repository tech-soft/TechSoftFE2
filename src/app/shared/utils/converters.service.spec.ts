import { inject, TestBed } from '@angular/core/testing';
import { NgbDateParserFormatter } from '@ng-bootstrap/ng-bootstrap';
import { ConvertersService } from './converters.service';
import { CustomNgbDateParserFormatter } from '../services/custom-ngb-date-parser-formatter';



describe('ConvertersService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        { provide: NgbDateParserFormatter, useFactory: () => new CustomNgbDateParserFormatter('longDate') },
        ConvertersService
      ]
    });
  });

  it('should be created', inject([ConvertersService], (service: ConvertersService) => {
    expect(service).toBeTruthy();
  }));
});
