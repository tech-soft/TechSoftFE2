import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject, Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

const helper = new JwtHelperService();
const TOKEN_KEY = 'access_token';

@Injectable({
  providedIn: 'root'
})
export class AuthenticationService {

  token: string;

  private loggedIn = new BehaviorSubject<boolean>(false);

  constructor(
    private httpClient: HttpClient
  ) { }

  login(username: String, password: String): Observable<boolean> {

    return this.httpClient
      .post<md.AuthToken>(
        environment.APIS.AUTH,
        JSON.stringify({ username, password })
      )
      .pipe(
        map((response: md.AuthToken) => {
          const token = response && response.token;

          if (token) {
            this.token = token;
            this.setAccessToken(token);

            this.loggedIn.next(true);

            return true;
          } else {

            this.loggedIn.next(false);

            return false;
          }
        }),
        catchError(
          (err: any, caught: Observable<any>) => {
            this.loggedIn.next(false);
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)          
        }
      ));
  }

  logout(): void {
    this.token = undefined;
    this.removeAccessToken();
    this.loggedIn.next(false);
  }

  get isLoggedIn(): Observable<boolean> {
    if (this.getAccessToken()) {
      this.loggedIn.next(true);
    } else {
      this.loggedIn.next(false);
    }

    return this.loggedIn.asObservable();
  }

  setAccessToken(token: string): void {
    localStorage.setItem(TOKEN_KEY, token);
  }

  getAccessToken(): String {

    return localStorage.getItem(TOKEN_KEY);
  }

  removeAccessToken(): void {
    return localStorage.removeItem(TOKEN_KEY);
  }
}
