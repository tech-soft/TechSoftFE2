import { HttpClientModule, HttpRequest } from '@angular/common/http';
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { MessageService } from '../../shared/services/message.service';
import { AuthenticationService } from './authentication.service';
import { AuthToken } from '../../_models';

let store = {};

const mockLocalStorage = {
  getItem: (key: string): string => {
    return key in store ? store[key] : undefined;
  },
  setItem: (key: string, value: string) => {
    store[key] = `${value}`;
  },
  removeItem: (key: string) => {
    delete store[key];
  },
  clear: () => {
    store = {};
  }
};

describe('AuthenticationService', () => {

  let service: AuthenticationService;
  let httpMock: HttpTestingController;

  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientModule,
        HttpClientTestingModule
      ],
      providers: [
        AuthenticationService
      ]
    });

    service = TestBed.get(AuthenticationService);
    httpMock = TestBed.get(HttpTestingController);
    spyOn(localStorage, 'getItem').and.callFake(mockLocalStorage.getItem);
    spyOn(localStorage, 'setItem').and.callFake(mockLocalStorage.setItem);
    spyOn(localStorage, 'removeItem').and.callFake(mockLocalStorage.removeItem);
    spyOn(localStorage, 'clear').and.callFake(mockLocalStorage.clear);

  });

  afterEach(() => {
    80
    httpMock.verify();
  });

  it('Se espera que con el logout se limpie el token', () => {
    localStorage.setItem('access_token', 'anothertoken');
    service.token = 'not undefined';

    service.logout();

    expect(service.token).toBeUndefined('Se espera token nulo');
    expect(service.getAccessToken()).toBeFalsy('Se espera token eliminado del localStorage');
  });

  it('Se espera almacenar token en localStorage', () => {
    service.setAccessToken('sometoken');

    expect(localStorage.getItem('access_token')).toEqual('sometoken');
  });

  it('Se espera devolver el token almacenado en localStorage', () => {
    localStorage.setItem('access_token', 'anothertoken');

    expect(service.getAccessToken()).toEqual('anothertoken');
  });

  it('Se espera remover el token almacenado en localStorage', () => {
    localStorage.setItem('access_token', 'anothertoken');
    localStorage.removeItem('access_token');

    expect(service.getAccessToken()).toBeFalsy();
  });

  it('Se espera http POST', () => {
    const mockToken: AuthToken = { token: '123456' };

    service.login('foo', 'bar').subscribe(
      response => {}
    );

    const request = httpMock.expectOne(environment.APIS.AUTH, 'Se espera url .../auth');
    expect(request.request.method).toBe('POST', 'Se espeta método POST');

    request.flush(mockToken);
  });

});
