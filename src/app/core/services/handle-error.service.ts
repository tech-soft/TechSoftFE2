import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';

import { MessageService } from '../../shared/services/message.service';

@Injectable()
export class HandleErrorService {

  constructor(private messageService: MessageService) { }

  handleError<T>(operation = 'operation', result?: T): any {

    return (error: any): Observable<T> => {
      console.error(error);
      this.log(`${operation} failed: ${error.message}`);

      return of(result as T);
    };
  }

  log(message: String): void {
    this.messageService.addError(`DEPRECATED:${message}`);
  }

}
