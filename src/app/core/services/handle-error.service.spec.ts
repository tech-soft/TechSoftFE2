import { inject, TestBed } from '@angular/core/testing';

import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../services/handle-error.service';


describe('HandleErrorService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [
        HandleErrorService,
        MessageService
      ]
    });
  });

  // it('Se espera que el servicio esté creado', inject([HandleErrorService], (service: HandleErrorService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));
});
