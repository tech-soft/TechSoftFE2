import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { AuthenticationService } from '../services/authentication.service';
import { HandleErrorService } from '../services/handle-error.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class UserService {

  constructor(
    private httpClient: HttpClient,
    private authenticationService: AuthenticationService,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService) {
  }

  getUsers(): Observable<Array<md.User>> {

    return this.httpClient
      .get<Array<md.User>>(
        environment.APIS.USUARIO.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getUsers', []))
      );
  }

  getUserByUsername(username: String): Observable<md.User> {

    return this.httpClient
      .get<md.User>(
        environment.APIS.USUARIO.BY_USERNAME + `/${username}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.User>(`getUserByUsername username=${username}`))
      );
  }

}
