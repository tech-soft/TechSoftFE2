import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { MessageService } from '../../shared/services/message.service';
import { AuthenticationService } from '../services/authentication.service';
import { HandleErrorService } from '../services/handle-error.service';
import { UserService } from './user.service';

describe('UserService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        UserService,
        HandleErrorService,
        AuthenticationService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([UserService], (service: UserService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/usuarios',
  //   async(
  //     inject([UserService, HttpTestingController],
  //       (service: UserService, backend: HttpTestingController) => {

  //         service.getUsers()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);

  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );
});
