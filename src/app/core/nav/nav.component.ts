import { Component, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/authentication.service';


@Component({
  selector: 'app-nav',
  templateUrl: './nav.component.html',
  styleUrls: ['./nav.component.scss']
})
export class NavComponent implements OnInit {

  @Input() usuario;
  isLoggedIn: Observable<boolean>;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.isLoggedIn = this.authenticationService.isLoggedIn;
  }

  goto(menuOption: String): void {

    switch (menuOption) {
      case 'Logout':
        this.authenticationService.logout();
        break;
      default:
        break;
    }

  }

}
