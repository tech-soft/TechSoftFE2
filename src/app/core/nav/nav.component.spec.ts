import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';

import { AuthenticationService } from '../services/authentication.service';
import { NavComponent } from './nav.component';

// class MockAuthenticationService {
//   isLoggedIn = true;
//   token = 'mock token';
// };

describe('NavComponent', () => {
  let component: NavComponent;
  let fixture: ComponentFixture<NavComponent>;
  // const de: DebugElement;
  // const el: HTMLElement;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        NavComponent
      ],
      imports: [
        BrowserModule,
        FormsModule,
        HttpClientModule,
        ReactiveFormsModule,
        RouterTestingModule
      ],
      providers: [
        HttpClient,
        AuthenticationService
        // {
        //   provide: AuthenticationService,
        //   useClass: MockAuthenticationService
        // }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(NavComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });
  // TODO
  // it('el componente debería estar definido', () => {
  //   expect(component)
  //     .toBeDefined();
  // });

  // it('onInit el estado de isLoggedIn debe coincidir con el del servicio de autentiación',
  //   inject([AuthenticationService], (service: AuthenticationService) => {
  //     component.ngOnInit();
  //     expect(component.isLoggedIn)
  // .toEqual(service.isLoggedIn);
  //   }));

  // it('debe contener <nav>',
  // fakeAsync(inject([AuthenticationService], (service: AuthenticationService) => {
  //     // const debugElement: DebugElement = fixture.debugElement;
  //     // const appElement: HTMLElement = debugElement.nativeElement;
  //     // console.log('HERE!!!!!!!!!!!!!!', component);

  //     // const nav = appElement.querySelector('nav');

  //     // expect(nav)
  // .not.toBeNull('Se esperaba <nav>');

  //     component.ngOnInit();
  //     console.log('HERE!!!!!!!!!!!!!!', component.isLoggedIn);
  //     tick(400);
  //     fixture.detectChanges();
  //     de = fixture.debugElement.query(By.css('nav'));
  //     el = de.nativeElement;
  //     console.log('HERE!!!!!!!!!!!!!!', el);

  //     // const bannerDe: DebugElement = fixture.debugElement;
  //     // const paragraphDe = bannerDe.query(By.css('nav'));
  //     // const p: HTMLElement = paragraphDe.nativeElement;
  //     expect(el)
  // .not.toBeNull('Se esperaba <nav>');
  //   })));
});
