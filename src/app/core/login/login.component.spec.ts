import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule, NgForm } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { By } from '@angular/platform-browser';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of } from 'rxjs';

import { MessagesComponent } from '../../shared/messages/messages.component';
import { MessageService } from '../../shared/services/message.service';
import { CoreRoutingModule } from '../core-routing.module';
import { AuthenticationService } from '../services/authentication.service';
import { LoginComponent } from './login.component';

describe('LoginComponent', () => {

  let component: LoginComponent;
  let fixture: ComponentFixture<LoginComponent>;
  let authenticationService: AuthenticationService;
  let messageService: MessageService;

  beforeEach(
    async(() => {
      TestBed.configureTestingModule({
        declarations: [
          LoginComponent,
          MessagesComponent
        ],
        imports: [
          FormsModule,
          HttpClientModule,
          RouterTestingModule
        ],
        providers: [
          AuthenticationService,
          MessageService
        ]
      })
        .compileComponents();

      fixture = TestBed.createComponent(LoginComponent);
      component = fixture.componentInstance;
      fixture.detectChanges();
      authenticationService = TestBed.get(AuthenticationService);
      messageService = TestBed.get(MessageService);
    })
  );

  it('Se debe desloguear el usuario al iniciar el componente', fakeAsync(() => {
    spyOn(authenticationService, 'logout');
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(authenticationService.logout).toHaveBeenCalled();
  }));

  it('Se debe mostrar error de credenciales cuando son inválidas', fakeAsync(() => {
    spyOn(authenticationService, 'login').and.returnValue(of(false));
    component.ngOnInit();
    component.login();

    tick();
    fixture.detectChanges();
    expect(messageService.messages.length).toBe(1);
    expect(messageService.messages[0].message).toBe("Credenciales inválidas.");
    expect(component.loading).toBeFalsy();

  }));

  it('No se deben mostrar errores cuando las credenciales son válidas', fakeAsync(() => {
    spyOn(authenticationService, 'login').and.returnValue(of(true));
    component.ngOnInit();
    component.login();

    tick();
    fixture.detectChanges();
    expect(messageService.messages.length).toBe(0);

  }));

});
