import { Component, Input, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { throwError } from 'rxjs';

import { environment } from '../../../environments/environment';
import { MessageService } from '../../shared/services/message.service';
import { AuthenticationService } from '../services/authentication.service';
import { UserService } from '../services/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})

export class LoginComponent implements OnInit {

  username: String;
  password: String;

  loading: boolean;
  submitted: boolean;

  keyUpUsername: string;
  keyUpPassword: string;

  constructor(
    private router: Router,
    private authenticationService: AuthenticationService,
    private messageService: MessageService
  ) {

    this.loading = false;
    this.submitted = false;

  }

  ngOnInit(): void {

    this.authenticationService.logout();

  }

  doKeyUp(event: any): void {

    const propName = `keyUp${event.target.id}`;
    this[propName] = true;

    if (event.target.value === '') {
      this[propName] = false;
    }

  }

  validateForm(f: NgForm): void {

    this.loading = true;
    this.submitted = true;
    this.messageService.clear();

    if (!f.valid) {
      this.loading = false;
      this.messageService.addError('Formulario inválido: revise los campos obligatorios!');
    } else {
      this.login();
    }

  }

  login(): void {

    this.authenticationService
      .login(this.username, this.password)
      .subscribe(
        result => {
          if (result === true) {
            this.router.navigate(['/']);
          } else {
            this.messageService.addError('Credenciales inválidas');
            this.loading = false;
          }
        },
        error => {
          if (error.status === environment.HTTP.ERROR.FORBIDDEN) {
            this.messageService.addError('Credenciales inválidas');
          } else {
            this.messageService.addError(`Error desconocido.`);
          }

          this.loading = false;

          return throwError(error);
        }
      );
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }

}
