import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

import { SharedModule } from '../shared/shared.module';
import { CoreRoutingModule } from './core-routing.module';
import { DashboardComponent } from './dashboard/dashboard.component';
import { LoginComponent } from './login/login.component';
import { MenuBarComponent } from './menu-bar/menu-bar.component';
import { NavComponent } from './nav/nav.component';
import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AuthenticationService } from './services/authentication.service';
import { HandleErrorService } from './services/handle-error.service';
import { UserService } from './services/user.service';

@NgModule({
  declarations: [
    DashboardComponent,
    LoginComponent,
    MenuBarComponent,
    NavComponent,
    PageNotFoundComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    ReactiveFormsModule,
    CoreRoutingModule,
    SharedModule
  ],
  exports: [
    DashboardComponent,
    LoginComponent,
    MenuBarComponent,
    NavComponent,
    PageNotFoundComponent
  ],
  providers: [
    AuthenticationService,
    HandleErrorService,
    UserService
  ]
})
export class CoreModule { }
