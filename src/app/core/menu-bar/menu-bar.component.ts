import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Observable } from 'rxjs';

import { AuthenticationService } from '../services/authentication.service';

@Component({
  selector: 'app-menu-bar',
  templateUrl: './menu-bar.component.html',
  styleUrls: ['./menu-bar.component.scss']
})
export class MenuBarComponent implements OnInit {

  isLoggedIn: Observable<boolean>;
  expandCompras: boolean;

  constructor(
    private authenticationService: AuthenticationService,
    private router: Router) {

  }

  ngOnInit(): void {
    this.isLoggedIn = this.authenticationService.isLoggedIn;
    this.expandCompras = false;
  }

  toggle(menuOption: String): void {

    switch (menuOption) {
      case 'Compras':
        this.expandCompras = !this.expandCompras;
        break;
      default:
        break;
    }

  }

  goto(menuOption: String): void {

    switch (menuOption) {
      case 'PersonaSearch':
        this.router.navigate(['/personas/search']);
        break;
      case 'ProductoSearch':
        this.router.navigate(['/productos/search']);
        break;
      case 'OrdenComprasSearch':
        this.router.navigate(['/orden-compras/search']);
        break;
      case 'RemitosProveedorSearch':
        this.router.navigate(['/proveedores/remitos/search']);
        break;
      case 'FacturasProveedorSearch':
        this.router.navigate(['/proveedores/facturas/search']);
        break;
      default:
        break;
    }

  }

}
