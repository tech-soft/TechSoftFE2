import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']

  // https://scotch.io/tutorials/all-the-ways-to-add-css-to-angular-2-components

})
export class DashboardComponent implements OnInit {

  constructor() {
  }

  ngOnInit(): void {
  }

}
