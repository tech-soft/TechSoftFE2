import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../shared/shared.module';
import { VentasRoutingModule } from './ventas-routing.module';

@NgModule({
  declarations: [
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    VentasRoutingModule,
    SharedModule
  ],
  exports: [
  ]
})
export class VentasModule { }
