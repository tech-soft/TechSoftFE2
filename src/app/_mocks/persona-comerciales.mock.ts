
import * as md from '../_models';

export const PERSONA_COMERCIAL: Array<md.PersonaComercial> = [
  {
  id: 1,
  personaId: 1,
  ivaTipo: {
    id: 1,
    descripcion: 'tipo 1'
  },
  pagoForma: {
    id: 1,
    descripcion: 'CONTADO'
  },
  precioLista: {
    id: 1,
    descripcion: 'LISTA 1'
  },
  descuento: 50
}];
