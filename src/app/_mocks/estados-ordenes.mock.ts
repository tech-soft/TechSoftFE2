
import * as md from '../_models';

export const ESTADOS_ORDENES: Array<md.Estado> = [
  {
    id: 1000,
    descripcion: 'NUEVA'
  },
  {
    id: 1001,
    descripcion: 'ENVIADA'
  },
  {
    id: 1002,
    descripcion: 'RECIBIDA'
  },
  {
    id: 1003,
    descripcion: 'CANCELADA'
  }
];
