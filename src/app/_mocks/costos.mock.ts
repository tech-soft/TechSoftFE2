import * as md from '../_models';


export const COSTOS: Array<md.Costo> = [
  {
    id: 1,
    articulo: {
      id: 1,
      descripcion: 'Notebook MSI GT685',
      descripcionReducida: 'GT685',
      codigoBarras: '123123123123',
      nroSerie: true,
      costo: 10000,
      stockEntrante: 0,
      stockDisponible: 1,
      gravado: {
        id: 2,
        porcentaje: 21
      },
      origen: {
        id: 2,
        descripcion: 'IMPORTADO'
      },
      estado: {
        id: 1,
        descripcion: 'ACTIVO'
      },
      precios: [
        {
          id: 1,
          lista: {
            id: 1,
            descripcion: 'Lista 1'
          },
          renta: 10,
          precioFinal: 13310
        }
      ]
    },
    proveedor: {
      id: 1,
      nombre: 'Air Commputers',
      documentoTipo: {
        id: 1,
        descripcion: 'DNI',
        descripcionReducida: 'DNI'
      },
      documentoNumero: 22255533,
      estado: {
        id: 1,
        descripcion: 'Activo'
      },
      cliente: false,
      proveedor: true
    },
    fecha: '2018-02-01',
    costo: 10000,
    cantidad: 10
  },
  {
    id: 2,
    articulo: {
      id: 1,
      descripcion: 'Notebook MSI GT685',
      descripcionReducida: 'GT685',
      codigoBarras: '123123123123',
      nroSerie: true,
      costo: 10000,
      stockEntrante: 0,
      stockDisponible: 1,
      gravado: {
        id: 2,
        porcentaje: 21
      },
      origen: {
        id: 2,
        descripcion: 'IMPORTADO'
      },
      estado: {
        id: 1,
        descripcion: 'ACTIVO'
      },
      precios: [
        {
          id: 1,
          lista: {
            id: 1,
            descripcion: 'Lista 1'
          },
          renta: 10,
          precioFinal: 13310
        }
      ]
    },
    proveedor: {
      id: 1,
      nombre: 'Air Commputers',
      documentoTipo: {
        id: 1,
        descripcion: 'DNI',
        descripcionReducida: 'DNI'
      },
      documentoNumero: 22255533,
      estado: {
        id: 1,
        descripcion: 'Activo'
      },
      cliente: false,
      proveedor: true
    },
    fecha: '2017-11-15',
    costo: 9000,
    cantidad: 3
  },
  {
    id: 3,
    articulo: {
      id: 1,
      descripcion: 'Notebook MSI GT685',
      descripcionReducida: 'GT685',
      codigoBarras: '123123123123',
      nroSerie: true,
      costo: 10000,
      stockEntrante: 0,
      stockDisponible: 1,
      gravado: {
        id: 2,
        porcentaje: 21
      },
      origen: {
        id: 2,
        descripcion: 'IMPORTADO'
      },
      estado: {
        id: 1,
        descripcion: 'ACTIVO'
      },
      precios: [
        {
          id: 1,
          lista: {
            id: 1,
            descripcion: 'Lista 1'
          },
          renta: 10,
          precioFinal: 13310
        }
      ]
    },
    proveedor: {
      id: 1,
      nombre: 'Air Commputers',
      documentoTipo: {
        id: 1,
        descripcion: 'DNI',
        descripcionReducida: 'DNI'
      },
      documentoNumero: 22255533,
      estado: {
        id: 1,
        descripcion: 'Activo'
      },
      cliente: false,
      proveedor: true
    },
    fecha: '2017-10-01',
    costo: 7000,
    cantidad: 4
  }
];
