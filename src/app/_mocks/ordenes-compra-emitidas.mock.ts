  
import * as md from '../_models';

export const ORDENES_COMPRA_EMITIDAS: Array<md.OrdenCompraEmitida> = [
  {
    id: 1,
    puesto: 1,
    numero: 1234,
    seleccionada: true
  },
  {
    id: 2,
    puesto: 2,
    numero: 5678,
    seleccionada: false
  }
];