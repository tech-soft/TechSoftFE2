
import * as md from '../_models';

export const IVA_TIPOS: Array<md.IvaTipo> = [
  {
    id: 1,
    descripcion: 'tipo 1'
  },
  {
    id: 2,
    descripcion: 'tipo 2'
  },
  {
    id: 3,
    descripcion: 'tipo 3'
  }
];
