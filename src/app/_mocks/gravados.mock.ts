
import * as md from '../_models';

export const GRAVADOS: Array<md.Gravado> = [
  {
    id: 1,
    porcentaje: 10.5
  },
  {
    id: 2,
    porcentaje: 21
  }
];
