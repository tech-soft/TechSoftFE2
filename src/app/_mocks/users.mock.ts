
import * as md from '../_models';

export const USUARIOS: Array<md.User> = [
  {
  id: 1,
  persona: {
    id: 1,
    nombre: 'Guille',
    documentoTipo: {
      id: 1,
      descripcion: 'DNI',
      descripcionReducida: 'DNI'
    },
    documentoNumero: 11123123,
    estado: {
      id: 1,
      descripcion: 'Activo'
    },
    cliente: true,
    proveedor: true
  },
  username: 'foo',
  password: 'var',
  activo: true
}];
