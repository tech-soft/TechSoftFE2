import * as md from '../_models';

export const ARTICULOS: Array<md.Articulo> = [
  {
    id: 1,
    descripcion: 'Notebook MSI GT685',
    descripcionReducida: 'GT685',
    codigoBarras: '123123123123',
    nroSerie: true,
    costo: 10000,
    stockEntrante: 1,
    stockDisponible: 0,
    gravado: {
      id: 2,
      porcentaje: 21
    },
    origen: {
      id: 2,
      descripcion: 'IMPORTADO'
    },
    estado: {
      id: 1,
      descripcion: 'ACTIVO'
    },
    precios: [{
        id: 1,
        lista: {
          id: 1,
          descripcion: 'Lista 1'
        },
        renta: 10,
        precioFinal: 13310
      },
      {
        id: 2,
        lista: {
          id: 2,
          descripcion: 'Lista 2'
        },
        renta: 20,
        precioFinal: 14520
      },
      {
        id: 3,
        lista: {
          id: 3,
          descripcion: 'Lista 3'
        },
        renta: 30,
        precioFinal: 15730
      },
      {
        id: 4,
        lista: {
          id: 4,
          descripcion: 'Lista 4'
        },
        renta: 40,
        precioFinal: 16940
      },
      {
        id: 5,
        lista: {
          id: 5,
          descripcion: 'Lista 5'
        },
        renta: 50,
        precioFinal: 18150
      },
      {
        id: 6,
        lista: {
          id: 6,
          descripcion: 'Lista 6'
        },
        renta: 60,
        precioFinal: 19360
      }
    ]
  },
  {
    id: 2,
    descripcion: 'Monitor Samsung TTT',
    descripcionReducida: 'SGTTT',
    codigoBarras: '999999999',
    nroSerie: true,
    costo: 2000,
    stockEntrante: 10,
    stockDisponible: 2,
    gravado: {
      id: 2,
      porcentaje: 10.5
    },
    origen: {
      id: 2,
      descripcion: 'NACIONAL'
    },
    estado: {
      id: 1,
      descripcion: 'ACTIVO'
    },
    precios: [{
        id: 1,
        lista: {
          id: 1,
          descripcion: 'Lista 1'
        },
        renta: 10,
        precioFinal: 2200
      },
      {
        id: 2,
        lista: {
          id: 2,
          descripcion: 'Lista 2'
        },
        renta: 20,
        precioFinal: 2400
      },
      {
        id: 3,
        lista: {
          id: 3,
          descripcion: 'Lista 3'
        },
        renta: 30,
        precioFinal: 2600
      },
      {
        id: 4,
        lista: {
          id: 4,
          descripcion: 'Lista 4'
        },
        renta: 40,
        precioFinal: 2800
      },
      {
        id: 5,
        lista: {
          id: 5,
          descripcion: 'Lista 5'
        },
        renta: 50,
        precioFinal: 3000
      },
      {
        id: 6,
        lista: {
          id: 6,
          descripcion: 'Lista 6'
        },
        renta: 60,
        precioFinal: 3200
      }
    ]
  }
];
