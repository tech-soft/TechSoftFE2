import * as md from '../_models';


export const CONTACTOS: Array<md.Contacto> = [
  {
    id: 1,
    tipo: 1,
    contacto: '11-123456',
    descripcion: 'casa',
    personaId: 1
  },
  {
    id: 2,
    tipo: 2,
    contacto: 'test@test.com',
    descripcion: 'personal',
    personaId: 2
  }
];
