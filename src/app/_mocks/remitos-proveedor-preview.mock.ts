
import * as md from '../_models';

export const REMITOS_PROVEEDOR_PREVIEW: Array<md.RemitoProveedorPreview> = [
  {
    id: 1,
    puestoId: 1,
    numero: 1,
    proveedor: {
      id: 3,
      nombre: 'Airoldi'
    },
    fecha: '2018-01-01'
  },
  {
    id: 2,
    puestoId: 1,
    numero: 2,
    proveedor: {
      id: 4,
      nombre: 'Air'
    },
    fecha: '2018-02-01'
  }
];