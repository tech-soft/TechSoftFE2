
import * as md from '../_models';

export const ORIGENES: Array<md.Origen> = [
  {
    id: 1,
    descripcion: 'NACIONAL'
  },
  {
    id: 2,
    descripcion: 'IMPORTADO'
  }
];
