import * as md from '../_models';

export const CIUDADES: Array<md.Ciudad> = [
  {
    id: 1,
    nombre: 'ROSARIO',
    codigoPostal: '2000',
    provincia: {
      id: 1,
      nombre: 'SANTA FE',
      pais: {
        id: 1,
        nombre: 'ARGENTINA'
      }
    }
  },
  {
    id: 2,
    nombre: 'SAN LORENZO',
    codigoPostal: '2000',
    provincia: {
      id: 1,
      nombre: 'SANTA FE',
      pais: {
        id: 1,
        nombre: 'ARGENTINA'
      }
    }
  },
  {
    id: 3,
    nombre: 'CORDOBA',
    codigoPostal: '3000',
    provincia: {
      id: 2,
      nombre: 'CORDOBA',
      pais: {
        id: 1,
        nombre: 'ARGENTINA'
      }
    }
  },
  {
    id: 4,
    nombre: 'MONTEVIDEO',
    codigoPostal: '9999',
    provincia: {
      id: 3,
      nombre: 'MONTEVIDEO',
      pais: {
        id: 2,
        nombre: 'URUGUAY'
      }
    }
  }
];
