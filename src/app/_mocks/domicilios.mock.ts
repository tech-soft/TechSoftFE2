
import * as md from '../_models';

export const DOMICILIOS: Array<md.Domicilio> = [
  {
  id: 1,
  personaId: 1,
  calle: 'jujuy',
  nro: '1234',
  piso: '1',
  dpto: 'A',
  ciudad: {
    id: 1,
    nombre: 'ROSARIO',
    codigoPostal: '2000',
    provincia: {
      id: 1,
      nombre: 'SANTA FE',
      pais: {
        id: 1,
        nombre: 'ARGENTINA'
      }
    }
  },
  descripcion: 'casa',
  principal: true
}];
