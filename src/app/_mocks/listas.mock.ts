
import * as md from '../_models';

export const LISTAS_PRECIO: Array<md.PrecioLista> = [
  {
    id: 1,
    descripcion: 'Lista 1'
  },
  {
    id: 2,
    descripcion: 'Lista 2'
  },
  {
    id: 3,
    descripcion: 'Lista 3'
  },
  {
    id: 4,
    descripcion: 'Lista 4'
  },
  {
    id: 5,
    descripcion: 'Lista 5'
  },
  {
    id: 6,
    descripcion: 'Lista 6'
  }
];
