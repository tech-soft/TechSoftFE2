
import * as md from '../_models';

export const ORDENES_COMPRA_PREVIEW: Array<md.OrdenCompraPreview> = [
  {
    id: 1,
    puesto: 1,
    numero: 1,
    proveedor: {
      id: 3,
      nombre: 'Airoldi'
    },
    fechaCreacion: '2018-01-01',
    estado: {
      id: 1000,
      descripcion: 'Nueva'
    }
  },
  {
    id: 2,
    puesto: 1,
    numero: 2,
    proveedor: {
      id: 4,
      nombre: 'Air'
    },
    fechaCreacion: '2018-02-01',
    estado: {
      id: 1000,
      descripcion: 'Nueva'
    }
  }
];