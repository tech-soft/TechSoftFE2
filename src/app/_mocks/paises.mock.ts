
import * as md from '../_models';

export const PAISES: Array<md.Pais> = [
  {
    id: 1,
    nombre: 'ARGENTINA'
  },
  {
    id: 2,
    nombre: 'URUGUAY'
  }
];
