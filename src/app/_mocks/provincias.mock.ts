
import * as md from '../_models';

export const PROVINCIAS: Array<md.Provincia> = [
  {
    id: 1,
    nombre: 'SANTA FE',
    pais: {
      id: 1,
      nombre: 'ARGENTINA'
    }
  },
  {
    id: 2,
    nombre: 'CORDOBA',
    pais: {
      id: 1,
      nombre: 'ARGENTINA'
    }
  }
];
