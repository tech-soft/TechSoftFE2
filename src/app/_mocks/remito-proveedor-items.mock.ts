
import * as md from '../_models';
import * as mk from '.';

export const REMITO_PROVEEDOR_ITEMS: Array<md.RemitoProveedorItem> = [
    {
        id: 1,
        producto: {
            id: 1,
            descripcion: 'Monitor'
        },
        cantidad: 1,
        numerosSerie: [
            '123456789'
        ],
        ordenCompra: {
            id: 1,
            puesto: 1,
            numero: 1,
            seleccionada: false
          },
        cantidadOrdenCompra: 1
    },
    {
        id: 2,
        producto: {
            id: 2,
            descripcion: 'Mouse'
        },
        cantidad: 2,
        numerosSerie: [
            '123456789',
            '123456456'
        ],
        ordenCompra: {
            id: 1,
            puesto: 1,
            numero: 1,
            seleccionada: false
          },
        cantidadOrdenCompra: 2
    },
    {
        id: 3,
        producto: {
            id: 2,
            descripcion: 'Teclado'
        },
        cantidad: 2,
        numerosSerie: undefined,
        ordenCompra: {
            id: 1,
            puesto: 1,
            numero: 1,
            seleccionada: false
          },
        cantidadOrdenCompra: 2
    },
    {
        id: 4,
        producto: {
            id: 1,
            descripcion: 'Monitor'
        },
        cantidad: 1,
        numerosSerie: [
            '123456789'
        ],
        ordenCompra: {
            id: 1,
            puesto: 1,
            numero: 1,
            seleccionada: false
          },
        cantidadOrdenCompra: 1
    }
];
