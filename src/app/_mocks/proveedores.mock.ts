
import * as md from '../_models';

export const PROVEEDORES: Array<md.Proveedor> = [
  {
    id: 1,
    nombre: 'Airoldi'
  },
  {
    id: 2,
    nombre: 'Chitarroni SRL'
  },
  {
    id: 3,
    nombre: 'Air SA'
  }
];
