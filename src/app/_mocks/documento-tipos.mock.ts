import * as md from '../_models';


export const DOCUMENTO_TIPOS: Array<md.DocumentoTipo> = [
  {
     id: 1,
     descripcion: 'DOCUMENTO NACIONAL DE IDENTIDAD',
     descripcionReducida: 'DNI'
   },
   {
     id: 2,
     descripcion: 'LIBRETA DE ENROLAMIENTO',
     descripcionReducida: 'LE'
   }
 ];
