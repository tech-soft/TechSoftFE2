
import * as md from '../_models';

export const PERSONAS: Array<md.Persona> = [
  {
    id: 1,
    nombre: 'Guille',
    documentoTipo: {
      id: 1,
      descripcion: 'DNI',
      descripcionReducida: 'DNI'
    },
    documentoNumero: 11123123,
    estado: {
      id: 1,
      descripcion: 'Activo'
    },
    cliente: true,
    proveedor: true
  },
  {
    id: 2,
    nombre: 'Mike',
    documentoTipo: {
      id: 1,
      descripcion: 'DNI',
      descripcionReducida: 'DNI'
    },
    documentoNumero: 22255533,
    estado: {
      id: 1,
      descripcion: 'Activo'
    },
    cliente: false,
    proveedor: true
  }
];
