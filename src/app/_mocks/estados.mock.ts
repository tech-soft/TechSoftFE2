
import * as md from '../_models';

export const ESTADOS: Array<md.Estado> = [
  {
    id: 1,
    descripcion: 'ACTIVO'
  },
  {
    id: 2,
    descripcion: 'BAJA'
  }
];
