
import * as md from '../_models';

export const ORDENES_COMPRA: Array<md.OrdenCompra> = [
  {
    id: 1,
    puesto: {
      id: 1,
      codigo: 1,
      codigoString: '0001'
    },
    numero: 1,
    proveedor: {
      id: 1,
      nombre: 'Guille'
    },
    items: [
      {
        id: 1,
        producto: {
          id: 1,
          descripcion: 'Monitor'
        },
        cantidad: 1,
        precio: 100,
        estado: {
          id: 2,
          descripcion: 'ENVIADA'
        }
      }
    ],      
    fechaCreacion: '2018-04-02',
    fechaEntrega: '2018-04-02',
    estado: {
      id: 2,
      descripcion: 'ENVIADA'
    },
    observaciones: 'nada'
  },
  {
    id: 2,
    puesto: {
      id: 1,
      codigo: 1,
      codigoString: '0001'
    },
    numero: 2,
    proveedor: {
      id: 1,
      nombre: 'Guille'
    },
    items: [
      {
        id: 1,
        producto: {
          id: 1,
          descripcion: 'Monitor'
        },
        cantidad: 1,
        precio: 100,
        estado: {
          id: 2,
          descripcion: 'ENVIADA'
        }
      }
    ],      
    fechaCreacion: '2018-04-02',
    fechaEntrega: '2018-04-02',
    estado: {
      id: 1,
      descripcion: 'NUEVA'
    },
    observaciones: 'nada'
  }
];
