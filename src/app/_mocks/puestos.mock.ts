
import * as md from '../_models';

export const PUESTOS: Array<md.Puesto> = [
  {
    id: 1,
    codigo: 1,
    codigoString: '0001'
  },
  {
    id: 2,
    codigo: 2,
    codigoString: '0002'
  }
];
