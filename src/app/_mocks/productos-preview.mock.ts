
import * as md from '../_models';

export const PRODUCTOS_PREVIEW: Array<md.ProductoPreview> = [
  {
    id: 1,
    descripcion: 'Mouse Genius M100'
  },
  {
    id: 2,
    descripcion: 'Teclado Genius K500'
  },
  {
    id: 3,
    descripcion: 'Monitor Samsung SM2343'
  },
  {
    id: 4,
    descripcion: 'Mouse Logitech G100'
  },
  {
    id: 5,
    descripcion: 'Notebook MSI GT683'
  },
  {
    id: 6,
    descripcion: 'Gabinete basico negro'
  },
  {
    id: 7,
    descripcion: 'Headphones Genius 200'
  },
  {
    id: 8,
    descripcion: 'HP Impresora Laser Color P1500'
  },
  {
    id: 9,
    descripcion: 'EPSON Impresona Inkjet 400'
  },
  {
    id: 10,
    descripcion: 'Cartucho E200 Negro'
  }
];
