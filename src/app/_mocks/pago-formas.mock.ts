
import * as md from '../_models';

export const PAGO_FORMA: Array<md.PagoForma> = [
  {
    id: 1,
    descripcion: 'CONTADO'
  },
  {
    id: 2,
    descripcion: 'DEBITO'
  }
];
