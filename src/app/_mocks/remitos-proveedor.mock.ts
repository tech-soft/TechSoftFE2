
import * as md from '../_models';
import * as mk from '.';

export const REMITOS_PROVEEDOR: Array<md.RemitoProveedor> = [
    {
        id: 1,
        comprobanteTipo: {
            id: 1,
            letra: 'X',
            tipo: 1
        },
        puestoId: 1,
        numero: 111,
        proveedor: {
            id: 4,
            nombre: 'Air'
        },
        items: [
            {
                id: 1,
                producto: {
                    id: 1,
                    descripcion: 'Monitor'
                },
                cantidad: 1,
                numerosSerie: [
                    '123456789'
                ],
                ordenCompra: {
                    id: 1,
                    puesto: 1,
                    numero: 1,
                    seleccionada: false
                },
                cantidadOrdenCompra: 1
            },
            {
                id: 2,
                producto: {
                    id: 2,
                    descripcion: 'Mouse'
                },
                cantidad: 2,
                numerosSerie: [
                    '123456789',
                    '123456456'
                ],
                ordenCompra: {
                    id: 1,
                    puesto: 1,
                    numero: 1,
                    seleccionada: false
                  },
                cantidadOrdenCompra: 2
            },
            {
                id: 3,
                producto: {
                    id: 2,
                    descripcion: 'Teclado'
                },
                cantidad: 2,
                numerosSerie: undefined,
                ordenCompra: {
                    id: 1,
                    puesto: 1,
                    numero: 1,
                    seleccionada: false
                  },
                cantidadOrdenCompra: 2
            },
            {
                id: 4,
                producto: {
                    id: 1,
                    descripcion: 'Monitor'
                },
                cantidad: 1,
                numerosSerie: [
                    '123456789'
                ],
                ordenCompra: {
                    id: 1,
                    puesto: 1,
                    numero: 1,
                    seleccionada: false
                  },
                cantidadOrdenCompra: 1
            }
        ],
        fecha: '2018-02-01',
        observaciones: ''
    }
];