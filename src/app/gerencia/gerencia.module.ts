import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../shared/shared.module';
import { GerenciaRoutingModule } from './gerencia-routing.module';

@NgModule({
  declarations: [
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    GerenciaRoutingModule,
    SharedModule
  ],
  exports: [
  ]
})
export class GerenciaModule { }
