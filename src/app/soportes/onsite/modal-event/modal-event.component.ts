import { Component, Input } from '@angular/core';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

// stole from;
// https://ng-bootstrap.github.io/#/components/modal/examples
@Component({
  selector: 'app-modal-event',
  templateUrl: './modal-event.component.html',
  styleUrls: ['./modal-event.component.scss']
})
export class ModalEventComponent {
  @Input() date;

  vm: any;

  constructor(public activeModal: NgbActiveModal) {}
}
