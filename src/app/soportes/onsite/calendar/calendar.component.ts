import { Component } from '@angular/core';
import { NgbModal } from '@ng-bootstrap/ng-bootstrap';
import { CalendarEvent, CalendarMonthViewDay } from 'angular-calendar';

import { ModalEventComponent } from '../modal-event/modal-event.component';
import { colors } from './colors';

// const events = require('./events.json');

// https://github.com/mattlewis92/angular-calendar/blob/master/demos/demo-modules/month-view-badge-total/component.ts
@Component({
  selector: 'app-calendar',
  templateUrl: './calendar.component.html',
  styleUrls: ['./calendar.component.scss']
})
export class CalendarComponent {

  viewDate: Date = new Date();
  clickedDate: Date;

  constructor(private modalService: NgbModal) { }

  events: Array<CalendarEvent<{ incrementsBadgeTotal: boolean }>> = [
    {
      title: 'Increments badge total on the day cell',
      color: colors.yellow,
      start: new Date(),
      meta: {
        incrementsBadgeTotal: true
      }
    },
    {
      title: 'Does not increment the badge total on the day cell',
      color: colors.blue,
      start: new Date(),
      meta: {
        incrementsBadgeTotal: false
      }
    }
  ];

  beforeMonthViewRender({ body }: { body: Array<CalendarMonthViewDay> }): void {
    body.forEach(day => {
      day.badgeTotal = day.events.filter(
        event => event.meta.incrementsBadgeTotal
      ).length;
    });
  }

  dayClickModal = (date: Date) => {

    const modalRef = this.modalService.open(ModalEventComponent);
    modalRef.componentInstance.date = `World ${date.toString()}`;
    modalRef.componentInstance.vm = {
      action: 'add',
      event: this.events
    };
  }
}
