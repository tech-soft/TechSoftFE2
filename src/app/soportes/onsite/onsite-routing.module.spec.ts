import { OnsiteRoutingModule } from './onsite-routing.module';

describe('OnsiteRoutingModule', () => {
  let onsiteRoutingModule: OnsiteRoutingModule;

  beforeEach(() => {
    onsiteRoutingModule = new OnsiteRoutingModule();
  });

  it('should create an instance', () => {
    expect(onsiteRoutingModule).toBeTruthy();
  });
});
