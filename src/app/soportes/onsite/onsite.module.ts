import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'angular-calendar';

import { SharedModule } from '../../shared/shared.module';
import { CalendarComponent } from './calendar/calendar.component';
import { ModalEventComponent } from './modal-event/modal-event.component';
import { OnsiteRoutingModule } from './onsite-routing.module';

@NgModule({
  declarations: [
    CalendarComponent,
    ModalEventComponent
  ],
  entryComponents: [
    ModalEventComponent
  ],
  imports: [
    CalendarModule.forRoot(),
    CommonModule,
    FormsModule,
    NgbModule.forRoot(),
    OnsiteRoutingModule,
    SharedModule
  ],
  exports: [
    CalendarComponent
  ]
})
export class OnsiteModule { }
