import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from '../../shared/shared.module';
import { TallerRoutingModule } from './taller-routing.module';

@NgModule({
  declarations: [
  ],
  entryComponents: [
  ],
  imports: [
    CommonModule,
    FormsModule,
    TallerRoutingModule,
    SharedModule
  ],
  exports: [
  ]
})
export class TallerModule { }
