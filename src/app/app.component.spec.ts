import { APP_BASE_HREF } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { CalendarModule } from 'angular-calendar';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { LoginComponent } from './core/login/login.component';
import { MenuBarComponent } from './core/menu-bar/menu-bar.component';
import { NavComponent } from './core/nav/nav.component';
import { PageNotFoundComponent } from './core/page-not-found/page-not-found.component';
import { AuthenticationService } from './core/services/authentication.service';
import { ContactosComponent } from './personas/contactos/contactos.component';
import { DomiciliosComponent } from './personas/domicilios/domicilios.component';
import { PersonasComercialComponent } from './personas/personas-comercial/personas-comercial.component';
import { PersonasCrudComponent } from './personas/personas-crud/personas-crud.component';
import { PersonasRelacionadasComponent } from './personas/personas-relacionadas/personas-relacionadas.component';
import { PersonasSearchComponent } from './personas/personas-search/personas-search.component';
import { MessagesComponent } from './shared/messages/messages.component';
import { CalendarComponent } from './soportes/onsite/calendar/calendar.component';

library.add(fas, far);

describe('AppComponent', () => {
  let component: AppComponent;
  let fixture: ComponentFixture<AppComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        AppComponent,
        CalendarComponent,
        ContactosComponent,
        DomiciliosComponent,
        LoginComponent,
        MenuBarComponent,
        MessagesComponent,
        NavComponent,
        PageNotFoundComponent,
        PersonasComercialComponent,
        PersonasCrudComponent,
        PersonasRelacionadasComponent,
        PersonasSearchComponent
      ],
      imports: [
        AppRoutingModule,
        FontAwesomeModule,
        CalendarModule.forRoot(),
        FormsModule,
        HttpClientModule,
        NgbModule.forRoot()
      ],
      providers: [
        AuthenticationService,
        {
          provide: APP_BASE_HREF,
          useValue: '/'
        }
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AppComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('Se espera la creación de la aplicación', async(() => {

  //   const app = fixture.debugElement.componentInstance;

  //   console.log('\x1b[36m===================RESTARTED========================\x1b[0m', 'I am cyan in component spec');  // cyan
  //   console.log('\x1b[36m===================RESTARTED========================\x1b[0m', 'I am cyan in component spec');  // cyan
  //   console.log('\x1b[36m===================RESTARTED========================\x1b[0m', 'I am cyan in component spec');  // cyan

  //   expect(app)
  //     .toBeTruthy();
  // }));

  // it('el componente debería estar definido', () => {
  //   expect(component)
  //     .toBeDefined();
  // });

  // // it('onInit el estado de isLoggedIn debe coincidir con el del servicio de autentiación',
  // //   inject([AuthenticationService], (service: AuthenticationService) => {
  // //     component.ngOnInit();
  // //     expect(component.isLoggedIn)
  // // .toEqual(service.isLoggedIn);
  // //   }));

  // it('debe contener <app-nav>, <app-menu-bar>, <router-outlet>', () => {
  //   const appElement: HTMLElement = fixture.nativeElement;
  //   const appnav = appElement.querySelector('app-nav');
  //   const appmenubar = appElement.querySelector('app-menu-bar');
  //   const routeroutlet = appElement.querySelector('router-outlet');

  //   expect(appnav)
  //     .not
  //     .toBeNull('Se esperaba <app-nav>');
  //   expect(appmenubar)
  //     .not
  //     .toBeNull('Se esperaba <app-menu-bar>');
  //   expect(routeroutlet)
  //     .not
  //     .toBeNull('Se esperaba <router-outlet>');
  // });
});
