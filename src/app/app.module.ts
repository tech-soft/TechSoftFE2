import { registerLocaleData } from '@angular/common';
import { HTTP_INTERCEPTORS, HttpClientModule } from '@angular/common/http';
import localeEs from '@angular/common/locales/es';
import localeEsExtra from '@angular/common/locales/extra/es';
import { LOCALE_ID, NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { JwtModule } from '@auth0/angular-jwt';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { library } from '@fortawesome/fontawesome-svg-core';
import { far } from '@fortawesome/free-regular-svg-icons';
import { fas } from '@fortawesome/free-solid-svg-icons';

import { environment } from '../environments/environment';
import { AuthGuard } from './_guards/auth.guard';
import { HeaderInterceptor } from './_interceptors/header.interceptor';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ComprasModule } from './compras/compras.module';
import { CoreModule } from './core/core.module';
import { GerenciaModule } from './gerencia/gerencia.module';
import { PersonaModule } from './personas/persona.module';
import { SharedModule } from './shared/shared.module';
import { OnsiteModule } from './soportes/onsite/onsite.module';
import { TallerModule } from './soportes/taller/taller.module';
import { StockModule } from './stock/stock.module';
import { VentasModule } from './ventas/ventas.module';

export function tokenGetter(): string {
  if (localStorage && localStorage.getItem('access_token')) {
    return localStorage.getItem('access_token');
  }
}

library.add(fas, far);

registerLocaleData(localeEs, 'es-AR', localeEsExtra);

@NgModule({
  declarations: [
    AppComponent
  ],
  entryComponents: [
  ],
  imports: [
    BrowserModule,
    FormsModule,
    FontAwesomeModule,
    HttpClientModule,
    RouterModule,
    JwtModule.forRoot({
      config: {
        tokenGetter: tokenGetter,
        whitelistedDomains: [
          environment.HOST
        ],
        blacklistedRoutes: [
          'localhost:4200/login'
        ],
        throwNoTokenError: false,
      }
    }),
    GerenciaModule,
    OnsiteModule,
    SharedModule,
    StockModule,
    PersonaModule,
    ComprasModule,
    TallerModule,
    VentasModule,
    CoreModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: LOCALE_ID,
      useValue: 'es-AR'
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HeaderInterceptor,
      multi: true
    },
    AuthGuard
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule { }
