import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { DomiciliosService } from '../services/domicilios.service';
import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { DomiciliosComponent } from './domicilios.component';

describe('DomiciliosComponent', () => {
  let component: DomiciliosComponent;
  let fixture: ComponentFixture<DomiciliosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [DomiciliosComponent],
      imports: [HttpClientModule, NgbModule.forRoot(), RouterTestingModule],
      providers: [NgbActiveModal, NgbModal, HandleErrorService, HttpClient, MessageService, DomiciliosService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(DomiciliosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
