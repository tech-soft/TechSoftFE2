import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import * as md from '../../_models';
import { DomiciliosService } from '../services/domicilios.service';
import { ModalDomicilioComponent } from '../modal-domicilio/modal-domicilio.component';
import { Input } from '@angular/core';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-domicilios',
  templateUrl: './domicilios.component.html',
  styleUrls: ['./domicilios.component.scss']
})
export class DomiciliosComponent implements OnInit {

  @Input() persona: md.Persona = <md.Persona>{};

  domicilios: md.Domicilio[];
  modalOption: NgbModalOptions = {};

  hasDomicilios: boolean;

  constructor(
    private modalService: NgbModal,
    private domiciliosService: DomiciliosService,
    private messageService: MessageService
  ) {
    this.hasDomicilios = false;
  }

  ngOnInit(): void {
    this.getDomicilios();
  }

  eliminarDomicilio(id: number): void {
    // TODO: confirm msg
    this.domiciliosService.deleteDomicilio(id)
      .subscribe(
        result => {
          this.messageService.addSuccess("El domicilio se ha eliminado con éxito.");
          this.getDomicilios();
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  openModal(id: number): void {
    this.messageService.clear();
    
    this.modalOption.backdrop = 'static'; // no close modal on click
    this.modalOption.keyboard = false; // no close modal pressing ESC

    const modalRef = this.modalService.open(ModalDomicilioComponent, this.modalOption);

    modalRef.componentInstance.persona = this.persona;
    modalRef.componentInstance.hasDomicilio = id && (id !== 0);
    modalRef.componentInstance.id = id;

    const parent = this;

    const resultFn = () => {
      parent.getDomicilios();
    };

    modalRef.result.then(resultFn);
  }

  private getDomicilios(): void {
    this.domiciliosService.getDomicilios(this.persona.id)
      .subscribe(result => {
        if (result) {
          this.domicilios = result;
          this.hasDomicilios = this.domicilios != undefined && this.domicilios.length > 0;
        } else {
          this.domicilios = undefined;
          this.hasDomicilios = false;
        }
      },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
