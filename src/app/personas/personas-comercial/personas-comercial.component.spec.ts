import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { IvaTiposService } from '../services/iva-tipos.service';
import { PagoFormasService } from '../services/pago-formas.service';
import { PersonaComercialesService } from '../services/persona-comerciales.service';
import { PersonasService } from '../services/personas.service';
import { PersonasComercialComponent } from './personas-comercial.component';
import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { PrecioListasService } from '../../shared/services/precio-listas.service';

describe('PersonasComercialComponent', () => {
  let component: PersonasComercialComponent;
  let fixture: ComponentFixture<PersonasComercialComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonasComercialComponent],
      imports: [HttpClientModule, NgbModule.forRoot(), FormsModule, RouterTestingModule],
      providers: [NgbActiveModal, NgbModal, HttpClient, MessageService, HandleErrorService,
        IvaTiposService, PagoFormasService, PersonasService, PersonaComercialesService, PrecioListasService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasComercialComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
