import { Component, OnInit } from '@angular/core';

import * as md from '../../_models';
import { IvaTiposService } from '../services/iva-tipos.service';
import { PagoFormasService } from '../services/pago-formas.service';
import { PersonaComercialesService } from '../services/persona-comerciales.service';
import { PersonasService } from '../services/personas.service';
import { PrecioListasService } from '../../shared/services/precio-listas.service';
import { Input } from '@angular/core';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';
import { Router } from '@angular/router';
import { ConditionalExpr } from '@angular/compiler';

@Component({
  selector: 'app-personas-comercial',
  templateUrl: './personas-comercial.component.html',
  styleUrls: ['./personas-comercial.component.scss']
})
export class PersonasComercialComponent implements OnInit {

  @Input() persona: md.Persona = <md.Persona>{};

  ivaTipos: Array<md.IvaTipo>;
  pagoFormas: Array<md.PagoForma>;
  precioListas: Array<md.PrecioLista>;

  personaComercial: md.PersonaComercial = <md.PersonaComercial>{};

  readOnly: boolean;
  hasDatosComerciales: boolean;

  constructor(
    private router: Router,
    private ivaTiposService: IvaTiposService,
    private messageService: MessageService,
    private pagoFormasService: PagoFormasService,
    private personaComercialesService: PersonaComercialesService,
    private precioListasService: PrecioListasService
  ) {
    this.readOnly = true;
    this.hasDatosComerciales = false;
  }

  ngOnInit(): void {
    this.getDatosComerciales();
    this.getIvaTipos();
    this.getFormasPago();
    this.getListasPrecio();
  }

  getStatus(currentStatus: string) {
    switch (currentStatus) {
      case 'edit':
        this.edit();
        break;
      case 'delete':
        this.eliminarDatosComerciales();
        break;
      case 'save':
        this.guardarDatosComerciales();
        break;
      case 'cancel':
        this.cancel();
        break;
      default:
        break;
    }
  }

  validForm(showAlerts: boolean): boolean {

    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (this.personaComercial.descuento < 0 || this.personaComercial.descuento > 100) {
      if (showAlerts) {
        this.messageService.addError("El descuento debe estar entre 0 y 100.");
      }
      valido = false;
    }

    return valido;
  }

  // Init
  private getIvaTipos(): void {
    this.ivaTiposService.getIvaTipos()
      .subscribe(
        results => {
          this.ivaTipos = results;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getFormasPago(): void {
    this.pagoFormasService.getPagoFormas()
      .subscribe(
        results => {
          this.pagoFormas = results;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getListasPrecio(): void {
    this.precioListasService.getPrecioListas()
      .subscribe(
        results => {
          this.precioListas = results;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  // Datos Comerciales
  private getDatosComerciales(): void {
    if (this.persona && this.persona.id > 0) {
      this.personaComercialesService.getDatosComerciales(this.persona.id)
        .subscribe(result => {
          if (result) {
            this.personaComercial = result;
            this.hasDatosComerciales = this.personaComercial != undefined && this.personaComercial.id != undefined;
            this.personaComercial.personaId = this.persona.id;
          } else {
            this.personaComercial = <md.PersonaComercial>{};
            this.hasDatosComerciales = false;
            this.personaComercial.personaId = this.persona.id;

          }
        },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private guardarDatosComerciales(): void {
    if (this.validForm(true)) {
      this.personaComercialesService.addDatosComerciales(this.personaComercial)
        .subscribe(
          result => {
            if (result && result.id) {

              if (this.hasDatosComerciales) {
                this.messageService.addSuccess('Se han actualizado los datos comerciales con éxito');
              } else {
                this.messageService.addSuccess('Se han generado los datos comerciales con éxito');
              }

              this.getDatosComerciales();

              this.readOnly = true;
              this.hasDatosComerciales = true;

            } else {
              this.messageService.addError('No se ha podido recuperar el código con que se generó la persona. Verifique que la misma se haya generado correctamente.');
            }
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private eliminarDatosComerciales(): void {
    // TODO: confirm msg
    if (this.persona && this.persona.id) {
      this.personaComercialesService.deleteDatosComerciales(this.persona.id)
        .subscribe(
          result => {
            this.messageService.addSuccess("Se ha eliminado con éxito.");
            this.router.navigate([`/personas/crud/${this.persona.id}`]);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  // Form
  private edit(): void {
    this.readOnly = false;
    this.hasDatosComerciales = true;
  }

  private cancel(): void {
    this.getDatosComerciales();
    this.readOnly = true;
  }

  compareFn(p1: any, p2: any): boolean {

    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }

  trackByFn(index, item): number {
    return index;
  }
}
