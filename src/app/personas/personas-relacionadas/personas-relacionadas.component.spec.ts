import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { PersonasRelacionadasService } from '../services/personas-relacionadas.service';
import { PersonasRelacionadasComponent } from './personas-relacionadas.component';

describe('PersonasRelacionadasComponent', () => {
  let component: PersonasRelacionadasComponent;
  let fixture: ComponentFixture<PersonasRelacionadasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [PersonasRelacionadasComponent],
      imports: [HttpClientModule, NgbModule.forRoot(), RouterTestingModule],
      providers: [NgbActiveModal, NgbModal, HttpClient, HandleErrorService, MessageService, PersonasRelacionadasService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PersonasRelacionadasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
