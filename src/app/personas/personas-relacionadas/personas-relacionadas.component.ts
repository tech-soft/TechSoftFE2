import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import * as md from '../../_models';
import { PersonasRelacionadasService } from '../services/personas-relacionadas.service';
import { ModalPersonaRelacionadaComponent } from '../modal-persona-relacionada/modal-persona-relacionada.component';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-personas-relacionadas',
  templateUrl: './personas-relacionadas.component.html',
  styleUrls: ['./personas-relacionadas.component.scss']
})
export class PersonasRelacionadasComponent implements OnInit {

  @Input() persona: md.Persona = <md.Persona>{};

  personasRelacionadas: md.PersonaRelacionada[];
  modalOption: NgbModalOptions = {};

  hasPersonasRelacionadas: boolean;

  constructor(
    private modalService: NgbModal,
    private personasRelacionadasService: PersonasRelacionadasService,
    private messageService: MessageService
  ) {
    this.hasPersonasRelacionadas = false;
  }

  ngOnInit(): void {
    this.getPersonasRelacionadas();
  }

  eliminarPersonaRelacionada(id: number): void {
    // TODO: confirm msg
    this.personasRelacionadasService.deletePersonaRelacionada(id)
      .subscribe(
        result => {
          this.messageService.addSuccess("La persona relacionada se ha eliminado con éxito.");
          this.getPersonasRelacionadas();
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  openModal = (id: number) => {
    this.messageService.clear();

    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;

    const modalRef = this.modalService.open(ModalPersonaRelacionadaComponent, this.modalOption);

    modalRef.componentInstance.persona = this.persona;
    modalRef.componentInstance.hasPersonaRelacionada = !!(id && (id != 0));
    modalRef.componentInstance.id = id;

    const parent = this;

    const resultFn = () => {
      parent.getPersonasRelacionadas();
    };

    modalRef.result.then(resultFn);
  }

  private getPersonasRelacionadas(): void {
    this.personasRelacionadasService.getPersonasRelacionadas(this.persona.id)
      .subscribe(result => {
        if (result) {
          this.personasRelacionadas = result;
          this.hasPersonasRelacionadas = this.personasRelacionadas !== undefined && this.personasRelacionadas.length > 0;
        } else {
          this.personasRelacionadas = undefined;
          this.hasPersonasRelacionadas = false;
        }
      },
      error => {
        this.messageService.addError(`${error.message}`);
        return throwError(error);
      }
    );
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
