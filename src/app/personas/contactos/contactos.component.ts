import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbModal, NgbModalOptions } from '@ng-bootstrap/ng-bootstrap';

import * as md from '../../_models';
import { ModalContactoComponent } from '../modal-contacto/modal-contacto.component';
import { ContactosService } from '../services/contactos.service';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-contactos',
  templateUrl: './contactos.component.html',
  styleUrls: ['./contactos.component.scss']
})
export class ContactosComponent implements OnInit {

  @Input() tipo;
  @Input() persona: md.Persona = <md.Persona>{};

  contactos: md.Contacto[];
  modalOption: NgbModalOptions = {};

  hasContactos: boolean;

  constructor(
    private modalService: NgbModal,
    private contactosService: ContactosService,
    private messageService: MessageService
  ) {
    this.hasContactos = false;
  }

  ngOnInit(): void {
    this.getContactos();
  }

  eliminarContacto(id: number): void {
    // TODO: confirm msg
    this.contactosService.deleteContacto(id)
      .subscribe(
        result => {
          this.messageService.addSuccess("El contacto se ha eliminado con éxito.");
          this.getContactos();
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  openModal = (id: number) => {
    this.messageService.clear();
    
    this.modalOption.backdrop = 'static';
    this.modalOption.keyboard = false;

    const modalRef = this.modalService.open(ModalContactoComponent, this.modalOption);

    modalRef.componentInstance.persona = this.persona;
    modalRef.componentInstance.hasContacto = id &&  (id !== 0);
    modalRef.componentInstance.id = id;
    modalRef.componentInstance.tipo = this.tipo;

    const parent = this;

    const resultFn = () => {
      parent.getContactos();
    };

    modalRef.result.then(resultFn);
  }

  private getContactos(): void {
    this.contactosService.getContactos(this.persona.id, this.tipo)
      .subscribe(result => {
        if (result) {
          this.contactos = result;
          this.hasContactos = this.contactos != undefined && this.contactos.length > 0;
        } else {
          this.contactos = undefined;
          this.hasContactos = false;
        }
      },
      error => {
        this.messageService.addError(`${error.message}`);
        return throwError(error);
      }
    );
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
