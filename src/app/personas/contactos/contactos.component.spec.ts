import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { ContactosService } from '../services/contactos.service';
import { ContactosComponent } from './contactos.component';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';

describe('ContactosComponent', () => {
  let component: ContactosComponent;
  let fixture: ComponentFixture<ContactosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({

      declarations: [ContactosComponent],
      imports: [
        HttpClientModule, 
        FontAwesomeModule, 
        NgbModule.forRoot(), 
        RouterTestingModule
      ],
      providers: [NgbActiveModal,
        HandleErrorService, NgbModal, HttpClient, MessageService, ContactosService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ContactosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component)
  //     .toBeTruthy();
  // });
});
