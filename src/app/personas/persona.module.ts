import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { SharedModule } from '../shared/shared.module';
import { ContactosComponent } from './contactos/contactos.component';
import { DomiciliosComponent } from './domicilios/domicilios.component';
import { ModalContactoComponent } from './modal-contacto/modal-contacto.component';
import { ModalDomicilioComponent } from './modal-domicilio/modal-domicilio.component';
import { ModalPersonaRelacionadaComponent } from './modal-persona-relacionada/modal-persona-relacionada.component';
import { PersonaRoutingModule } from './persona-routing.module';
import { PersonasComercialComponent } from './personas-comercial/personas-comercial.component';
import { PersonasCrudComponent } from './personas-crud/personas-crud.component';
import { PersonasRelacionadasComponent } from './personas-relacionadas/personas-relacionadas.component';
import { PersonasSearchComponent } from './personas-search/personas-search.component';
import { CiudadesService } from './services/ciudades.service';
import { ContactosService } from './services/contactos.service';
import { DocumentoTiposService } from './services/documento-tipos.service';
import { DomiciliosService } from './services/domicilios.service';
import { IvaTiposService } from './services/iva-tipos.service';
import { PagoFormasService } from './services/pago-formas.service';
import { PaisesService } from './services/paises.service';
import { PersonaComercialesService } from './services/persona-comerciales.service';
import { PersonaRelacionTiposService } from './services/persona-relacion-tipos.service';
import { PersonasRelacionadasService } from './services/personas-relacionadas.service';
import { PersonasService } from './services/personas.service';
import { ProvinciasService } from './services/provincias.service';

@NgModule({
  declarations: [
    ContactosComponent,
    DomiciliosComponent,
    ModalContactoComponent,
    ModalDomicilioComponent,
    ModalPersonaRelacionadaComponent,
    PersonasComercialComponent,
    PersonasCrudComponent,
    PersonasRelacionadasComponent,
    PersonasSearchComponent
  ],
  entryComponents: [
    ModalContactoComponent,
    ModalDomicilioComponent,
    ModalPersonaRelacionadaComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    FontAwesomeModule,
    NgbModule.forRoot(),
    PersonaRoutingModule,
    SharedModule
  ],
  exports: [
    ContactosComponent,
    DomiciliosComponent,
    PersonasComercialComponent,
    PersonasCrudComponent,
    PersonasRelacionadasComponent,
    PersonasSearchComponent
  ],
  providers: [
    CiudadesService,
    ContactosService,
    DocumentoTiposService,
    DomiciliosService,
    IvaTiposService,
    PagoFormasService,
    PaisesService,
    PersonaComercialesService,
    PersonaRelacionTiposService,
    PersonasRelacionadasService,
    PersonasService,
    ProvinciasService,
    PaisesService
  ]
})
export class PersonaModule { }
