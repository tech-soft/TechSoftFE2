import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { PAISES } from '../../_mocks/paises.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { PaisesService } from './paises.service';

describe('PaisesService', () => {
  const serviceUrlBase = environment.APIS.PAIS.BASE;

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PaisesService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([PaisesService], (service: PaisesService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/paises',
//     async(
//       inject([PaisesService, HttpTestingController],
//         (service: PaisesService, backend: HttpTestingController) => {

//           service.getPaises()
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/paises',
//     async(
//       inject([PaisesService, HttpTestingController],
//         (service: PaisesService, backend: HttpTestingController) => {

//           service.addPais(PAISES[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/paises',
//     async(
//       inject([PaisesService, HttpTestingController],
//         (service: PaisesService, backend: HttpTestingController) => {

//           service.updatePais(PAISES[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/paises',
//     async(
//       inject([PaisesService, HttpTestingController],
//         (service: PaisesService, backend: HttpTestingController) => {

//           service.deletePais(PAISES[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + PAISES[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + PAISES[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos los paises',
//     async(
//       inject([PaisesService],
//         (service: PaisesService) => {

//           service.getPaises()
// .subscribe(paises => {
//             expect(paises.length)
// .toBe(2, 'Se esperan 2 elementos');
//             expect(paises)
// .toEqual(PAISES, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(PAISES);
//         })));

});
