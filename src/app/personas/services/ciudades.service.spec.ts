import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { CIUDADES } from '../../_mocks/ciudades.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { CiudadesService } from './ciudades.service';

describe('CiudadesService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        CiudadesService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([CiudadesService], (service: CiudadesService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/ciudades',
  //   async(
  //     inject([CiudadesService, HttpTestingController],
  //       (service: CiudadesService, backend: HttpTestingController) => {

  //         service.getCiudades()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método GET sobre url ../techsoftapp/ciudades/byprovinciaid?id=',
  //   async(
  //     inject([CiudadesService, HttpTestingController],
  //       (service: CiudadesService, backend: HttpTestingController) => {

  //         const provinciaId = CIUDADES[0].provincia.id;

  //         service.getCiudadesByProvincia(provinciaId)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlByProvincia + provinciaId);

  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlByProvincia + provinciaId);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/ciudades',
  //   async(
  //     inject([CiudadesService, HttpTestingController],
  //       (service: CiudadesService, backend: HttpTestingController) => {

  //         service.addCiudad(CIUDADES[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/ciudades',
  //   async(
  //     inject([CiudadesService, HttpTestingController],
  //       (service: CiudadesService, backend: HttpTestingController) => {

  //         service.updateCiudad(CIUDADES[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/ciudades',
  //   async(
  //     inject([CiudadesService, HttpTestingController],
  //       (service: CiudadesService, backend: HttpTestingController) => {

  //         service.deleteCiudad(CIUDADES[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + CIUDADES[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + CIUDADES[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todas las ciudades',
  //   async(
  //     inject([CiudadesService],
  //       (service: CiudadesService) => {

  //         service.getCiudades()
  //           .subscribe(ciudades => {
  //             expect(ciudades.length)
  //               .toBe(4, 'Se esperan 4 elementos');
  //             expect(ciudades)
  //               .toEqual(CIUDADES, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(CIUDADES);
  //       })
  //   )
  // );

});
