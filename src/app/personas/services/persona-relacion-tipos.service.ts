import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class PersonaRelacionTiposService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getPersonaRelacionTipos(): Observable<Array<md.PersonaRelacionTipo>> {

    return this.httpClient
      .get<Array<md.PersonaRelacionTipo>>(
        environment.APIS.RELACION_TIPO.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getPersonaRelacionTipos', []))
      );
  }

  addPersonaRelacionTipo(personaRelacionTipo: md.PersonaRelacionTipo): Observable<md.PersonaRelacionTipo> {

    return this.httpClient
      .post<md.PersonaRelacionTipo>(
        environment.APIS.RELACION_TIPO.BASE,
        personaRelacionTipo,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PersonaRelacionTipo>('addPersonaRelacionTipo'))
      );
  }

  updatePersonaRelacionTipo(personaRelacionTipo: md.PersonaRelacionTipo): Observable<md.PersonaRelacionTipo> {

    return this.httpClient
      .put<md.PersonaRelacionTipo>(
        environment.APIS.RELACION_TIPO.BASE,
        personaRelacionTipo,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PersonaRelacionTipo>('updatePersonaRelacionTipo'))
      );
  }

  deletePersonaRelacionTipo(id: number): Observable<md.PersonaRelacionTipo> {

    return this.httpClient
      .delete<md.PersonaRelacionTipo>(
        environment.APIS.RELACION_TIPO.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PersonaRelacionTipo>('deletePersonaRelacionTipo'))
      );
  }
}
