import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { PERSONA_RELACION_TIPO } from '../../_mocks/persona-relacion-tipos.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { PersonaRelacionTiposService } from './persona-relacion-tipos.service';

describe('PersonaRelacionTiposService', () => {

  const serviceUrlBase = environment.APIS.RELACION_TIPO.BASE;

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PersonaRelacionTiposService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([PersonaRelacionTiposService], (service: PersonaRelacionTiposService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/persona-relacion-tipos',
  //   async(
  //     inject([PersonaRelacionTiposService, HttpTestingController],
  //       (service: PersonaRelacionTiposService, backend: HttpTestingController) => {

  //         service.getPersonaRelacionTipos()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/persona-relacion-tipos',
  //   async(
  //     inject([PersonaRelacionTiposService, HttpTestingController],
  //       (service: PersonaRelacionTiposService, backend: HttpTestingController) => {

  //         service.addPersonaRelacionTipo(PERSONA_RELACION_TIPO[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/persona-relacion-tipos',
  //   async(
  //     inject([PersonaRelacionTiposService, HttpTestingController],
  //       (service: PersonaRelacionTiposService, backend: HttpTestingController) => {

  //         service.updatePersonaRelacionTipo(PERSONA_RELACION_TIPO[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/persona-relacion-tipos',
  //   async(
  //     inject([PersonaRelacionTiposService, HttpTestingController],
  //       (service: PersonaRelacionTiposService, backend: HttpTestingController) => {

  //         service.deletePersonaRelacionTipo(PERSONA_RELACION_TIPO[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + PERSONA_RELACION_TIPO[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + PERSONA_RELACION_TIPO[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos las formas de pago',
  //   async(
  //     inject([PersonaRelacionTiposService],
  //       (service: PersonaRelacionTiposService) => {

  //         service.getPersonaRelacionTipos()
  //           .subscribe(formaPagos => {
  //             expect(formaPagos.length)
  //               .toBe(1, 'Se espera 1 elemento');
  //             expect(formaPagos)
  //               .toEqual(PERSONA_RELACION_TIPO, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(PERSONA_RELACION_TIPO);
  //       })));
});
