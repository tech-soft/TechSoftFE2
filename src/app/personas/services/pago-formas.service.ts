import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class PagoFormasService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getPagoFormas(): Observable<Array<md.PagoForma>> {

    return this.httpClient
      .get<Array<md.PagoForma>>(
        environment.APIS.PAGO_FORMA.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getPagoFormas', []))
      );
  }

  addPagoForma(pagoForma: md.PagoForma): Observable<md.PagoForma> {

    return this.httpClient
      .post<md.PagoForma>(
        environment.APIS.PAGO_FORMA.BASE,
        pagoForma,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PagoForma>('addPagoForma'))
      );
  }

  updatePagoForma(pagoForma: md.PagoForma): Observable<md.PagoForma> {

    return this.httpClient
      .put<md.PagoForma>(
        environment.APIS.PAGO_FORMA.BASE,
        pagoForma,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PagoForma>('updatePagoForma'))
      );
  }

  deletePagoForma(id: number): Observable<md.PagoForma> {

    return this.httpClient
      .delete<md.PagoForma>(
        environment.APIS.PAGO_FORMA.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.PagoForma>('deletePagoForma'))
      );
  }

}
