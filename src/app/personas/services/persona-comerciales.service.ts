import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable()
export class PersonaComercialesService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getDatosComerciales(personaId: number): Observable<md.PersonaComercial> {

    return this.httpClient.get<md.PersonaComercial>(
        environment.APIS.PERSONA.BASE + `/${personaId}/datos-comerciales`
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addDatosComerciales(personaComercial: md.PersonaComercial): Observable<md.PersonaComercial> {

    return this.httpClient.post<md.PersonaComercial>(
        environment.APIS.PERSONA.DATOS_COMERCIALES.BASE,
        personaComercial
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deleteDatosComerciales(personaId: number): Observable<md.PersonaComercial> {

    return this.httpClient.delete<md.PersonaComercial>(
        environment.APIS.PERSONA.DATOS_COMERCIALES.BASE + `/${personaId}`
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
