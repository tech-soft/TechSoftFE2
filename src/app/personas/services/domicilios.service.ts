import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

@Injectable()
export class DomiciliosService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getDomicilio(id: number): Observable<md.Domicilio> {

    return this.httpClient
      .get<md.Domicilio>(
        environment.APIS.PERSONA.DOMICILIO.BASE + `/${id}`
      )
      .pipe(
        tap(
          domicilio => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  getDomicilios(personaId: number): Observable<md.Domicilio[]> {

    return this.httpClient
      .get<md.Domicilio[]>(
        environment.APIS.PERSONA.BASE + `/${personaId}/domicilios`
      )
      .pipe(
        tap(
          domicilios => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addDomicilio(domicilio: md.Domicilio): Observable<md.Domicilio> {

    return this.httpClient
      .post<md.Domicilio>(
        environment.APIS.PERSONA.DOMICILIO.BASE,
        domicilio
      )
      .pipe(
        tap(
          domicilio => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deleteDomicilio(id: number): Observable<md.Domicilio> {

    return this.httpClient
      .delete<md.Domicilio>(
        environment.APIS.PERSONA.DOMICILIO.BASE + `/${id}`
      )
      .pipe(
        tap(
          domicilio => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
