import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable()
export class ContactosService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getContacto(id: number): Observable<md.Contacto> {

    return this.httpClient
      .get<md.Contacto>(
        environment.APIS.PERSONA.CONTACTO.BASE + `/${id}`
      )
      .pipe(
        tap(
          contacto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  getContactos(personaId: number, tipoId: number): Observable<md.Contacto[]> {

    return this.httpClient
      .get<md.Contacto[]>(
        environment.APIS.PERSONA.BASE + `/${personaId}/contactos/${tipoId}`
      )
      .pipe(
        tap(
          contactos => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addContacto(contacto: md.Contacto): Observable<md.Contacto> {

    return this.httpClient
      .post<md.Contacto>(
        environment.APIS.PERSONA.CONTACTO.BASE,
        contacto
      )
      .pipe(
        tap(
          contacto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deleteContacto(id: number): Observable<md.Contacto> {

    return this.httpClient
      .delete<md.Contacto>(
        environment.APIS.PERSONA.CONTACTO.BASE + `/${id}`
      )
      .pipe(
        tap(
          contacto => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
