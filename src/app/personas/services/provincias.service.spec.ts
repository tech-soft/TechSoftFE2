import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { PROVINCIAS } from '../../_mocks/provincias.mock';
import { ProvinciasService } from './provincias.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

describe('ProvinciasService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ProvinciasService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([ProvinciasService], (service: ProvinciasService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/provincias',
  //   async(
  //     inject([ProvinciasService, HttpTestingController],
  //       (service: ProvinciasService, backend: HttpTestingController) => {

  //         service.getProvincias()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método GET sobre url ../techsoftapp/provincias/bypaisid?id=',
  //   async(
  //     inject([ProvinciasService, HttpTestingController],
  //       (service: ProvinciasService, backend: HttpTestingController) => {

  //         service.getProvinciasByPais(PROVINCIAS[0].pais.id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlByPais + PROVINCIAS[0].pais.id);

  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlByPais + PROVINCIAS[0].pais.id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/provincias',
  //   async(
  //     inject([ProvinciasService, HttpTestingController],
  //       (service: ProvinciasService, backend: HttpTestingController) => {

  //         service.addProvincia(PROVINCIAS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/provincias',
  //   async(
  //     inject([ProvinciasService, HttpTestingController],
  //       (service: ProvinciasService, backend: HttpTestingController) => {

  //         service.updateProvincia(PROVINCIAS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/provincias',
  //   async(
  //     inject([ProvinciasService, HttpTestingController],
  //       (service: ProvinciasService, backend: HttpTestingController) => {

  //         service.deleteProvincia(PROVINCIAS[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + PROVINCIAS[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + PROVINCIAS[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos las provincias',
  //   async(
  //     inject([ProvinciasService],
  //       (service: ProvinciasService) => {

  //         service.getProvincias()
  //           .subscribe(provincias => {
  //             expect(provincias.length)
  //               .toBe(2, 'Se esperan 2 elementos');
  //             expect(provincias)
  //               .toEqual(PROVINCIAS, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(PROVINCIAS);
  //       })
  //   )
  // );
});
