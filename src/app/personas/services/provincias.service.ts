import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class ProvinciasService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getProvincias(): Observable<Array<md.Provincia>> {

    return this.httpClient
      .get<Array<md.Provincia>>(
        environment.APIS.PROVINCIA.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getProvincias', []))
      );
  }

  // getProvinciasByPais(idPais: number): Observable<Array<md.Provincia>> {

  //   return this.httpClient
  //     .get<Array<md.Provincia>>(
  //       this.serviceUrlByPais + idPais,
  //       { headers }
  //     )
  //     .pipe(
  //       tap(_ => { }),
  //       catchError(this.handleErrorService.handleError(`getProvinciasByPais id=${idPais}`, []))
  //     );
  // }

  addProvincia(provincia: md.Provincia): Observable<md.Provincia> {

    return this.httpClient
      .post<md.Provincia>(
        environment.APIS.PROVINCIA.BASE,
        provincia,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Provincia>('addProvincia'))
      );
  }

  updateProvincia(provincia: md.Provincia): Observable<md.Provincia> {

    return this.httpClient
      .put<md.Provincia>(
        environment.APIS.PROVINCIA.BASE,
        provincia,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Provincia>('updateProvincia'))
      );
  }

  deleteProvincia(id: number): Observable<md.Provincia> {

    return this.httpClient
      .delete<md.Provincia>(
        environment.APIS.PROVINCIA.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Provincia>('deleteProvincia'))
      );
  }

}
