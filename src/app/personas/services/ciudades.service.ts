import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class CiudadesService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getCiudades(): Observable<Array<md.Ciudad>> {

    return this.httpClient
      .get<Array<md.Ciudad>>(
        environment.APIS.CIUDAD.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<Array<md.Ciudad>>('getCiudades', []))
      );
  }

  getCiudadesByProvincia(proviciaId: number): Observable<Array<md.Ciudad>> {

    return this.httpClient
      .get<Array<md.Ciudad>>(
        environment.APIS.PROVINCIA + `/${proviciaId}/ciudades`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<Array<md.Ciudad>>(`getCiudadesByProvincia id=${proviciaId}`, []))
      );
  }

  addCiudad(ciudad: md.Ciudad): Observable<md.Ciudad> {

    return this.httpClient
      .post<md.Ciudad>(
        environment.APIS.CIUDAD.BASE,
        ciudad,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Ciudad>('addCiudad'))
      );
  }

  updateCiudad(ciudad: md.Ciudad): Observable<md.Ciudad> {

    return this.httpClient
      .put<md.Ciudad>(
        environment.APIS.CIUDAD.BASE,
        ciudad,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Ciudad>('updateCiudad'))
      );
  }

  deleteCiudad(id: number): Observable<md.Ciudad> {

    return this.httpClient
      .delete<md.Ciudad>(
        environment.APIS.CIUDAD.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Ciudad>('deleteCiudad'))
      );
  }

}
