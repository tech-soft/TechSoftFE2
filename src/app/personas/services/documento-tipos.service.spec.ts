import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { DocumentoTiposService } from './documento-tipos.service';
import { DOCUMENTO_TIPOS } from '../../_mocks/documento-tipos.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

describe('DocumentoTiposService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        DocumentoTiposService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([DocumentoTiposService], (service: DocumentoTiposService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/documento-tipos',
//     async(
//       inject([DocumentoTiposService, HttpTestingController],
//         (service: DocumentoTiposService, backend: HttpTestingController) => {

//           service.getDocumentoTipos()
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/documento-tipos',
//     async(
//       inject([DocumentoTiposService, HttpTestingController],
//         (service: DocumentoTiposService, backend: HttpTestingController) => {

//           service.addDocumentoTipo(DOCUMENTO_TIPOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/documento-tipos',
//     async(
//       inject([DocumentoTiposService, HttpTestingController],
//         (service: DocumentoTiposService, backend: HttpTestingController) => {

//           service.updateDocumentoTipo(DOCUMENTO_TIPOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/documento-tipos',
//     async(
//       inject([DocumentoTiposService, HttpTestingController],
//         (service: DocumentoTiposService, backend: HttpTestingController) => {

//           service.deleteDocumentoTipo(DOCUMENTO_TIPOS[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + DOCUMENTO_TIPOS[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + DOCUMENTO_TIPOS[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todas los tipos de documento',
//     async(
//       inject([DocumentoTiposService],
//         (service: DocumentoTiposService) => {

//           service.getDocumentoTipos()
// .subscribe(documentoTipos => {
//             expect(documentoTipos.length)
// .toBe(2, 'Se esperan 2 elementos');
//             expect(documentoTipos)
// .toEqual(DOCUMENTO_TIPOS, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(DOCUMENTO_TIPOS);
//         })
//     )
//   );

});
