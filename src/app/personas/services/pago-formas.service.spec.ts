import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { PagoFormasService } from './pago-formas.service';
import { PAGO_FORMA } from '../../_mocks/pago-formas.mock';

describe('PagoFormasService', () => {
  const serviceUrlBase = environment.APIS.PAGO_FORMA.BASE;

  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PagoFormasService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([PagoFormasService], (service: PagoFormasService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/pago-formas',
//     async(
//       inject([PagoFormasService, HttpTestingController],
//         (service: PagoFormasService, backend: HttpTestingController) => {

//           service.getPagoFormas()
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/pago-formas',
//     async(
//       inject([PagoFormasService, HttpTestingController],
//         (service: PagoFormasService, backend: HttpTestingController) => {

//           service.addPagoForma(PAGO_FORMA[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/pago-formas',
//     async(
//       inject([PagoFormasService, HttpTestingController],
//         (service: PagoFormasService, backend: HttpTestingController) => {

//           service.updatePagoForma(PAGO_FORMA[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/pago-formas',
//     async(
//       inject([PagoFormasService, HttpTestingController],
//         (service: PagoFormasService, backend: HttpTestingController) => {

//           service.deletePagoForma(PAGO_FORMA[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + PAGO_FORMA[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + PAGO_FORMA[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos las formas de pago',
//     async(
//       inject([PagoFormasService],
//         (service: PagoFormasService) => {

//           service.getPagoFormas()
// .subscribe(formaPagos => {
//             expect(formaPagos.length)
// .toBe(2, 'Se esperan 2 elementos');
//             expect(formaPagos)
// .toEqual(PAGO_FORMA, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(PAGO_FORMA);
//         })));

});
