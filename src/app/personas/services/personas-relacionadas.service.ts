import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

@Injectable()
export class PersonasRelacionadasService {

  constructor(
    private httpClient: HttpClient
  ) { }

  getPersonaRelacionada(id: number): Observable<md.PersonaRelacionada> {

    return this.httpClient
      .get<md.PersonaRelacionada>(
        environment.APIS.PERSONA.RELACION.BASE + `/${id}`
      )
      .pipe(
        tap(
          personaRelacionada => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  getPersonasRelacionadas(personaId: number): Observable<md.PersonaRelacionada[]> {

    return this.httpClient
      .get<md.PersonaRelacionada[]>(
        environment.APIS.PERSONA.BASE + `/${personaId}/relacionadas`
      )
      .pipe(
        tap(
          personasRelacionadas => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addPersonaRelacionada(personaRelacionada: md.PersonaRelacionada): Observable<md.PersonaRelacionada> {
    return this.httpClient
      .post<md.PersonaRelacionada>(
        environment.APIS.PERSONA.RELACION.BASE,
        personaRelacionada
      )
      .pipe(
        tap(
          personaRelacionada => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deletePersonaRelacionada(id: number): Observable<md.PersonaRelacionada> {

    return this.httpClient
      .delete<md.PersonaRelacionada>(
        environment.APIS.PERSONA.RELACION.BASE + `/${id}`
      )
      .pipe(
        tap(
          personaRelacionada => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
