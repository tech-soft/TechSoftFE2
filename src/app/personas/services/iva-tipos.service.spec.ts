import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { IVA_TIPOS } from '../../_mocks/iva-tipos.mock';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { IvaTiposService } from './iva-tipos.service';

describe('IvaTiposService', () => {

  let injector: TestBed;
  let httpMock: HttpTestingController;
  beforeEach(() => {

    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        IvaTiposService,
        HandleErrorService,
        MessageService
      ]
    });
    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  afterEach(() => {
    httpMock.verify();
  });

  // it('Se espera que el servicio esté creado', inject([IvaTiposService], (service: IvaTiposService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/iva-tipos',
  //   async(
  //     inject([IvaTiposService, HttpTestingController],
  //       (service: IvaTiposService, backend: HttpTestingController) => {

  //         service.getIvaTipos()
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/iva-tipos',
  //   async(
  //     inject([IvaTiposService, HttpTestingController],
  //       (service: IvaTiposService, backend: HttpTestingController) => {

  //         service.addIvaTipo(IVA_TIPOS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/iva-tipos',
  //   async(
  //     inject([IvaTiposService, HttpTestingController],
  //       (service: IvaTiposService, backend: HttpTestingController) => {

  //         service.updateIvaTipo(IVA_TIPOS[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/iva-tipos',
  //   async(
  //     inject([IvaTiposService, HttpTestingController],
  //       (service: IvaTiposService, backend: HttpTestingController) => {

  //         service.deleteIvaTipo(IVA_TIPOS[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + IVA_TIPOS[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + IVA_TIPOS[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos los tipos de iva',
  //   async(
  //     inject([IvaTiposService],
  //       (service: IvaTiposService) => {

  //         service.getIvaTipos()
  //           .subscribe(ivaTipos => {
  //             expect(ivaTipos.length)
  //               .toBe(3, 'Se esperan 3 elementos');
  //             expect(ivaTipos)
  //               .toEqual(IVA_TIPOS, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(IVA_TIPOS);
  //       })));
});
