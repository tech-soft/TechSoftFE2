import { HttpClient, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of, throwError } from 'rxjs';
import { catchError, debounceTime, distinctUntilChanged, switchMap, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';

@Injectable({
  providedIn: 'root'
})
export class PersonasService {

  constructor(
    private httpClient: HttpClient
  ) { }

  public getPersona(id: number): Observable<md.Persona> {
    return this.httpClient.get<md.Persona>(
        environment.APIS.PERSONA.BASE + `/${id}`
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public getPersonas(): Observable<md.Persona[]> {
    return this.httpClient.get<md.Persona[]>(
        environment.APIS.PERSONA.BASE
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public searchPersonas(terms: Observable<String>): Observable<any> {
    return terms.pipe(
      debounceTime(400),
      distinctUntilChanged(),
      switchMap((term: String) => this.searchTerms(term))
    );
  }

  public searchTerms(term: String): Observable<any> {
    if (!term.trim()) {
      return of([]);
    }

    let httpParams = new HttpParams();

    httpParams = httpParams.set('q', term.toString());

    return this.httpClient.get<md.Persona[]>(
        environment.APIS.PERSONA.BY_KEYWORD,
        { params: httpParams }
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  public postPersona(persona: md.Persona): Observable<md.Persona> {
    return this.httpClient.post<md.Persona>(
      environment.APIS.PERSONA.BASE,
      persona
    )
    .pipe(
      tap(
        persona => { }
      ),
      catchError(
        (err: any, caught: Observable<any>) => {
          if (err && err.status === 0) {
            err.error.message = 'Error! verifique la conexion con el servidor';
          }
          return throwError(err.error)
        }
      ));
  }

  public putPersona(persona: md.Persona): Observable<md.Persona> {
    return this.httpClient.put<md.Persona>(
      environment.APIS.PERSONA.BASE,
      persona
    )
    .pipe(
      tap(
        persona => { }
      ),
      catchError(
        (err: any, caught: Observable<any>) => {
          if (err && err.status === 0) {
            err.error.message = 'Error! verifique la conexion con el servidor';
          }
          return throwError(err.error)
        }
      ));
  }

  public deletePersona(id: number): Observable<md.Persona> {
    return this.httpClient.delete<md.Persona>(
        environment.APIS.PERSONA.BASE + `/${id}`
      )
      .pipe(
        tap(
          persona => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }
}
