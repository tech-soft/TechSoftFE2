import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';
import { environment } from '../../../environments/environment';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import * as md from '../../_models';


@Injectable()
export class DocumentoTiposService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getDocumentoTipos(): Observable<md.DocumentoTipo[]> {

    return this.httpClient
      .get<Array<md.DocumentoTipo>>(
        environment.APIS.DOCUMENTO_TIPO.BASE
      )
      .pipe(
        tap(
          documentoTipo => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  addDocumentoTipo(documentoTipo: md.DocumentoTipo): Observable<md.DocumentoTipo> {

    return this.httpClient
      .post<md.DocumentoTipo>(
        environment.APIS.DOCUMENTO_TIPO.BASE,
        documentoTipo
      )
      .pipe(
        tap(
          documentoTipo => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  updateDocumentoTipo(documentoTipo: md.DocumentoTipo): Observable<md.DocumentoTipo> {

    return this.httpClient
      .put<md.DocumentoTipo>(
        environment.APIS.DOCUMENTO_TIPO.BASE,
        documentoTipo
      )
      .pipe(
        tap(
          documentoTipo => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

  deleteDocumentoTipo(id: number): Observable<md.DocumentoTipo> {

    return this.httpClient
      .delete<md.DocumentoTipo>(
        environment.APIS.DOCUMENTO_TIPO.BASE + `/${id}`
      )
      .pipe(
        tap(
          documentoTipo => { }
        ),
        catchError(
          (err: any, caught: Observable<any>) => {
            if (err && err.status === 0) {
              err.error.message = 'Error! verifique la conexion con el servidor';
            }
            return throwError(err.error)
          }
        ));
  }

}
