import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class PaisesService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getPaises(): Observable<Array<md.Pais>> {

    return this.httpClient
      .get<Array<md.Pais>>(
        environment.APIS.PAIS.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getPaises', []))
      );
  }

  addPais(pais: md.Pais): Observable<md.Pais> {

    return this.httpClient
      .post<md.Pais>(
        environment.APIS.PAIS.BASE,
        pais,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Pais>('addPais'))
      );
  }

  updatePais(pais: md.Pais): Observable<md.Pais> {

    return this.httpClient
      .put<md.Pais>(
        environment.APIS.PAIS.BASE,
        pais,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Pais>('updatePais'))
      );
  }

  deletePais(id: number): Observable<md.Pais> {

    return this.httpClient
      .delete<md.Pais>(
        environment.APIS.PAIS.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.Pais>('deletePais'))
      );
  }

}
