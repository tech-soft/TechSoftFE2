import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { PersonasRelacionadasService } from './personas-relacionadas.service';
import { PERSONAS_RELACIONADAS } from '../../_mocks/personas-relacionadas.mock';

describe('PersonasRelacionadasService', () => {


  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PersonasRelacionadasService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([PersonasRelacionadasService], (service: PersonasRelacionadasService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/personas/personas-relacionadas/byid?id=',
//     async(
//       inject([PersonasRelacionadasService, HttpTestingController],
//         (service: PersonasRelacionadasService, backend: HttpTestingController) => {

//           const relacionId = PERSONAS_RELACIONADAS[0].id;

//           service.getPersonaRelacionada(relacionId)
// .subscribe();

//           const req = backend.expectOne(serviceUrlById + relacionId);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlById + relacionId);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método GET sobre url ../techsoftapp/personas/personas-relacionadas/bypersonaid?id=',
//     async(
//       inject([PersonasRelacionadasService, HttpTestingController],
//         (service: PersonasRelacionadasService, backend: HttpTestingController) => {

//           const personaId = PERSONAS_RELACIONADAS[0].personaPrincipal.id;

//           service.getPersonasRelacionadas(personaId)
// .subscribe();

//           const req = backend.expectOne(serviceUrlByPersona + personaId);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlByPersona + personaId);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/personas-relacionadas',
//     async(
//       inject([PersonasRelacionadasService, HttpTestingController],
//         (service: PersonasRelacionadasService, backend: HttpTestingController) => {

//           service.addPersonaRelacionada(PERSONAS_RELACIONADAS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/personas-relacionadas',
//     async(
//       inject([PersonasRelacionadasService, HttpTestingController],
//         (service: PersonasRelacionadasService, backend: HttpTestingController) => {

//           service.updatePersonaRelacionada(PERSONAS_RELACIONADAS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/personas-relacionadas',
//     async(
//       inject([PersonasRelacionadasService, HttpTestingController],
//         (service: PersonasRelacionadasService, backend: HttpTestingController) => {

//           service.deletePersonaRelacionada(PERSONAS_RELACIONADAS[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + PERSONAS_RELACIONADAS[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + PERSONAS_RELACIONADAS[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos las personas relacionadas',
//     async(
//       inject([PersonasRelacionadasService],
//         (service: PersonasRelacionadasService) => {

//           const personaId = PERSONAS_RELACIONADAS[0].personaPrincipal.id;

//           service.getPersonasRelacionadas(personaId)
// .subscribe(personasRelacionadas => {
//             expect(personasRelacionadas.length)
// .toBe(1, 'Se espera 1 elemento');
//             expect(personasRelacionadas)
// .toEqual(PERSONAS_RELACIONADAS, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(serviceUrlByPersona + personaId);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(PERSONAS_RELACIONADAS);
//         })
//     )
//   );
});
