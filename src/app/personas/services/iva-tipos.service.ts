import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

const headers = new HttpHeaders()
  .set('Content-Type', 'application/json')
  .set('Accept', 'application/json');

@Injectable()
export class IvaTiposService {

  constructor(
    private httpClient: HttpClient,
    private messageService: MessageService,
    private handleErrorService: HandleErrorService
  ) { }

  getIvaTipos(): Observable<Array<md.IvaTipo>> {

    return this.httpClient
      .get<Array<md.IvaTipo>>(
        environment.APIS.IVA_TIPO.BASE,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError('getIvaTipos', []))
      );
  }

  addIvaTipo(ivaTipo: md.IvaTipo): Observable<md.IvaTipo> {

    return this.httpClient
      .post<md.IvaTipo>(
        environment.APIS.IVA_TIPO.BASE,
        ivaTipo,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.IvaTipo>('addIvaTipo'))
      );
  }

  updateIvaTipo(ivaTipo: md.IvaTipo): Observable<md.IvaTipo> {

    return this.httpClient
      .put<md.IvaTipo>(
        environment.APIS.IVA_TIPO.BASE,
        ivaTipo,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.IvaTipo>('updateIvaTipo'))
      );
  }

  deleteIvaTipo(id: number): Observable<md.IvaTipo> {

    return this.httpClient
      .delete<md.IvaTipo>(
        environment.APIS.IVA_TIPO.BASE + `/${id}`,
        { headers }
      )
      .pipe(
        tap(_ => { }),
        catchError(this.handleErrorService.handleError<md.IvaTipo>('deleteIvaTipo'))
      );
  }
}
