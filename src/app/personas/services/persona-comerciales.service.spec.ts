import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { PersonaComercialesService } from './persona-comerciales.service';
import { PERSONA_COMERCIAL } from '../../_mocks/persona-comerciales.mock';
import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';

describe('PersonaComercialesService', () => {


  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PersonaComercialesService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

  // it('Se espera que el servicio esté creado', inject([PersonaComercialesService], (service: PersonaComercialesService) => {
  //   expect(service)
  //     .toBeTruthy();
  // }));

  // it('Se espera método GET sobre url ../techsoftapp/personas/datos-comerciales/byid?id=',
  //   async(
  //     inject([PersonaComercialesService, HttpTestingController],
  //       (service: PersonaComercialesService, backend: HttpTestingController) => {

  //         const personaId = PERSONA_COMERCIAL[0].persona.id;

  //         service.getDatosComerciales(personaId)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlByPersona + personaId);
  //         expect(req.request.method)
  //           .toBe('GET');
  //         expect(req.request.url)
  //           .toBe(serviceUrlByPersona + personaId);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método POST sobre url ../techsoftapp/datos-comerciales',
  //   async(
  //     inject([PersonaComercialesService, HttpTestingController],
  //       (service: PersonaComercialesService, backend: HttpTestingController) => {

  //         service.addDatosComerciales(PERSONA_COMERCIAL[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('POST');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método UPDATE sobre url ../techsoftapp/datos-comerciales',
  //   async(
  //     inject([PersonaComercialesService, HttpTestingController],
  //       (service: PersonaComercialesService, backend: HttpTestingController) => {

  //         service.updateDatosComerciales(PERSONA_COMERCIAL[0])
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase);
  //         expect(req.request.method)
  //           .toBe('PUT');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera método DELETE sobre url ../techsoftapp/datos-comerciales',
  //   async(
  //     inject([PersonaComercialesService, HttpTestingController],
  //       (service: PersonaComercialesService, backend: HttpTestingController) => {

  //         service.deleteDatosComerciales(PERSONA_COMERCIAL[0].id)
  //           .subscribe();

  //         const req = backend.expectOne(serviceUrlBase + PERSONA_COMERCIAL[0].id);
  //         expect(req.request.method)
  //           .toBe('DELETE');
  //         expect(req.request.url)
  //           .toBe(serviceUrlBase + PERSONA_COMERCIAL[0].id);
  //         expect(req.request.headers.get('Content-Type'))
  //           .toBe('application/json');
  //         expect(req.request.headers.get('Accept'))
  //           .toBe('application/json');

  //       })
  //   )
  // );

  // it('Se espera de respuesta con todos los datos comerciales',
  //   async(
  //     inject([PersonaComercialesService],
  //       (service: PersonaComercialesService) => {

  //         const personaId = PERSONA_COMERCIAL[0].persona.id;

  //         service.getDatosComerciales(personaId)
  //           .subscribe(datosComerciales => {
  //             expect(datosComerciales)
  //               .toEqual(PERSONA_COMERCIAL, 'Se espera resultado según mock');
  //           });

  //         const req = httpMock.expectOne(serviceUrlByPersona + personaId);
  //         expect(req.request.method)
  //           .toBe('GET', 'Se espera GET');
  //         req.flush(PERSONA_COMERCIAL);
  //       })));
});
