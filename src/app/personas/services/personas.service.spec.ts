import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { PersonasService } from './personas.service';
import { PERSONAS } from '../../_mocks/personas.mock';

describe('PersonasService', () => {


  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        PersonasService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([PersonasService], (service: PersonasService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/personas/personas/byid?id=',
//     async(
//       inject([PersonasService, HttpTestingController],
//         (service: PersonasService, backend: HttpTestingController) => {

//           const personaId = PERSONAS[0].id;

//           service.getPersona(personaId)
// .subscribe();

//           const req = backend.expectOne(serviceUrlById + personaId);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlById + personaId);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método GET sobre url ../techsoftapp/personas',
//     async(
//       inject([PersonasService, HttpTestingController],
//         (service: PersonasService, backend: HttpTestingController) => {

//           service.getPersonas()
// .subscribe();

//           const req = backend.expectOne(`${serviceUrlByKeyword}\'\'`);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(`${serviceUrlByKeyword}\'\'`);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/personas',
//     async(
//       inject([PersonasService, HttpTestingController],
//         (service: PersonasService, backend: HttpTestingController) => {

//           service.addPersona(PERSONAS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/personas',
//     async(
//       inject([PersonasService, HttpTestingController],
//         (service: PersonasService, backend: HttpTestingController) => {

//           service.updatePersona(PERSONAS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/personas',
//     async(
//       inject([PersonasService, HttpTestingController],
//         (service: PersonasService, backend: HttpTestingController) => {

//           service.deletePersona(PERSONAS[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + PERSONAS[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + PERSONAS[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos las personas',
//     async(
//       inject([PersonasService],
//         (service: PersonasService) => {

//           service.getPersonas()
// .subscribe(personas => {
//             expect(personas.length)
// .toBe(2, 'Se esperan 2 elementos');
//             expect(personas)
// .toEqual(PERSONAS, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(`${serviceUrlByKeyword}\'\'`);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(PERSONAS);
//         })
//     )
//   );

  // TODO: test to searchPersona
});
