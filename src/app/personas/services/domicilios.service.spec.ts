import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { DOMICILIOS } from '../../_mocks/domicilios.mock';
import { DomiciliosService } from './domicilios.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

describe('DomiciliosService', () => {


  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        DomiciliosService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([DomiciliosService], (service: DomiciliosService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/personas/domicilios/byid?id=',
//     async(
//       inject([DomiciliosService, HttpTestingController],
//         (service: DomiciliosService, backend: HttpTestingController) => {

//           const domicilioId = DOMICILIOS[0].id;

//           service.getDomicilio(domicilioId)
//           .subscribe();

//           const req = backend.expectOne(serviceUrlById + domicilioId);
//           expect(req.request.method)
//           .toBe('GET');
//           expect(req.request.url)
//           .toBe(serviceUrlById + domicilioId);
//           expect(req.request.headers.get('Content-Type'))
//           .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
//           .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método GET sobre url ../techsoftapp/personas/domicilios/bypersonaid?id=',
//     async(
//       inject([DomiciliosService, HttpTestingController],
//         (service: DomiciliosService, backend: HttpTestingController) => {

//           const personaId = DOMICILIOS[0].persona.id;

//           service.getDomicilios(personaId)
// .subscribe();

//           const req = backend.expectOne(serviceUrlByPersona + personaId);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlByPersona + personaId);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/domicilios',
//     async(
//       inject([DomiciliosService, HttpTestingController],
//         (service: DomiciliosService, backend: HttpTestingController) => {

//           service.addDomicilio(DOMICILIOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/domicilios',
//     async(
//       inject([DomiciliosService, HttpTestingController],
//         (service: DomiciliosService, backend: HttpTestingController) => {

//           service.updateDomicilio(DOMICILIOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/domicilios',
//     async(
//       inject([DomiciliosService, HttpTestingController],
//         (service: DomiciliosService, backend: HttpTestingController) => {

//           service.deleteDomicilio(DOMICILIOS[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + DOMICILIOS[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + DOMICILIOS[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos los domicilios',
//     async(
//       inject([DomiciliosService],
//         (service: DomiciliosService) => {

//           const personaId = DOMICILIOS[0].persona.id;

//           service.getDomicilios(personaId)
// .subscribe(contactos => {
//             expect(contactos.length)
// .toBe(1, 'Se espera 1 elemento');
//             expect(contactos)
// .toEqual(DOMICILIOS, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(serviceUrlByPersona + personaId);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(DOMICILIOS);
//         })
//     )
//   );
});
