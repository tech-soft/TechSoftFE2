import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';
import { async, getTestBed, inject, TestBed } from '@angular/core/testing';

import { environment } from '../../../environments/environment';
import { CONTACTOS } from '../../_mocks/contactos.mock';
import { ContactosService } from './contactos.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';

describe('ContactosService', () => {


  let injector: TestBed;
  let httpMock: HttpTestingController;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [
        HttpClientTestingModule
      ],
      providers: [
        ContactosService,
        HandleErrorService,
        MessageService
      ]
    });

    injector = getTestBed();
    httpMock = injector.get(HttpTestingController);
  });

//   it('Se espera que el servicio esté creado', inject([ContactosService], (service: ContactosService) => {
//     expect(service)
// .toBeTruthy();
//   }));

//   it('Se espera método GET sobre url ../techsoftapp/personas/contactos/byid?id=',
//     async(
//       inject([ContactosService, HttpTestingController],
//         (service: ContactosService, backend: HttpTestingController) => {

//           const contactoId = CONTACTOS[0].id;

//           service.getContacto(contactoId)
// .subscribe();

//           const req = backend.expectOne(serviceUrlById + contactoId);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(serviceUrlById + contactoId);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método GET sobre url ../techsoftapp/personas/contactos/bypersonaid?id=',
//     async(
//       inject([ContactosService, HttpTestingController],
//         (service: ContactosService, backend: HttpTestingController) => {

//           const personaId = CONTACTOS[0].persona.id;
//           const tipoId = CONTACTOS[0].tipo;

//           service.getContactos(personaId, tipoId)
// .subscribe();

//           const req = backend.expectOne(`${serviceUrlByPersona}${personaId}&tipoId=${tipoId}`);
//           expect(req.request.method)
// .toBe('GET');
//           expect(req.request.url)
// .toBe(`${serviceUrlByPersona}${personaId}&tipoId=${tipoId}`);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método POST sobre url ../techsoftapp/contactos',
//     async(
//       inject([ContactosService, HttpTestingController],
//         (service: ContactosService, backend: HttpTestingController) => {

//           service.addContacto(CONTACTOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('POST');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método UPDATE sobre url ../techsoftapp/contactos',
//     async(
//       inject([ContactosService, HttpTestingController],
//         (service: ContactosService, backend: HttpTestingController) => {

//           service.updateContacto(CONTACTOS[0])
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase);
//           expect(req.request.method)
// .toBe('PUT');
//           expect(req.request.url)
// .toBe(serviceUrlBase);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera método DELETE sobre url ../techsoftapp/contactos',
//     async(
//       inject([ContactosService, HttpTestingController],
//         (service: ContactosService, backend: HttpTestingController) => {

//           service.deleteContacto(CONTACTOS[0].id)
// .subscribe();

//           const req = backend.expectOne(serviceUrlBase + CONTACTOS[0].id);
//           expect(req.request.method)
// .toBe('DELETE');
//           expect(req.request.url)
// .toBe(serviceUrlBase + CONTACTOS[0].id);
//           expect(req.request.headers.get('Content-Type'))
// .toBe('application/json');
//           expect(req.request.headers.get('Accept'))
// .toBe('application/json');

//         })
//     )
//   );

//   it('Se espera de respuesta con todos los contactos',
//     async(
//       inject([ContactosService],
//         (service: ContactosService) => {

//           const personaId = CONTACTOS[0].persona.id;
//           const tipoId = 0;

//           service.getContactos(personaId, tipoId)
//               .subscribe(contactos => {
//             expect(contactos.length)
//             .toBe(2, 'Se esperan 2 elementos');
//             expect(contactos)
//           .toEqual(CONTACTOS, 'Se espera resultado según mock');
//           });

//           const req = httpMock.expectOne(`${serviceUrlByPersona}${personaId}&tipoId=${tipoId}`);
//           expect(req.request.method)
// .toBe('GET', 'Se espera GET');
//           req.flush(CONTACTOS);
//         })
//     )
//   );
});
