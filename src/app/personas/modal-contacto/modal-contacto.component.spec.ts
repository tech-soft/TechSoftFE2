import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { ContactosService } from '../services/contactos.service';
import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { PersonasService } from '../services/personas.service';
import { ModalContactoComponent } from './modal-contacto.component';



describe('ModalContactoComponent', () => {
  let component: ModalContactoComponent;
  let fixture: ComponentFixture<ModalContactoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalContactoComponent],
      imports: [HttpClientModule, FormsModule, RouterTestingModule],
      providers: [NgbModal, HandleErrorService, NgbActiveModal, HttpClient, ContactosService, MessageService, PersonasService]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalContactoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
