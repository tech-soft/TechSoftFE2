import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';

import * as md from '../../_models';
import { ContactosService } from '../services/contactos.service';
import { PersonasService } from '../services/personas.service';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-modal-contacto',
  templateUrl: './modal-contacto.component.html',
  styleUrls: ['./modal-contacto.component.scss']
})
export class ModalContactoComponent implements OnInit {

  @Input() id;
  @Input() tipo;
  @Input() hasContacto;
  @Input() persona;

  contacto: md.Contacto = <md.Contacto>{};

  constructor(
    public activeModal: NgbActiveModal,
    private contactosService: ContactosService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.contacto.personaId = this.persona.id;
    this.contacto.tipo = this.tipo;

    if (this.hasContacto) {
      this.getContacto();
    }
  }

  guardar(): void {
    if (this.validForm(true)) {
      this.contactosService.addContacto(this.contacto)
        .subscribe(result => {
          this.contacto = result;
          this.activeModal.close();
        },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  cerrar(): void {
    this.activeModal.close();
  }

  validForm(showAlerts: boolean): boolean {
    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.contacto.contacto) {
      if (showAlerts) {
        if (this.tipo == 1) {
          this.messageService.addError("Complete el campo teléfono.");
        } else if (this.tipo == 2) {
          this.messageService.addError("Complete el campo email.");
        } else {
          this.messageService.addError("Complete el campo contacto.");
        }
      }
      valido = false;
    }

    return valido;
  }

  private getContacto(): void {
    this.contactosService.getContacto(this.id)
      .subscribe(result => {
        if (result) {
          this.contacto = result;
          this.contacto.personaId = this.persona.id;
        } else {
          this.contacto = undefined;
        }
      },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
