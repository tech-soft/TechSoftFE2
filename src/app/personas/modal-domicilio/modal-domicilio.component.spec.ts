import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule, NgbTypeaheadConfig, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { HandleErrorService } from '../../core/services/handle-error.service';
import { MessageService } from '../../shared/services/message.service';
import { CiudadesService } from '../services/ciudades.service';
import { DomiciliosService } from '../services/domicilios.service';
import { PersonasService } from '../services/personas.service';
import { ProvinciasService } from '../services/provincias.service';
import { ModalDomicilioComponent } from './modal-domicilio.component';

describe('ModalDomicilioComponent', () => {
  let component: ModalDomicilioComponent;
  let fixture: ComponentFixture<ModalDomicilioComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ModalDomicilioComponent
      ],
      imports: [
        HttpClientModule,
        FormsModule,
        NgbTypeaheadModule,
        RouterTestingModule
      ],
      providers: [
        NgbModal,
        NgbModule,
        HandleErrorService,
        NgbActiveModal,
        DomiciliosService,
        HttpClient,
        MessageService,
        CiudadesService,
        ProvinciasService,
        PersonasService,
        NgbTypeaheadConfig
      ]
    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalDomicilioComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  // it('should create', () => {
  //   expect(component)
  //     .toBeTruthy();
  // });
});
