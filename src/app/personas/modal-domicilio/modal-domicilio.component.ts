import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { Observable, throwError } from 'rxjs';
import { debounceTime, map } from 'rxjs/operators';

import * as md from '../../_models';
import { CiudadesService } from '../services/ciudades.service';
import { DomiciliosService } from '../services/domicilios.service';
import { PersonasService } from '../services/personas.service';
import { ProvinciasService } from '../services/provincias.service';
import { MessageService } from 'src/app/shared/services/message.service';
import { temporaryAllocator } from '@angular/compiler/src/render3/view/util';


@Component({
  selector: 'app-modal-domicilio',
  templateUrl: './modal-domicilio.component.html',
  styleUrls: ['./modal-domicilio.component.scss']
})
export class ModalDomicilioComponent implements OnInit {

  @Input() id;
  @Input() hasDomicilio;
  @Input() persona;

  domicilio: md.Domicilio = <md.Domicilio>{};

  ciudades: md.Ciudad[];
  provincias: md.Provincia[];

  constructor(
    public activeModal: NgbActiveModal,
    private domiciliosService: DomiciliosService,
    private ciudadesService: CiudadesService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.domicilio.personaId = this.persona.id;
    this.domicilio.ciudad = <md.Ciudad>{};
    this.domicilio.ciudad.provincia = <md.Provincia>{};
    this.domicilio.principal = false;

    if (this.hasDomicilio) {
      this.getDomicilio();
    }

    this.getCiudades();
  }

  guardar(): void {
    if (this.validForm(true)) {
      this.domiciliosService.addDomicilio(this.domicilio)
        .subscribe(result => {
          this.domicilio = result;
          this.activeModal.close();
        },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  cerrar(): void {
    this.activeModal.close();
  }

  validForm(showAlerts: boolean): boolean {
    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.domicilio.calle) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo calle.");
      }
      valido = false;
    }
    if (!this.domicilio.nro) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo número.");
      }
      valido = false;
    }
    if (!this.domicilio.ciudad.nombre) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo ciudad.");
      }
      valido = false;
    }

    return valido;
  }

  private getDomicilio(): void {
    this.domiciliosService.getDomicilio(this.id)
      .subscribe(result => {
        if (result) {
          this.domicilio = result;
          this.domicilio.personaId = this.persona.id;
        } else {
          this.domicilio = undefined;
        }
      },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getCiudades(): void {
    this.ciudadesService.getCiudades()
      .subscribe(result => {
        if (result) {
          this.ciudades = result;
        }
      },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  search = (text$: Observable<String>) =>
    text$.pipe(
      debounceTime(200),
      map(term => term === '' ? []
        : this.ciudades.filter(v => v.nombre.toUpperCase()
          .indexOf(term.toUpperCase()) > -1)
          .slice(0, 10))
    )

  formatter = (x: { nombre: String }) => x.nombre;

  ngOnDestroy(): void {
    this.messageService.clear();
  }

}
