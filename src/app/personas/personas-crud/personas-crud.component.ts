import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { throwError } from 'rxjs';
import { MessageService } from 'src/app/shared/services/message.service';

import { environment } from '../../../environments/environment';
import * as md from '../../_models';
import { EstadosService } from '../../shared/services/estados.service';
import { DocumentoTiposService } from '../services/documento-tipos.service';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-personas-crud',
  templateUrl: './personas-crud.component.html',
  styleUrls: ['./personas-crud.component.scss']
})
export class PersonasCrudComponent implements OnInit {

  title: string;

  documentoTipos: md.DocumentoTipo[];
  estados: md.Estado[];

  personaId: number;
  persona: md.Persona = <md.Persona>{};

  readOnly: boolean;
  hasPersona: boolean;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private documentoTiposService: DocumentoTiposService,
    private estadoService: EstadosService,
    private messageService: MessageService,
    private personasService: PersonasService
  ) {
    this.readOnly = false;
    this.personaId = 0;
    this.hasPersona = false;
    this.title = "Gestión de Personas";
  }

  // TODO: agregar spinner cuando esta esperando
  // TODO: validar formato numero documento
  // TODO: validar documento repetido

  ngOnInit(): void {
    this.getDocumentoTipos();
    this.getEstados();
    this.getCrudStatus();
  }

  // Form
  getStatus(currentStatus: string) {
    switch (currentStatus) {
      case 'edit':
        this.edit();
        break;
      case 'delete':
        this.eliminarPersona();
        break;
      case 'save':
        this.guardarPersona();
        break;
      case 'cancel':
        this.cancel();
        break;
      default:
        break;
    }
  }

  validForm(showAlerts: boolean): boolean {

    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.persona.nombre) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo nombre.");
      }
      valido = false;
    }
    if (!this.persona.documentoTipo && this.persona.documentoNumero) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo tipo de documento o elimine el número de documento.");
      }
      valido = false;
    }

    return valido;
  }

  // Init
  private getDocumentoTipos(): void {
    this.documentoTiposService.getDocumentoTipos()
      .subscribe(
        results => {
          this.documentoTipos = results;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getEstados(): void {
    this.estadoService.getEstados()
      .subscribe(
        results => {
          this.estados = results;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getCrudStatus(): void {
    this.hasPersona =
      this.route.snapshot.paramMap.get('id') != undefined
      && this.route.snapshot.paramMap.get('id') !== '0';

    if (this.hasPersona) {
      this.readOnly = true;
      this.personaId = +this.route.snapshot.paramMap.get('id');
      this.getPersona();

    } else {
      this.readOnly = false;
      this.personaId = 0;
    }
  }

  // Persona
  private getPersona(): void {
    if (this.personaId > 0) {
      this.personasService.getPersona(this.personaId)
        .subscribe(
          result => {
            this.persona = result;
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  private guardarPersona(): void {
    if (this.validForm(true)) {
      if (!this.persona.documentoNumero) {
        this.persona.documentoTipo = undefined;
      }

      if (!this.persona.estado) {
        this.persona.estado = {
          id: environment.ESTADOS.ACTIVO,
          descripcion: "Nueva"
        }
      }

      (this.hasPersona) ? this.actualizarPersona() : this.insertarPersona();

    }
  }

  private insertarPersona(): void {
    this.personasService.postPersona(this.persona)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("La persona ha sido generada con éxito.");

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el código con el que se generó la persona. Verifique que la misma se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private actualizarPersona(): void {
    this.personasService.putPersona(this.persona)
      .subscribe(
        result => {
          if (result && result.id) {

            this.messageService.addSuccess("La persona ha sido actualizada con éxito.");

            this.initAfterSave(result.id);

          } else {
            this.messageService.addWarning("No se ha podido recuperar el código con el que se generó la persona. Verifique que la misma se haya generado correctamente.");
          }
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private initAfterSave(personaId: number): void {
    this.personaId = personaId;
    this.getPersona();

    this.readOnly = true;
    this.hasPersona = true;
    this.router.navigate([`/personas/crud/${this.personaId}`]);
  }
  
  private eliminarPersona(): void {
    // TODO: confirm msg
    if (this.persona && this.persona.id) {
      this.personasService.deletePersona(this.persona.id)
        .subscribe(
          result => {
            this.messageService.addSuccess("Se ha eliminado con éxito.");
            this.router.navigate(['/personas/search']);
          },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  // Form  
  private edit(): void {
    this.readOnly = false;
    this.hasPersona = true;
  };

  private cancel(): void {
    this.getPersona();
    this.readOnly = true;
  }

  compareFn(a1: any, a2: any): boolean {
    return a1 && a2 ? a1.id === a2.id : a1 === a2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }

}
