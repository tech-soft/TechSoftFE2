import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { ActivatedRoute, convertToParamMap, Router } from '@angular/router';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';
import { CrudButtonsComponent } from 'src/app/shared/crud-buttons/crud-buttons.component';
import { environment } from 'src/environments/environment';

import * as mk from '../../_mocks';
import * as md from '../../_models';
import { EstadosService } from '../../shared/services/estados.service';
import { MessageService } from '../../shared/services/message.service';
import { ContactosComponent } from '../contactos/contactos.component';
import { DomiciliosComponent } from '../domicilios/domicilios.component';
import { PersonasComercialComponent } from '../personas-comercial/personas-comercial.component';
import { PersonasRelacionadasComponent } from '../personas-relacionadas/personas-relacionadas.component';
import { DocumentoTiposService } from '../services/documento-tipos.service';
import { PersonasService } from '../services/personas.service';
import { PersonasCrudComponent } from './personas-crud.component';
import { ModalContactoComponent } from '../modal-contacto/modal-contacto.component';
import { ModalDomicilioComponent } from '../modal-domicilio/modal-domicilio.component';
import { ModalPersonaRelacionadaComponent } from '../modal-persona-relacionada/modal-persona-relacionada.component';

class DocumentoTiposServiceMock {
  getDocumentoTipos(): Observable<md.DocumentoTipo[]> {
    return of(undefined);
  }
}

class EstadosServiceMock {
  getEstados(): Observable<md.Estado[]> {
    return of(undefined);
  }

}

class PersonasServiceMock {
  getPersona(id: number): Observable<md.Persona> {
    return of(undefined);
  }
  searchPersonas(terms: Observable<String>): Observable<any> {
    return of(undefined);
  }
  postPersona(persona: md.Persona): Observable<md.Persona> {
    return of(undefined);
  }
  putPersona(persona: md.Persona): Observable<md.Persona> {
    return of(undefined);
  }
  deletePersona(id: number): Observable<md.Persona> {
    return of(undefined);
  }
}

describe('PersonasCrudComponent', () => {
  let component: PersonasCrudComponent;
  let fixture: ComponentFixture<PersonasCrudComponent>;
  let documentoTiposService: DocumentoTiposService;
  let estadosService: EstadosService;
  let messageService: MessageService;
  let personasService: PersonasService;

  const routerSpy = jasmine.createSpyObj('Router', ['navigate']);

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        ContactosComponent,
        DomiciliosComponent,
        ModalContactoComponent,
        ModalDomicilioComponent,
        ModalPersonaRelacionadaComponent,
        PersonasComercialComponent,
        PersonasCrudComponent,
        PersonasRelacionadasComponent,
        CrudButtonsComponent
      ],
      imports: [
        FormsModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule.forRoot()
      ],
      providers: [
        { provide: DocumentoTiposService, useClass: DocumentoTiposServiceMock },
        { provide: EstadosService, useClass: EstadosServiceMock },
        { provide: PersonasService, useClass: PersonasServiceMock },
        { provide: Router, useValue: routerSpy },
        {
          provide: ActivatedRoute,
          useValue: { snapshot: { paramMap: convertToParamMap({ id: undefined }) } }
        },
        MessageService
      ]
    })
      .compileComponents();
      fixture = TestBed.createComponent(PersonasCrudComponent);

    component = fixture.componentInstance;
    documentoTiposService = TestBed.get(DocumentoTiposService);
    estadosService = TestBed.get(EstadosService);
    messageService = TestBed.get(MessageService);
    personasService = TestBed.get(PersonasService);
    fixture.detectChanges();
  }));


  it('se debe inicializar valores en el constructor', fakeAsync(() => {
    expect(component.readOnly).toBeFalsy();
    expect(component.personaId).toBe(0);
    expect(component.hasPersona).toBeFalsy();
    expect(component.title).toBe("Gestión de Personas");
  }));

  it('ngOnInit debe llamar a getDocumentoTipos/getEstados', fakeAsync(() => {
    spyOn(documentoTiposService, 'getDocumentoTipos').and.returnValue(of());
    spyOn(estadosService, 'getEstados').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(documentoTiposService.getDocumentoTipos).toHaveBeenCalled();
    expect(estadosService.getEstados).toHaveBeenCalled();
  }));

  it('getDocumentoTipos debe devolver error cuando falla', fakeAsync(() => {
    spyOn(documentoTiposService, 'getDocumentoTipos').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('getEstados debe devolver error cuando falla', fakeAsync(() => {
    spyOn(estadosService, 'getEstados').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('ngOnInit debe llamar a getCRUDStatus y definir estados segun CRUD status', fakeAsync(() => {
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(component.hasPersona).toBeFalsy();
    expect(component.readOnly).toBeFalsy();
    expect(component.personaId).toBe(0);
  }));

  it('guardarPersona debe llamar a postPersona si la persona es válida y hasPersona false', fakeAsync(() => {
    component.persona.documentoTipo = {
      id: 1,
      descripcion: 'DNI',
      descripcionReducida: 'DNI'
    };
    spyOn(component, 'validForm').and.returnValue(true);
    spyOn(personasService, 'postPersona').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(component.persona.documentoTipo.id).toBe(1);
    component.hasPersona = false;
    component.guardarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.postPersona).toHaveBeenCalled();
    expect(component.persona.estado.id).toBe(environment.ESTADOS.ACTIVO);
    expect(component.persona.documentoTipo).toBeUndefined();

    personasService.postPersona(mk.PERSONAS[0]).subscribe(
      result => {
        expect(component.readOnly).toBeTruthy();
        expect(component.hasPersona).toBeTruthy();
      }
    );
  }));

  it('guardarPersona debe llamar a putPersona si la persona es válida y hasPersona true', fakeAsync(() => {
    spyOn(component, 'validForm').and.returnValue(true);
    spyOn(personasService, 'putPersona').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasPersona = true;
    component.guardarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.putPersona).toHaveBeenCalled();

    personasService.putPersona(mk.PERSONAS[0]).subscribe(
      result => {
        expect(component.readOnly).toBeTruthy();
        expect(component.hasPersona).toBeTruthy();
      }
    );
  }));

  it('guardarPersona no debe llamar a postPersona ni a putPersona si la persona no es válida', fakeAsync(() => {
    spyOn(component, 'validForm').and.returnValue(false);
    spyOn(personasService, 'postPersona').and.returnValue(of());
    spyOn(personasService, 'putPersona').and.returnValue(of());
    component.ngOnInit();
    component.guardarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.postPersona).not.toHaveBeenCalled();
    expect(personasService.putPersona).not.toHaveBeenCalled();
  }));

  it('guardarPersona debe devolver error cuando falla el post', fakeAsync(() => {
    spyOn(component, 'validForm').and.returnValue(true);
    spyOn(personasService, 'postPersona').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasPersona = false;
    component.guardarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.postPersona).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('guardarPersona deben devolver error cuando falla el put', fakeAsync(() => {
    spyOn(component, 'validForm').and.returnValue(true);
    spyOn(personasService, 'putPersona').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.hasPersona = true;
    component.guardarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.putPersona).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);
  }));

  it('eliminarPersona debe llamar a deletePersona si la persona es válida', fakeAsync(() => {
    component.persona = Object.assign({}, mk.PERSONAS[0]);
    spyOn(personasService, 'deletePersona').and.returnValue(of());
    component.ngOnInit();
    component.eliminarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.deletePersona).toHaveBeenCalled();
  }));

  it('eliminarPersona no debe llamar a deletePersona si la persona no es válida', fakeAsync(() => {
    spyOn(personasService, 'deletePersona').and.returnValue(of());
    component.ngOnInit();
    component.eliminarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.deletePersona).not.toHaveBeenCalled();
  }));

  it('eliminarPersona deben devolver error cuando falla', fakeAsync(() => {
    component.persona = Object.assign({}, mk.PERSONAS[0]);
    spyOn(personasService, 'deletePersona').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();
    component.eliminarPersona();

    tick();
    fixture.detectChanges();
    expect(personasService.deletePersona).toHaveBeenCalled();
    expect(messageService.addError).toHaveBeenCalledTimes(1);

  }));

  it('edit debe setear variables para editar', fakeAsync(() => {
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.edit();

    tick();
    fixture.detectChanges();
    expect(component.readOnly).toBeFalsy();
    expect(component.hasPersona).toBeTruthy();
  }));

  it('cancel debe setear variables', fakeAsync(() => {
    spyOn(personasService, 'getPersona').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    component.personaId = 1;
    component.cancel();

    tick();
    fixture.detectChanges();
    expect(component.readOnly).toBeTruthy();
    expect(personasService.getPersona).toHaveBeenCalled();

  }));

  it('getStatus debe seleccionar accion de acuerdo a string enviado', fakeAsync(() => {
    spyOn(component, 'edit');
    spyOn(component, 'eliminarPersona');
    spyOn(component, 'guardarPersona');
    spyOn(component, 'cancel');
    component.ngOnInit();
    component.getStatus('edit');

    tick();
    fixture.detectChanges();
    expect(component.edit).toHaveBeenCalled();
    component.getStatus('delete');

    tick();
    fixture.detectChanges();
    expect(component.eliminarPersona).toHaveBeenCalled();
    component.getStatus('save');

    tick();
    fixture.detectChanges();
    expect(component.guardarPersona).toHaveBeenCalled();
    component.getStatus('cancel');

    tick();
    fixture.detectChanges();
    expect(component.cancel).toHaveBeenCalled();
  }));

  it('ngOnDestroy debe limpiar los mensajes', fakeAsync(() => {
    spyOn(messageService, 'clear');
    component.ngOnInit();
    component.ngOnDestroy();

    tick();
    fixture.detectChanges();
    expect(messageService.messages.length).toBe(0);
    expect(messageService.clear).toHaveBeenCalled();
  }));
});
