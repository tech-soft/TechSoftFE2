import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Subject, throwError } from 'rxjs';

import * as md from '../../_models';
import { MessageService } from '../../shared/services/message.service';
import { PersonasService } from '../services/personas.service';

@Component({
  selector: 'app-personas-search',
  templateUrl: './personas-search.component.html',
  styleUrls: ['./personas-search.component.scss']
})
export class PersonasSearchComponent implements OnInit {

  title: string;

  personas: md.Persona[];
  searchPersonas$ = new Subject<String>();

  constructor(
    private router: Router,
    private personasService: PersonasService,
    private messageService: MessageService
  ) {
    this.title = "Búsqueda de Personas";
  }

  ngOnInit(): void {
    this.getPersonas();
  }

  goto(menuOption: String, id: number): void {
    switch (menuOption) {
      case 'PersonaCrudAdd':
        this.router.navigate(['/personas/crud']);
        break;
      case 'PersonaCrudEdit':
        this.router.navigate([`/personas/crud/${id}`]);
        break;
      default:
        break;
    }
  }

  private getPersonas(): void {
    this.personasService.searchPersonas(this.searchPersonas$)
      .subscribe(
        result => {
          this.personas = result;
        },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        });
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }
}
