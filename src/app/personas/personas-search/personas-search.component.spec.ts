import { HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { FontAwesomeModule } from '@fortawesome/angular-fontawesome';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { Observable, of, throwError } from 'rxjs';

import { MessageService } from '../../shared/services/message.service';
import { PersonasService } from '../services/personas.service';
import { PersonasSearchComponent } from './personas-search.component';

class PersonasServiceMock {
  public searchPersonas(terms: Observable<String>): Observable<any> {
    return of(undefined);
  }
}

describe('PersonasSearchComponent', () => {
  let component: PersonasSearchComponent;
  let fixture: ComponentFixture<PersonasSearchComponent>;
  let messageService: MessageService;
  let personasService: PersonasService;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        PersonasSearchComponent
      ],
      imports: [
        FormsModule,
        FontAwesomeModule,
        HttpClientModule,
        NgbModule.forRoot(),
        RouterTestingModule
      ],
      providers: [
        { provide: PersonasService, useClass: PersonasServiceMock },
        MessageService
      ]
    })
      .compileComponents();

    fixture = TestBed.createComponent(PersonasSearchComponent);
    component = fixture.componentInstance;
    personasService = TestBed.get(PersonasService);
    messageService = TestBed.get(MessageService);
    fixture.detectChanges();
  }));

  it('ngOnInit debe llamar a searchPersonas en PersonaService', fakeAsync(() => {
    spyOn(personasService, 'searchPersonas').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(personasService.searchPersonas).toHaveBeenCalled();
  }));

  it('getPersonas debe devolver error cuando falla searchPersonas en PersonaService', fakeAsync(() => {
    spyOn(personasService, 'searchPersonas').and.returnValue(throwError({}));
    spyOn(messageService, 'addError').and.returnValue(of());
    component.ngOnInit();

    tick();
    fixture.detectChanges();
    expect(messageService.addError).toHaveBeenCalled();
  }));

  it('OnDestroy debe eliminar los mensajes de error', fakeAsync(() => {
    spyOn(personasService, 'searchPersonas').and.returnValue(throwError({}));
    spyOn(messageService, 'clear').and.returnValue(of());
    component.ngOnInit();
    component.ngOnDestroy();

    tick();
    fixture.detectChanges();
    expect(messageService.clear).toHaveBeenCalled();
  }));
});
