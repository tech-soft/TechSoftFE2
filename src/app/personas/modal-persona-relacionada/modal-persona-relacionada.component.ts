import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { NgbActiveModal, NgbTypeahead } from '@ng-bootstrap/ng-bootstrap';
import { Observable, Subject, throwError } from 'rxjs';
import { debounceTime, distinctUntilChanged, map } from 'rxjs/operators';

import * as md from '../../_models';
import { PersonaRelacionTiposService } from '../services/persona-relacion-tipos.service';
import { PersonasRelacionadasService } from '../services/personas-relacionadas.service';
import { PersonasService } from '../services/personas.service';
import { MessageService } from 'src/app/shared/services/message.service';

@Component({
  selector: 'app-modal-persona-relacionada',
  templateUrl: './modal-persona-relacionada.component.html',
  styleUrls: ['./modal-persona-relacionada.component.scss']
})
export class ModalPersonaRelacionadaComponent implements OnInit {

  @Input() id: number;
  @Input() hasPersonaRelacionada: boolean;
  @Input() persona;

  relacion: md.PersonaRelacionada = <md.PersonaRelacionada>{};

  relacionTipos: md.PersonaRelacionTipo[];
  personas: md.Persona[];

  constructor(
    public activeModal: NgbActiveModal,
    private personasRelacionadasService: PersonasRelacionadasService,
    private personasService: PersonasService,
    private personaRelacionTiposService: PersonaRelacionTiposService,
    private messageService: MessageService
  ) { }

  ngOnInit(): void {
    this.relacion.personaPrincipal = this.persona;
    this.relacion.personaRelacionada = <md.Persona>{};

    if (this.hasPersonaRelacionada) {
      this.getPersonaRelacionada();
    }

    this.getRelacionTipos();
    this.getPersonas();

  }

  guardar(): void {
    if (this.validForm(true)) {
      this.personasRelacionadasService.addPersonaRelacionada(this.relacion)
        .subscribe(result => {
          this.relacion = result;
          this.activeModal.close();
        },
          error => {
            this.messageService.addError(`${error.message}`);
            return throwError(error);
          }
        );
    }
  }

  cerrar(): void {
    this.activeModal.close();
  }

  validForm(showAlerts: boolean): boolean {
    if (showAlerts) {
      // should test this call
      this.messageService.clear();
    }

    let valido: boolean;

    valido = true;

    if (!this.relacion.personaRelacionada.id || !this.relacion.personaPrincipal.id) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo persona relacionada.");
      }
      valido = false;
    }
    if (!this.relacion.tipo) {
      if (showAlerts) {
        this.messageService.addError("Complete el campo tipo de relación.");
      }
      valido = false;
    }

    return valido;
  }

  isMe(currentId: number): boolean {
    if (this.persona.id === currentId) {
      return true;
    } else {
      return false;
    }
  }
  
  private getPersonaRelacionada(): void {
    this.personasRelacionadasService.getPersonaRelacionada(this.id)
      .subscribe(result => {
        if (result) {
          this.relacion = result;
        }
      },
        error => {
          this.messageService.addError(`${error.message}`);
          return throwError(error);
        }
      );
  }

  private getRelacionTipos(): void {
    this.personaRelacionTiposService.getPersonaRelacionTipos()
      .subscribe(result => {
        if (result) {
          this.relacionTipos = result;
        }
      },
      error => {
        this.messageService.addError(`${error.message}`);
        return throwError(error);
      }
    );
  }

  private getPersonas(): void {
    this.personasService.getPersonas()
      .subscribe(result => {
        if (result) {
          this.personas = result;
        }
      },
      error => {
        this.messageService.addError(`${error.message}`);
        return throwError(error);
      }
    );
  }

  search = (text$: Observable<String>) =>
    text$.pipe(
      debounceTime(200),
      distinctUntilChanged(),
      map(term => term === '' ? []
        : this.personas.filter(p =>
          p.nombre.toLowerCase()
            .indexOf(term.toLowerCase()) > -1)
          .slice(0, 5))

    )

  formatter = (x: { nombre: String }) => x.nombre;

  compareFn(p1: any, p2: any): boolean {

    return p1 && p2 ? p1.id === p2.id : p1 === p2;
  }

  trackByFn(index, item): number {
    return index;
  }

  ngOnDestroy(): void {
    this.messageService.clear();
  }

}
