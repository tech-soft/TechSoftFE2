import { HttpClient, HttpClientModule } from '@angular/common/http';
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { FormsModule } from '@angular/forms';
import { RouterTestingModule } from '@angular/router/testing';
import { NgbActiveModal, NgbModal, NgbModule, NgbTypeaheadConfig, NgbTypeaheadModule } from '@ng-bootstrap/ng-bootstrap';

import { MessageService } from '../../shared/services/message.service';
import { HandleErrorService } from '../../core/services/handle-error.service';
import { PersonaRelacionTiposService } from '../services/persona-relacion-tipos.service';
import { PersonasRelacionadasService } from '../services/personas-relacionadas.service';
import { PersonasService } from '../services/personas.service';
import { ModalPersonaRelacionadaComponent } from './modal-persona-relacionada.component';

describe('ModalPersonaRelacionadaComponent', () => {
  let component: ModalPersonaRelacionadaComponent;
  let fixture: ComponentFixture<ModalPersonaRelacionadaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ModalPersonaRelacionadaComponent],
      imports: [HttpClientModule, FormsModule, NgbTypeaheadModule, RouterTestingModule],
      providers: [NgbModal, NgbModule, HandleErrorService, NgbActiveModal, PersonasService, PersonaRelacionTiposService,
        PersonasRelacionadasService, HttpClient, MessageService, NgbTypeaheadConfig]

    })
      .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ModalPersonaRelacionadaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

//   it('should create', () => {
//     expect(component)
// .toBeTruthy();
//   });
});
