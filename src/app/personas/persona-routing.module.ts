import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthGuard } from '../_guards/auth.guard';
import { ContactosComponent } from './contactos/contactos.component';
import { DomiciliosComponent } from './domicilios/domicilios.component';
import { PersonasComercialComponent } from './personas-comercial/personas-comercial.component';
import { PersonasCrudComponent } from './personas-crud/personas-crud.component';
import { PersonasRelacionadasComponent } from './personas-relacionadas/personas-relacionadas.component';
import { PersonasSearchComponent } from './personas-search/personas-search.component';

const routes: Routes = [
  {
    path: 'contactos',
    component: ContactosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'domicilios',
    component: DomiciliosComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personas/comercial',
    component: PersonasComercialComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personas/crud',
    component: PersonasCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personas/crud/:id',
    component: PersonasCrudComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personas/relacionadas',
    component: PersonasRelacionadasComponent,
    canActivate: [AuthGuard]
  },
  {
    path: 'personas/search',
    component: PersonasSearchComponent,
    canActivate: [AuthGuard]
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes)
  ],
  exports: [
    RouterModule
  ]
})
export class PersonaRoutingModule { }
