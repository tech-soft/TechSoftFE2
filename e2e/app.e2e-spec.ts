import { AngularEdgePage } from './app.po';

describe('angular-edge App', (): void => {
  let page: AngularEdgePage;

  beforeEach(() => {
    page = new AngularEdgePage();
  });

  it('should display message saying app works', () => {
    page.navigateTo();
    expect(page.getParagraphText())
    .toEqual('app works!');
  });
});
